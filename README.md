------------------------------------------------------------------------------
    NEURAL NETWOK LIBRARY for C/C++ Version 1.1
    Copyright (C) 2007-2010   Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------   

TITLE: NEURAL NETWORK LIBRARY FOR C/C++
RELEASE DATE: 29 March 2010, Monday
VERSION: 1.2


Dependency: 

      1. GSL (including cblas)
      2. Lapack, BLAS



-------------------------
INSTALLATION
------------------------

It is assumed that you have downloaded the package "nnlib-1.2.tar.gz"
to your home folder.  After extracting the package go to 'src' folder
inside the package. Now build libraries by running make available in
all the folders inside src as shown below:

$ tar -xzvf nnlib-1.1.tar.gz
$ cd nnlib-1.1
$ cd src

$ cd NN
$ make

$ cd ../utility   (will be removed in future)
$ make

After compilation you should have following 3 libraries inside 'lib'
folder.

$ cd ~/nnlib-1.2/lib/
libneural.a  libutil.a

Now test examples available in 'test' folder can be compiled as follows:

$ cd ~/nnlib-1.2/test/
$ make

Test examples demonstrate the use of available routines. 

-------------------
USAGE
------------------

The make file builds static libraries which can be linked with your
source program easily by using flags as shown below:

$ g++ -g abc.cpp -I ~/nnlib-1.2/include/ -L ~/nnlib-1.2/lib -lneural -lgsl -lgslcblas -o abc
$ ./abc

Please refer to makefiles for more information.

---------------------------



