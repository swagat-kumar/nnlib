/* ==================================================
 * Updates:
 *
 * Date: 26 March 2007 Monday
 * - RK4 tested  
 *
 *
 *
 * -----
 *  Date: 12 April 2007 Thursday
 *  - constant matrix functions
 *  - Successfully tested
 *  Date: 13 April 2007 Friday
 *  - Const Vector functions
 *  - successfully tested 
 *  -----
 *  
 *  ----
 *  Date: 13 April 2007 Friday
 *  - Vector::extract(int)
 *  - Vector::extract(int, int)
 *  - Matrix::extract(int, int)
 *  - Matrix::extract(int, int, int, int)
 *  - Matrix::operator[](int) - extract vectors
 *  ----
 *
 * 
 * 
 * ======================================================= */
#include<iostream>
#include<cmath>
#include <cvm.h>
#include <numroute.h>
#include <fstream>

//#define CAT
//#define ELMT
//#define ARITH
//#define RK4
//#define CONST
#define EXT

using namespace std;
using namespace cvm;

#ifdef RK4
// Vander Pol Equation
Vector sysfunc(Vector x, Vector u, double t)
{
  Vector xdot(2);
  xdot[0] = x[1];
  xdot[1] = -10 * sin(x[0]) - x[1];

  return xdot;
}
#endif

int main()
{
  
#ifdef ARITH
  double a[4] = {1, 2, 3 ,4};
  double b[4] = {6, 7, 8, 9};
  double d[2] = {1, 2};
  double e[2] = {10, 12};
  double a1 = 2.0;

  
  Matrix A(a,2,2);
  Matrix B(b, 2,2);
  Matrix C(2,2);
  Matrix D(2,2);
  Matrix E(2,2);
  Matrix G(2,2);
  Matrix H(2,2);



  Vector dd(d,2);
  Vector ee(e,2);
  Vector ff(2);
  Vector gg(2);
  Vector hh(2);
  Vector ii(2);

 
  cout << "A = " << A;
#endif   

#ifdef ELMT
  
  double a[4] = {1, 2, 3 ,4};
  Matrix A(a,2,2);

  // Accessing Matrix elements
  cout << "A(1,1) = " << A(1,1) << endl;

  // Assigning to a matrix element.
  A(1,1) = 10.0;

  cout << "A = " << A ;
  getchar();
 
  cout << "B=" << B;
  
  Vector ff(e, 2);

  // Accessing Vector elements
  cout << "ff(1) = " << ff[1] << endl;
 
  //Changing vector elements
  ff[1] = 15.0;
  cout << "ff = " << ff;
  getchar();

#endif


#ifdef ARITH  

  C = A + B;
  ff = dd + ee;
  E = A * B;
  G = B * a1;
  hh = 2.0 * ff;
  gg = A * ee;
  ii = dd * B;
                 
  cout << "C = A + B" << C;

  cout << "E = A * B" << E;

  cout << "G = B * a1" << G;

  cout << "dd = " << dd;

  cout << "ee = " << ee;

  cout << "ff = dd + ee " << ff;

  cout << "hh = 2.0 * ff " << hh;

  cout << "gg = A * ee" << gg;

  cout << "ii = dd * B" << ii;

  cout << "printing ff" << endl;
  cout << ff  << endl;


  double s = dd * ee;
  cout << "Dot product = dd.ee:" << s << endl;

  cout << "outer Product = D = dd * ee' ";

  D = dd.outerproduct(ee);
  cout << D;

  cout << "Transpose of Matrix D \n";
  cout << !D;

  cout << "Negative of a matrix D \n";
  cout << -D;
#endif 
  
#ifdef CAT

  double a2[6] = {0, 1, 2, 3, 4, 5};
  double b2[6] = {10, 11, 12, 13, 14, 15};

  Matrix A2(a2, 2, 3);
  Matrix B2(b2, 2, 3);

  Matrix C2(2,6);
  Matrix D2(4,3);

  C2 = A2 | B2;
  D2 = A2 ^ B2;

  cout << "A2 = " << A2 ;
  cout << "B2 = " << B2 ;
  cout << "C2 = A2 ^ B2" << C2 ;
  cout << "D2 = A2 | B2" << D2 ;
 
#endif

#ifdef RK4
  ofstream f1 ("data.txt");
  double x0[2] = {4, 5};
  Vector x(x0, 2);
  Vector u(1);
  for(double t = 0.0; t < 10.0; t = t + 0.01)
  {
    
      x = rk4(x, u, t, 0.01, sysfunc);

      f1 << x[0] << "\t" << x[1] << endl;
  }
  
#endif  
  
 #ifdef CONST 
  //date: 12 APRIL 2007 Thursday
  // Successfully tested  ...

  cout << "Const Matrix Objects : \n";
  double a[4] = {1, 2, 3, 4};
  double b[4] = {5, 6, 7, 8};
  const Matrix A(a, 2, 2);
  Matrix B(b, 2, 2);
  Matrix C(2,2);
  Matrix D(2,2);
  Matrix E(2,2);

  C = A + B;
  D = B - A;
  E = B * A;
  
  cout << "A = " << A ;
  cout << "B = " << B;
  cout << "C = " << C;
  cout << "D = " << D;
  cout << "E = " << E;
  cout << "A(1, 1) = " << A(1,1) << endl;

  // Assignment 
  B(1,1) = 2.5;

  cout << "After assignment\n";
  cout << "B = " << B ;


  

  // Const Vectors
  // 13 April 2007 Friday

  const Vector a1(a,4);
  Vector b1(b, 4);

  Vector c1(4);
  Vector c2(4);
  Vector d1(4);
  double g;

  cout << "Const Vector objects :\n";
  c1 = a1 + b1;
  c2 = b1 + a1;
  d1 = b1 - a1;
  g = a1 * b1;

  cout << "a1 = " << a1;
  cout << "b1 = " << b1;
  cout << "c1 = a1 + b1 " << c1;
  cout << "c2 = b1 + a1" << c2;
  cout << "d1 = b1 - a1" << d1;
  cout << "g = a1 * b1 = " << g << endl;
#endif           

#ifdef EXT

  //Date: 13 April 2007 Friday
  // Extracting Vectors
  double q[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  Matrix A(q, 3, 3);
  Vector Q(q, 9);
  Matrix B(4, 4);
  Matrix C(2, 2);
  Matrix D(3, 3);

  B.random();

  Vector e(3);
  Vector f(4);
  Vector g(5);

  e = A[1];
  f = Q.extract(5,8);
  g = Q.extract(4);

  C = B.extract(2,3,2,3);
  D = B.extract(2, 2);


  cout << "A =" << A;
  cout << "e = A[1]" << e ;
  cout << "Q = " << Q;
  cout << "f = Q[5-8]" << f;
  cout << "g = Q[0-4]" << g;

  cout << "B = " << B;
  cout << "C = " << C;
  cout << "D = " << D;

#endif 
  
  
  return 0;
}
   
