/* Testing MLP3 Program for XOR function
 * Date: 08 June 2007 Friday
 * Status: OK
 * --------------------------------------- */
#include<iostream>
#include<nnet.h>
#include<fstream>

using namespace std;
using namespace nnet;

#define Np 4
#define Ni 2
#define No 1

int main()
{
  
  int n,p;
  double x[Ni], y[No], yd[No], Px[Np][2], Py[Np], s1;

  int N[3] = {2, 4, 1};
  double act[3] = {1,1,0}, oact[3] = {1, 1, 0};

  ofstream f1("data.txt");

  //Training patterns
  Px[0][0] = 0.1; Px[0][1] = 0.1; Py[0] = 0.1;
  Px[1][0] = 0.1; Px[1][1] = 0.9; Py[1] = 0.9;
  Px[2][0] = 0.9; Px[2][1] = 0.1; Py[2] = 0.9;
  Px[3][0] = 0.9; Px[3][1] = 0.9; Py[3] = 0.1;
  
  mlp3 A(N, act, oact, 1, 1); //bipolar, unity bias


  //Training
  for(n = 1; n <= 10000; n++)
  {
    s1 = 0.0;
    for(p = 0; p <= Np; p++)
    {
      x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

      A.network_output(x, y);
      //A.wt_jacobian();
      //A.gd_update2(yd, 0.5);
      A.gd_update(yd, 0.5);

      s1 += (yd[0] - y[0]) * (yd[0] - y[0]);
    }
    s1 = s1/Np;

    //cout << "n = " << n << "\t se = " << s1 << endl;

    f1 << n << "\t" << s1 << endl;
  }

  //Testing
  
  for(p = 0; p < Np; p++)
  {
     x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

     A.network_output(x, y); 

     cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;
  }

  f1.close();
  return 0;
}
    

      
      

  


  
