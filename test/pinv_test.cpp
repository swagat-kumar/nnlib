/* This file demonstrates the implementation of 'pinv' function that computes
 * the pseudo-inverse of a rectangular matrix A of dimension m x n using LAPACK
 * routines. Please refer to 'pinv.h' for more details
 *
 *  Example 1: m < n
 *
 *  A = [ 2 3 5 ;
 *        7 8 9 ];
 *        
 *  pinv(A) = [ -0.399586       0.207039;
 *              -0.169772       0.113872;
 *              0.461698        -0.151139 ]
 *
 *  Example 2: m > n
 *
 *  A = [ 4  5  7;
 *        8  9  2;
 *        3  2  1;
 *        6  5  4 ]
 *
 *  pinv(A) = [ -0.199137       -0.149892       0.303379       0.347592;
 *               0.143494        0.267937      -0.271316      -0.317254;
 *               0.148239       -0.10647       -0.00273185     0.0445004 ]
 *
 *
 *  Example 3: m = n 
 *
 *  A = [ 2   5   9  13;
 *        2   6  10  14;
 *        2   7  13  21;
 *        2   8  18  40 ];
 *
 *  pinv(A) = [  1.66667        0.5     -2        0.333333;
 *              -1.83333        3       -1.5      0.333333;
 *               1             -2.5      2       -0.5;
 *              -0.166667       0.5     -0.5      0.166667 ]
 *
 *  
 *  Author       : Swagat Kumar
 *  Email        : swagatk@iitk.ac.in
 *  Affiliation  : Institute  : Indian Institute of Technology Kanpur, India
 *  Date         : 29 December 2006, Friday
 *  ========================================================================= */

#include <cmath>
#include<iostream>
#include <numroute.h>

using namespace std;

int main()
{
  double **A,  **AI, *x, *b;
  //int m = 2, n = 3;      // Example 1
  int m = 4, n = 3;      // Example 2
  //int m = 4, n = 4;      // Example 3
  
  int i, j, maxdim;

  maxdim = MAX(m,n);
  
  // Generate the matrices for my equation
  //
 
  x = new double [n];
  b = new double [m];

  A = new double *[m];
  for (i = 0; i < m; i++) 
    A[i] = new double[n];
  
  AI = new double *[maxdim];
  for(i = 0; i < maxdim; i++)
    AI[i] = new double [maxdim];


  //-------------------------------------------------
  // Example 1: m < n
  //A[0][0] = 2; A[0][1] = 3; A[0][2] = 5;
  //A[1][0] = 7; A[1][1] = 8; A[1][2] = 9;

  // Example 2: m > n
  A[0][0] = 4; A[0][1] = 5; A[0][2] = 7;
  A[1][0] = 8; A[1][1] = 9; A[1][2] = 2;
  A[2][0] = 3; A[2][1] = 2; A[2][2] = 1;
  A[3][0] = 6; A[3][1] = 5; A[3][2] = 4; 
  b[0] = 3; b[1] = 2; b[2] = 1; b[3] = 5;
 

  // Example 3: m = n
  //A[0][0] = 2; A[0][1] =  5; A[0][2] =  9; A[0][3] = 13;
  //A[1][0] = 2; A[1][1] =  6; A[1][2] = 10; A[1][3] = 14;
  //A[2][0] = 2; A[2][1] =  7; A[2][2] = 13; A[2][3] = 21;
  //A[3][0] = 2; A[3][1] =  8; A[3][2] = 18; A[3][3] = 40;
  //------------------------------------------------------

  // Call the pseudo-inverse routine 


  pinv(A, AI, m, n);
  
  // Output the results
  cout << "\n\nInverse of Matrix  = \n";
  for (i = 0; i < n; i++) 
  {
    for(j = 0; j < m; j++)
      cout << AI[i][j] << "\t";
    cout << endl;
  }

  cout << "\n\n Solution: ";
  for(i = 0; i < n; i++)
    cout << x[i] << "\t";
  cout << endl;
  return 0;

}
 
