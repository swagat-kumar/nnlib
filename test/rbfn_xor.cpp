 /* Testing RBFN Program for XOR function
 * Date: 13 September 2007 Thursday
 *
 *
 * Remark: Tested for both TPS and Gaussian activation function
 *
 * Date: 28 February 2009, Saturday
 * Output activation function is incorporated ... 
 * ------------------------------------------------------------- */

#include<iostream>
#include<nnet.h>
#include<fstream>
#include <cstdlib>
#include <cmath>

using namespace std;
using namespace nnet;

#define Np 4
#define Ni 2
#define No 1

#define Nh 4
#define Nmax 10000

//#define TPS
//#define RLS

int main()
{

  int n;
  double x[Ni], y[No], yd[No], s1;

  //Initial parameters
  //param[3] = {max_input, min_input, pinit}
  // pinit is necessary for RLS 


  //Number of neurons in each layer
  int N[3] = {Ni, Nh, No};

#ifdef TPS
  double act[2] = {7, 1.0};
  double param[3] = {1, 0, 100};
#else
  double act[2] = {5, 0.5};
  double param[3] = {1, 0, 10};
#endif


#ifdef RLS
  ofstream f1("error_rls1.txt");
#else
  ofstream f1("error_bp1.txt");
#endif

  //Assumes linear activation function at output
  rbfn A(N, act, param); 

#ifdef RLS
  double sig_init = 1.0, sig_fin = 0.1;
  double eta_init = 0.5, eta_fin = 0.1;
  double eta, sigma;
  A.wt_init(0.01,0);
#endif

  //Training
  double rmse = 0.0;
  for(n = 1; n <= Nmax; n++)
  {
    //Generate input-output data
    if(((double)rand()/RAND_MAX)>0.5) 
      x[0]=0.1;
    else 
      x[0]=0.9;

    if((double)rand()/(RAND_MAX)>0.5) 
      x[1]=0.1;
    else 
      x[1]=0.9;


    if(x[0]==0.1 && x[1]==0.1) yd[0]=0.1;
    if(x[0]==0.1 && x[1]==0.9) yd[0]=0.9;
    if(x[0]==0.9 && x[1]==0.1) yd[0]=0.9;
    if(x[0]==0.9 && x[1]==0.9) yd[0]=0.1;

#ifdef RLS
    sigma = sig_init * pow( (sig_fin / sig_init), (n / (double)Nmax) );
    eta = eta_init * pow( (eta_fin / eta_init), (n / (double)Nmax) );

    A.ksom_cluster(x, eta, sigma);
    A.network_output(x, y);
    A.rls_wt_update(yd, 1.0);

#else
    A.network_output(x, y);
    A.backprop(yd, 0.1, 0.1);
#endif

    //Instantaneous error
    s1 = (yd[0] - y[0]) * (yd[0] - y[0]);

    //RMS error
    rmse += s1;

    f1 << sqrt(rmse/n) << endl;
  }

  //Testing

  x[0] = 0.1; x[1] = 0.1; yd[0]= 0.1;
  A.network_output(x, y); 
  cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;

  x[0] = 0.1; x[1] = 0.9; yd[0]= 0.9;
  A.network_output(x, y); 
  cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;

  x[0] = 0.9; x[1] = 0.1; yd[0]= 0.9;
  A.network_output(x, y); 
  cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;

  x[0] = 0.9; x[1] = 0.9; yd[0]= 0.1;
  A.network_output(x, y); 
  cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;


  f1.close();
  return 0;
}
    

      
      

  


 
