// IMPLEMENTING XOR GATE WITH NN-BACKPROP and MLP
// Date: 2004-09-07 10:32 
// Author: Swagat Kumar (swagatk@iitk.ac.in)
//-----------------------------------------------------------
#include<iostream>
#include<fstream>
#include<cmath>
#include "nnet.h"

using namespace std;
using namespace nnet;

int main()
{
  double X[3], U[5][3], YD[5][2],Yd[2],Y[2],e,e1,rmse;
  int N[3]={2,5,1}; 
  int i,k;

  ofstream f1("si.dat");

  //training patterns (can also be read from a file)
  U[1][1]=-0.9, U[1][2]= -0.9, YD[1][1]= -0.9; //for unipolar values change
  U[2][1]=-0.9, U[2][2]=  0.9, YD[2][1]=  0.9; //activation function in defn.cpp  
  U[3][1]= 0.9, U[3][2]= -0.9, YD[3][1]=  0.9; //from bipolar to unipolar sigmoid
  U[4][1]= 0.9, U[4][2]= 0.9, YD[4][1]= -0.9; 

  //create an MLP object
  rbfn A(N, 5, 1,-1);

  //training
  k=1;
  do{
    cout<<k<<endl;
    e=0.0;
    for(i=1;i<=4;i++){
      X[0]=U[i][1], X[1]=U[i][2], Yd[0]=YD[i][1];
      
      //apply input X and find the output Y of the NN
      A.network_output(X,Y);

      //output error for each pattern.
      e1=pow((Yd[0]-Y[0]),2);

      //cumulative error
      e+=e1;
      
      //instantaneous weight update
      A.backprop(Yd, 0.5, 0.5);
      
      //print error in a file
      f1<<e1<<endl;
      k++;
    }
    //rms error per epoch
    rmse=sqrt(e/4);
  }while(rmse>0.01);
  
  cout<<"\n\nrms error per epoch="<<rmse<<endl<<endl;
  
  //checking the network
  for(i=1;i<=4;i++){
    X[0]=U[i][1], X[1]=U[i][2], Yd[0]=YD[i][1];
     A.network_output(X,Y);
     cout<<X[1]<<"\t"<<X[2]<<"\t"<<Yd[0]<<"\t"<<Y[0]<<endl;
  }
  f1.close();

}
//-------------------------------------------     
//end of main program
