/* Demonstration of LF-I Algorithm
 * Date: 29 April 2009, Wednesday
 * -------------------------------------- */

#include <iostream>
#include <fstream>
#include <cmath>
#include <nnet.h>
#include <utility.h>

using namespace std;
using namespace nnet;

#define Np 4
#define Ni 2
#define Nh 4
#define No 1

int main()
{

  int n,p;
  double x[Ni], y[No], yd[No], Px[Np][2], Py[Np], s1;

  ofstream f1("data.txt");

  //Training patterns
  Px[0][0] = 0.1; Px[0][1] = 0.1; Py[0] = 0.1;
  Px[1][0] = 0.1; Px[1][1] = 0.9; Py[1] = 0.9;
  Px[2][0] = 0.9; Px[2][1] = 0.1; Py[2] = 0.9;
  Px[3][0] = 0.9; Px[3][1] = 0.9; Py[3] = 0.1;

  // Define a Feed-forward network
  int N[3] = {Ni, Nh, No};
  double af[3] = {2, 1, 0};   //sigmoid
  double oa[3] = {2, 1, 0};   //sigmoid

  ffn A(N, 3, af, oa, 0);     // unipolar


  A.wt_init(0.1, 0.0);

  cout << "A = " << A << endl;
  cout << "Press Enter to start training" << endl;
  getchar();
  //Training
  n = 1;
  do
  {
    cout << "epochs = " << n << endl;
    s1 = 0.0;
    for(p = 0; p < Np; p++)
    {
      x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

      A.network_output(x, y);
      A.wt_jacobian();

      A.lf1_update(yd, 0.55);
      s1 += (yd[0] - y[0]) * (yd[0] - y[0]);

    }

    s1 = s1/Np;

    f1 << n << "\t" << sqrt(s1) << endl;

    n = n + 1;

  }while(sqrt(s1) > 0.01);

  //Testing
  for(p = 0; p < Np; p++)
  {
    x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];
    A.network_output(x, y); 

    cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;
  }


  f1.close();
  return 0;
}

