/* ------------------------------------------
 * Test for Runge-Kutta integration routine
 * Date: 30 February 2009 Tuesday
 * Status: success
 * Remark: Plot the file "output.txt"
 * How to compile: make rk
 * ----------------------------------------- */

#include<iostream>
#include<fstream>
#include<matrix.h>
#include<cmath>

using namespace std;

void slm(double *xdot, double *x, double t, void *param)
{
  double *u = (double*)param;
  xdot[0] = x[1];
  xdot[1] = -10 * sin(x[0]) + u[0];
}

int main()
{
  ofstream f1("output.txt");

  double dt = 0.01, u[1];
  double x[2] = {-1.0, 0.0};
  double xdot[2] = {0.0, 0.0};

  for(double t = 0.0; t < 5; t = t+dt)
  {
    u[0] = - 2 * x[0] - 2 * x[1];
    runge_kutta(xdot, x, (void*)u, t, dt, 2, slm);
    f1 << t << "\t" << x[0] << "\t" << x[1] << "\t" << u[0] << endl;
  }//t-loop

  f1.close();
  return 0;
}

