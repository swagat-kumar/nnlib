/* TESTING RBFN FOR APPROXIMATING SINE FUNCTION
 * USING RECURSIVE LEAST SQUARE ALGORITHM
 * DATE: 13 SEPTEMBER 2007 THURSDAY
 * DATE: 24 April 2009, Friday
 *
 * ---------------------------------------*/
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cmath>
#include<nnet.h>

#define pi 3.141592
#define NH 20
#define NIP 1
#define NOP 1
#define Nmax 50000

//#define RLS_KSOM
//#define BP_KSOM
#define BP

using namespace std;
using namespace nnet;

int main()
{
#ifdef RLS_KSOM
  ofstream fe("error_rls.txt");
  ofstream fo("output_rls.txt");
#endif

#ifdef BP_KSOM
  ofstream fe("error_bp1.txt");
  ofstream fo("output_bp1.txt");
#endif

#ifdef BP
  ofstream fe("error_bp2.txt");
  ofstream fo("output_bp2.txt");
#endif

  ofstream fci("center_init.txt");
  ofstream fc("center.txt");

  int N[3] = {1, NH, 1};

  double **cv;

  cv = new double *[NH];
  for(int i = 0; i < NH; i++)
    cv[i] = new double [NIP];

  //For gaussian activation
  double act[2] = {5, 1.0};
  double param[3] = {1.6, -1.6, 5};

  rbfn A(N, act, param); 
  A.wt_init(0.01,0);

  A.read_centers(cv);

  double x[NOP], xd[NOP], theta[NOP];

  for(int i = 0; i < NH; i++)
  {
    for(int j = 0; j < NIP; j++)
      fci << cv[i][j] << "\t";
    fci << endl;
  }

  double sigw_init = 2, sigw_fin = 0.05;
  double etaw_init = 0.4, etaw_fin = 0.1;     
  double eta_w, sigma_w;         

  double rmse = 0.0;
  for(int n = 1; n <= Nmax; n++)
  {
    theta[0] = -pi + 2 * pi * rand()/(double)RAND_MAX;

    xd[0] = sin(theta[0]);

    //necessary for Ksom clustering
    sigma_w = sigw_init * pow( (sigw_fin / sigw_init), (n / (double)Nmax) );
    eta_w = etaw_init * pow( (etaw_fin / etaw_init), (n / (double)Nmax) );      

    
    A.network_output(theta, x);

#ifdef RLS_KSOM
    A.ksom_cluster(theta, eta_w, sigma_w); 
    A.rls_wt_update(xd, 1.0);
#endif

#ifdef BP_KSOM
    A.ksom_cluster(theta, eta_w, sigma_w); 
    A.bp_wt_update(xd, 0.1);
#endif

#ifdef BP
    A.backprop(xd, 0.1, 0.2);
#endif

    rmse += pow((xd[0] - x[0]), 2.0);
    fe << sqrt(rmse/n) << endl;
  }

  A.read_centers(cv);
  for(int m = 0; m < NH; m++)
  {
    for(int q = 0; q < NIP; q++)
      fc << cv[m][q] << "\t";
    fc << endl;
  }



  //testing data

  for(double th = -pi; th <= pi; th = th+0.02)
  {
    theta[0] = th;
    xd[0] = sin(th);

    A.network_output(theta, x);
    fo << theta[0] << "\t" << xd[0] << "\t" << x[0] << endl;
  }
  fo.close();
  fc.close();
  fe.close();
  fci.close();

  return 0;
}
