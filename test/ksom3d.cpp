/* KSOM for learning Gabor function
 * Date: 22 April 2009, Wednesday
 * ------------------------------- */

#include<iostream>
#include<cmath>
#include<cstdlib>
#include<fstream>
#include<nnet.h>


using namespace std;
using namespace nnet;


int main()
{
  const int Nmax = 50000;
  double X[3];
  int N[3] = {7, 7, 7};

  double sigma, sigma_init = 1.0, sigma_final = 0.4;
  double eta, eta_init = 0.7, eta_final = 0.05;

  //Define a KSOM network
  ksom A(3, N, 3);
  cout << A << endl;

  cout << "Press Enter to Continue .." << endl;
  getchar();

//  A.initialize_parameters(0.5, -0.5);

  A.save_parameters("wtinit.txt");

  ofstream f1("gabor.txt");

  double x1, x2, y;

  for(x1 = -0.5; x1 <= 0.5; x1 = x1 + 0.01)
    for(x2 = -0.5; x2 <= 0.5; x2 = x2 + 0.01)
    {
      y = exp(-2 * (x1 * x1 + x2 * x2)) * cos(2 * M_PI * (x1 + x2)) / (0.5 * M_PI);
      f1 << x1 << "\t" << x2 << "\t" << y << endl;
    }
  f1.close();

    

 // Training starts here
   for(int n = 1; n <= Nmax; n++)
   {
      cout << "n = "<< n << endl;

      X[0] = -0.5 + 1.0 * rand()/(double)RAND_MAX;
      X[1] = -0.5 + 1.0 * rand()/(double)RAND_MAX;
      X[2] = exp(-2*(X[0] * X[0] + X[1] * X[1])) * cos(2 * M_PI * (X[0] + X[1]))/(0.5 * M_PI);

      
      
      //Spread of Gaussian function
      sigma = sigma_init * pow( (sigma_final / sigma_init), (n / (double)Nmax) );

      //Learning rate
      eta = eta_init * pow( (eta_final / eta_init), (n / (double)Nmax) );     

      A.create_clusters(eta, sigma, X);
   }

   A.save_parameters("wtfinal.txt");

   return 0;
}




