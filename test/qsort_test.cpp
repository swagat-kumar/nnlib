/* Testing Qsort Algorithm
 * Date: 08 December 2009, Tuesday
 * Status:
 * -------------------------------------- */
#include<iostream>
#include<utility.h>

using namespace std;

int main()
{
  srand(time(NULL));
  double *x = array(20);

  cout << "x = "<<endl ;
  for(int i = 0; i < 20; i++)
  {
    x[i] = rand()/(double)RAND_MAX;
    cout << x[i] << "\n";
  }
  cout << endl;

  qsortD(x, 20, "ascend");

  cout << "Sorted x = "<<endl ;
  for(int i = 0; i < 20; i++)
    cout << x[i] << "\n";

  free_memory(x);
  return 0;
}
