 /*-------------------------------------------------------------------
     Learning XOR function using RBFN and BackPropagation Algorithm
    Copyright (C) 2007 Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

  -------------------------------------------------------------------- */


 /* Testing RBFN Program for XOR function
 * Date: 13 June 2007 Friday
 * Status: OK
 * --------------------------------------- */
#include<iostream>
#include<nnet.h>
#include<fstream>

using namespace std;
using namespace nnet;

#define Np 4
#define Ni 2
#define No 1

int main()
{
  
  int n,p;
  double x[Ni], y[No], yd[No], Px[Np][2], Py[Np], s1;

  int N[3] = {2, 5, 1};

  ofstream f1("data.txt");

  //Training patterns
  Px[0][0] = 0.1; Px[0][1] = 0.1; Py[0] = 0.1;
  Px[1][0] = 0.1; Px[1][1] = 0.9; Py[1] = 0.9;
  Px[2][0] = 0.9; Px[2][1] = 0.1; Py[2] = 0.9;
  Px[3][0] = 0.9; Px[3][1] = 0.9; Py[3] = 0.1;
  
  // Define a Radial Basis Function
  rbfn A(N, 5, 1, 0); 


  //Training
  for(n = 1; n <= 50000; n++)
  {
    s1 = 0.0;
    for(p = 0; p <= Np; p++)
    {
      x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

      A.network_output(x, y);
      A.backprop(yd, 0.5, 0.5);

      s1 += (yd[0] - y[0]) * (yd[0] - y[0]);
    }
    s1 = s1/Np;


    f1 << n << "\t" << s1 << endl;
  }

  //Testing
  
  for(p = 0; p < Np; p++)
  {
     x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

     A.network_output(x, y); 

     cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;
  }

  f1.close();
  return 0;
}
    

      
      

  


 
