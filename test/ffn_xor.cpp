/* Testing FFN for learning xor function
 *  Date: 30 June 2007 Saturday
 *  Status : Success
 * -------------------------------------- */

#include <iostream>
#include <fstream>
#include<cmath>
#include <nnet.h>

using namespace std;
using namespace nnet;

#define Np 4
#define Ni 2
#define No 1

int main()
{

  int n,p;
  double x[Ni], y[No], yd[No], Px[Np][2], Py[Np], s1;

  ofstream f1("data.txt");

  //Training patterns
  Px[0][0] = 0.1; Px[0][1] = 0.1; Py[0] = 0.1;
  Px[1][0] = 0.1; Px[1][1] = 0.9; Py[1] = 0.9;
  Px[2][0] = 0.9; Px[2][1] = 0.1; Py[2] = 0.9;
  Px[3][0] = 0.9; Px[3][1] = 0.9; Py[3] = 0.1;

  
  // Define a Feed-forward network
  int N[3] = {2, 4, 1};
  double af[3] = {2, 1, 0};  // unipolar, sigmoid 
  double oa[3] = {2, 1, 0};

  ffn A(N, 3, af, oa, 0); 

  cout << "Network Details = " << A << endl;

  A.display_var("wt");
  A.display_var("io");
  //A.display_var("jyu");
  //A.display_var("jyw");
  //A.display_var("idx");
  //getchar();

  //Training
  for(n = 1; n <= 10000; n++)
  {
    s1 = 0.0;
    for(p = 0; p < Np; p++)
    {
      x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

      A.network_output(x, y);


      A.backprop(yd, 0.5);

      s1 += (yd[0] - y[0]) * (yd[0] - y[0]);
    }
    s1 = s1/Np;


    f1 << n << "\t" << sqrt(s1) << endl;
  }

  //Testing
  
  for(p = 0; p < Np; p++)
  {
     x[0] = Px[p][0]; x[1] = Px[p][1]; yd[0]= Py[p];

     A.network_output(x, y); 

     cout<< x[0] << "\t" << x[1] << "\t" << yd[0] << "\t" << y[0] << endl;
  }
  f1.close();

  return 0;
}
