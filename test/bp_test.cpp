#include<iostream>
#include<fstream>
#include "nnet.h"

using namespace std;
using namespace nnet;

#define P 4

int main()
{

  double inp[2], yd[1], out[1];
  double x[P][2], y[P][1], mse;

  int n, Nmax = 30000, p, bf = 0;
  int N[3] = {2, 3, 1};
  double act[2] = {2, 1}, oact[2] = {2, 1};

  ofstream f1("data.txt");
  
  //training patterns
  x[0][0] = 0; x[0][1] = 0; y[0][0] = 0;
  x[1][0] = 0; x[1][1] = 1; y[1][0] = 1;
  x[2][0] = 1; x[2][1] = 0; y[2][0] = 1;
  x[3][0] = 1; x[3][1] = 1; y[3][0] = 0;
  
  mlp3 A(N, act, oact, bf);

  for( n = 1; n <= Nmax; n++)
  {
    mse = 0.0;
    for(p = 0; p < P; p++)
    {
      inp[0] = x[p][0]; inp[1] = x[p][1];
      yd[0] = y[p][0];

      A.network_output(inp, out);

      A.backprop(yd, 0.6);

      mse += (yd[0] - out[0]) * (yd[0] - out[0]);
    }
    mse = mse / P;

    f1 << mse << endl;
  }

  //testing
   for(p = 0; p < P; p++)
   {
     inp[0] = x[p][0]; inp[1] = x[p][1];
     yd[0] = y[p][0];

     A.network_output(inp, out);

     cout << inp[0] << "\t" << inp[1] << "\t" << yd[0] << "\t" << out[0] << endl;
   }

  f1.close();
  return 0;
}
  
    

    

      
      

      
      
      

  

