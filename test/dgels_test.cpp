 /*
  * This is an example program using the header file DGELS Routines,
  * implementation of the LAPACK linear least square solver.  See that
  * header file for more documentation on its use.
  * 
  * It solves three problems:
  *
  * Example 1: m < n
  * 
  *
  *  A = [ 2 3 5 ;     
  *        7 8 9 ];
  *
  *  b = [4; 5];
  *
  *  x =  [-0.563147   -0.109731       1.0911]
  *  xi = [-0.563147   -0.109731       1.0911]
  *  
  *  Example 2: m > n
  *
  *  A = [ 4  5  7;
  *        8  9  2;
  *        3  2  1;
  *        6  5  4 ]
  *        
  *  b = [ 3;  2;  1; 5 ]
  *  
  *  x = [ 1.14414     -0.891229       0.451546]
  *  xi = [ 1.14414     -0.891229       0.451546]
  * 
  *
  *  Example 3: m = n 
  *
  *  A = [ 2   5   9  13;
  *        2   6  10  14;
  *        2   7  13  21;
  *        2   8  18  40 ];
  *
  *  b = [ 2; 4; 8; 16]
  *
  *  x = [-5.33333    1.66667 2.49456e-14     0.333333]
  *
  *  xi = [ -5.33333    1.66667 2.30926e-14     0.333333 ]
  *
  *  
  * Note: x is the "minimum-norm" solution obtained using dgels, dgelsy and
  * dgelss and xi is the solution computed using pseudoinverse = pinv(A) * b
  * The solution matches with those obtained from octave functions "gls" or
  * "ols".
  *  
  *  
  *  Author       : Swagat Kumar
  *  Email        : swagatk@iitk.ac.in
  *  Affiliation  : Institute  : Indian Institute of Technology Kanpur, India
  *  URL          : http://home.iitk.ac.in/~swagatk/
  *  Date         : 30 December 2006, Saturday              
  *  
  *  ============================================================== */

/* -----------------
 * UPDATES: 
 *
 * DAte: 05 October 2008 Sunday
 * dgels function is tested
 *
 * ---------------------------------- */

#include <cmath>
#include<iostream>
#include <cstdio>
#include <cstdlib>
#include <matrix.h>

using namespace std;



int main()
{
  double **A, *b, *x;
  int m = 2, n = 3;      // Example 1
  //int m = 4, n = 3;      // Example 2
  //int m = 4, n = 4;      // Example 3
  //int i, j;
  int maxdim;
  
  // Generate the matrices for my equation

  maxdim = MAX(m,n);
  
  A = new double*[m];
  for (int i = 0; i < m; i++) 
    A[i] = new double[n];
  
  b = new double[m];
  x = new double [maxdim];      // Solution

  //-------------------------------------------------
  // Example 1: m < n
  A[0][0] = 2; A[0][1] = 3; A[0][2] = 5;
  A[1][0] = 7; A[1][1] = 8; A[1][2] = 9;
  b[0] = 4; b[1] = 5;

  cout << "A = " << endl;
  display(A, 2, 3);

  cout << "b = " << endl;
  display(b, 2);

  // Example 2: m > n
  //A[0][0] = 4; A[0][1] = 5; A[0][2] = 7;
  //A[1][0] = 8; A[1][1] = 9; A[1][2] = 2;
  //A[2][0] = 3; A[2][1] = 2; A[2][2] = 1;
  //A[3][0] = 6; A[3][1] = 5; A[3][2] = 4; 
  //b[0] = 3; b[1] = 2; b[2] = 1; b[3] = 5;
  
 

  // Example 3: m= n
  //A[0][0] = 2; A[0][1] =  5; A[0][2] =  9; A[0][3] = 13;
  //A[1][0] = 2; A[1][1] =  6; A[1][2] = 10; A[1][3] = 14;
  //A[2][0] = 2; A[2][1] =  7; A[2][2] = 13; A[2][3] = 21;
  //A[3][0] = 2; A[3][1] =  8; A[3][2] = 18; A[3][3] = 40;
  //b[0] = 2; b[1] = 4; b[2] = 8; b[3] = 16;
  //------------------------------------------------------

  // Call the LAPACK solver


  //dgelss_pi(A, b, m, n, x);
  dgels(A, b, m, n, x);
  //dgelss(A, b, m, n, x);
  //dgelsy(A, b, m, n, x);
  
  // Output the results
  cout << "X = ";
  for (int i = 0; i < n; i++) 
    cout << x[i] << "\t"; 
  cout << "\n";

  return 0;

}
 
