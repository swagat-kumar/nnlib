/**********************************
 * This program test the overloaded operators
 * and function of default constructor
 *
 * Date: 13 December 2009, Sunday
 * Status: Perfectly OK
 * It was also checked with Valgrind for memory leakage. There is
 * no memory leakage.
 * *************************************/
#include<iostream>
#include<nnet.h>
#include<cmath>

//#define RBFN
//#define FFN
#define KSOM

using namespace std;
using namespace nnet;

int main()
{
  /***********************************
   *  RBFN 
  ***********************************/

#ifdef RBFN
  rbfn A; //Calls the default constructor
  cout << "Empty RBF network (A) =" << endl;
  cout << A; 

  int N[3] = {2, 5, 2};
  double af[2] = {5, 0.1};
  double param[3] = {1.0, 0.0, 0.0};

  rbfn B(N, af, param); // Create a new RBFN network

  cout << "Non-empty network (B) = " << endl;
  cout << B ;

  // Assign B to A
  A = B;
  cout << "Details of network A = " << endl;
  cout << A;
#endif

  /**************************************
   *   FFN
   *   Tested on 24 March 2010, Wednesday
   ***************************************/

#ifdef FFN

  ffn A; //Calls the default constructor
  cout << "Empty FFN network (A) =" << endl;
  cout << A; 

  int N[3] = {2, 5, 2};
  double af[3] = {1, 1.0, 0.0};
  int L = 3;

   ffn B(N, L, af); // Create a new FFN network

  cout << "Non-empty network (B) = " << endl;
  cout << B ;

  // Assign B to A
  A = B;
  cout << "Details of network A = " << endl;
  cout << A;
#endif

  /*******************************************
   * KSOM
   * Date: 24 March 2010, Wednesday
   *
   * Last check: 29 March 2010, Monday
   * Its working fine ...
   * ********************************************/
#ifdef KSOM

  ksom A;

  cout << "Empty KSOM network (A) = " << endl;
  cout << A;


  int N[3] = {7, 7, 7};


  //Define a KSOM network
  ksom B(3, N, 3);

  cout << "Non-empty Network (B)" << endl;
  cout << B << endl;

  //Assign B to A
  A = B;
  cout << "Details of network A = " << endl;
  cout << A;

#endif



  return 0;
}

