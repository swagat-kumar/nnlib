/* learning 8-to-3 Encoder function using Feed-forward network
 *
 *  -- 5-layer Network 
 *  -- Test for Weight Jacobian 
 *  -- BP Algorithm
 *
 * Date: 17 May 2009, Sunday
 * Status: success
 * -------------------------------------- */

#include <iostream>
#include <fstream>
#include <cmath>
#include <nnet.h>
#include <utility.h>

using namespace std;
using namespace nnet;

#define Np 8
#define Ni 8
#define No 3


int main()
{
  double *x = new double [Ni];
  double *y = new double [No];
  double *yd = new double [No];

  ofstream f1("error.txt");

  double Px[64] = {0,  0,  0,  0,  0,  0,  0,  1,  
    0,  0,  0,  0,  0,  0,  1,  0,  
    0,  0,  0,  0,  0,  1,  0,  0,  
    0,  0,  0,  0,  1,  0,  0,  0,  
    0,  0,  0,  1,  0,  0,  0,  0,  
    0,  0,  1,  0,  0,  0,  0,  0,  
    0,  1,  0,  0,  0,  0,  0,  0,  
    1,  0,  0,  0,  0,  0,  0,  0 };

  double Py[24] = { 0,  0,  0, 
    0,  0,  1, 
    0,  1,  0, 
    0,  1,  1, 
    1,  0,  0, 
    1,  0,  1, 
    1,  1,  0, 
    1,  1,  1}; 

  cout << "Px = " << endl;
  DisplayArray(Px, Np, Ni);

  cout << "Py = " << endl;
  DisplayArray(Py, Np, No);


  // Define a Feed-forward network
  int N[5] = {Ni, 5, 5, 5, No};
  double af[3] = {2, 1, 0};   //sigmoid
  double oa[3] = {2, 1, 0};   //sigmoid

  ffn A(N, 5, af, oa, 0);     // unipolar

  A.wt_init(1.0, 0.0);

  int wm = A.wmax();
  double **jyw = array(No, wm);
  double *delta_w = array(wm);

  cout << "A = " << A << endl;

  cout << "\n-------------------\nBP Training\n------------" << endl;


  cout << "\nTraining in Progress .. Wait" << endl;

  //Training
  int n = 0;
  double eta = 0.9;
  double se = 0.0;
  do
  {
    //cout << "n = " << n << endl;
    se = 0.0;
    for(int p = 0; p < Np; p++)
    {
      for(int i = 0; i < Ni; i++)
        x[i] = Px[p * Ni + i];

      for(int i = 0; i < No; i++)
        yd[i] = Py[p * No + i];

      A.network_output(x, y);
      A.wt_jacobian(jyw);

      for(int i = 0; i < wm; i++)
      {
        double s1 = 0.0;
        for(int p = 0; p < No; p++)
          s1 += jyw[p][i] * (yd[p] - y[p]);
        delta_w[i] = eta * s1;
      }

      //update weights
      A.ExternalWeightUpdate(delta_w);

      for(int i = 0; i < No; i++)
        se += pow((yd[i] - y[i]), 2.0);
    }

    f1 << n << "\t" << sqrt(se/(No*Np)) << endl;
    //cout << n << "\t" << sqrt(se/(No*Np)) << endl;

    n = n + 1;

  }while(sqrt(se/(No*Np)) > 0.005);

  //----------------------
  // TEST 
  // ---------------------

  double se2 = 0.0;
  for(int p = 0; p < Np; p++)
  {
    for(int i = 0; i < Ni; i++)
    {
      x[i] = Px[p * Ni + i];
      cout << x[i] << "\t" ;
    }

    for(int i = 0; i < No; i++)
    {
      yd[i] = Py[p * No + i];

      cout << yd[i] << "\t";
    }


    A.network_output(x, y); 

    double s1 = 0.0;
    for(int i = 0; i < No; i++)
    {
      s1 += pow((yd[i] - y[i]), 2.0);
      cout << y[i] << "\t";
    }
    cout << endl;
    se2 += s1;


  }
  cout << "rms error = " << sqrt(se2/(No*Np)) << endl;  

  delete [] x;
  delete [] y;
  delete [] yd;
  delete [] delta_w;
  free_memory(jyw, No);


  f1.close();
  return 0;
}

