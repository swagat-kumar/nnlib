/* Learning 2-D sine curve using KSOM network
 * Remark: Plot the the files "wtinit.txt" and "wtfinal.txt" using
 * gnuplot as follows:
 *
 * $gnuplot> plot 'wtinit.txt' u 1:2 w p, 'wtfinal.txt' u 1:2 w p
 *
 * Last checked on : 29 March 2010, Monday
 * ------------------------------------------------------------------*/
#include<iostream>
#include<cmath>
#include<cstdlib>
#include<fstream>
#include<nnet.h>
#define PI 3.141512 

using namespace std;
using namespace nnet;


int main()
{
  const int Nmax = 50000;
  double X[2];
  int N[2] = {10, 10};

  double sigma, sigma_init = 1.0, sigma_final = 0.1;
  double eta, eta_init = 0.5, eta_final = 0.05;

  //Define a KSOM network
  ksom A(2, N, 2);
  cout << A << endl;


  A.save_parameters("wtinit.txt");

 // Training starts here
   for(int n = 1; n <= Nmax; n++)
   {
      cout << "n = "<< n << endl;
      
      X[0] = (-1 + 2*(rand() / (double)RAND_MAX)) * PI; // denormalized X coordinate
      X[1] = sin (X[0]);    // Y = sin x, normalized y coordinate
      X[0] = X[0]/PI;        // normalized x coordinate

      //Spread of Gaussian function
      sigma = sigma_init * pow( (sigma_final / sigma_init), (n / (double)Nmax) );

      //Learning rate
      eta = eta_init * pow( (eta_final / eta_init), (n / (double)Nmax) );     

      A.create_clusters(eta, sigma, X);
   }

   A.save_parameters("wtfinal.txt");

   return 0;
}




