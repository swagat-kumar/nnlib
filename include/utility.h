#ifndef _UTILITY_H
#define _UTILITY_H

#include<iostream>
#include<cstdlib>
#include<string>

using namespace std;

//allocate memory for arrays
double* array(int m);
double* array(const double *a, int m);
double** array(int m, int n);
double** array(const double *a, int m, int n);
double*** array(int m, int n, int p);

int** Intarray(int m, int n);

//deallocate memory
void free_memory(double *x);
void free_memory(double **x, int m);
void free_memory(double ***x, int m, int n);
void free_memory(int **x, int m);

//display arrays
void DisplayArray(double **q,int row,int col);
void DisplayArray(double *q, int row, int col);
void DisplayArray(double *q, int col);
void DisplayIntArray(int **q, int row, int col, int sw=5);
void DisplayArray(int *q, int col, int sw=5);
void DisplayArray(int *q, int row, int col, int sw=5);

//normalize or denormalize a vector
// Date: 30 January 2009
void normalize(const double *y, double *yn, int m, const double *max, const double *min, int bpf=1);
void denormalize(const double *yn, double *y, int m, const double *max, const double *min, int bpf=1);

//Runge-kutta numerical integration
void runge_kutta(double *xdot, double *x,void *param, double t,double h,int n,
    void (*func_dxdt)(double*, double*, double, void*));


//Sort an array
int compare_double_ascend(const void *num1, const void* num2);
int compare_double_descend(const void *num1, const void* num2);
void qsortD(double *data, int N, const string option); 
int compare_integer_ascend(const void *num1, const void* num2);
int compare_integer_descend(const void *num1, const void* num2);
void qsortInt(int *data, int N, string option); 

//Wrappers for Lapack Routines
//Date: 28 April 2009, Tuesday

int maximum(int m, int n);
int minimum(int m, int n);
int dgesvd(double **A, int m, int n, double *s=NULL, double **U = NULL, double **VT = NULL);
int dgesvd(const double *A, int m, int n, double *s=NULL, double *U = NULL, double *VT = NULL);


#endif
