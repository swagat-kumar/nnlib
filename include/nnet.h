 /*--------------------------------------------------------------------

    NEURAL NETWORK LIBRARY FOR C/C++
    Copyright (C) 2007-2010   Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

-------------------------------------------------------------------------- */ 

#ifndef _NNET
#define _NNET                  
#include<iostream>
#include<string>
#include<gsl/gsl_rng.h>

using namespace std;

namespace nnet                                                                
{
  //=======================================================
  //General Functions
  class func
  {
    protected:
      const gsl_rng_type *T;
      gsl_rng *r;

    public:
      func(); 
      ~func();
      // Note: here 'x' refers to neuron input in all cases
      //Activation functions
      double sigmoid(double x,double a);
      double tansig(double x,double a);
      double purelin(double x,double a);
      double arctan(double x, double a);
      double gauss(double x,double sigma);
      double hyptan(double x, double a, double b);
      double tps(double x, double a = 1);
      double tps2(double x);
      double hypcos(double x);
      double tansigsq(double x, double a);
      double square(double x, double a);

      //Derivative of activation functions
      double dsigmoid(double x,double a);
      double dtansig(double x,double a);
      double dlin(double x,double a);
      double darctan(double x, double a);
      double dgauss(double x,double y);
      double dhyptan(double x, double a, double b);
      double dtps (double x, double a = 1.0);
      double dtps2(double x);
      double dhypcos(double x);
      double dtansigsq(double x, double a);
      double dsquare(double x, double a);

      //General functions
      int kdel(int i, int j);
      double ran1(long *idum);  //From NRUTIL 
      double active_func(double a[], double x);
      double dactive(double a[], double);

      //allocate memory for arrays
      double* array(int m);
      double** array(int m, int n);
      double*** array(int m, int n, int p);
      int** intarray(int m, int n);

      //Memory deallocation function
      void free_memory(double *x);
      void free_memory(int *x);
      void free_memory(double **x, int m);
      void free_memory(int **x, int m);
      void free_memory(double ***x, int m, int n);
      void free_memory(int ***x, int m, int n);
  };
  //======================================================

  //Feedforward network
  class ffn : public func
  {
    private:
      int *N, L, bf;
      double **X, **S, *Y, alpha, **Jyw, **Jyu;
      double act[3], oact[3];
      double ***W, *AW, bias;
      int **Wt_index, WM, mnpl, ***WI;
      int called_wj, called_ij;
      double rmax, rmin;
      void allocate_memory();
      void deallocate_memory();

    public:
      ffn();
      ffn(int NI[], int L, double act[]);
      ffn(int NI[], int L, double act[], double oact[], int bf, double bv=1.0);
      ffn(const ffn &a);
      ~ffn();
      void network_output(double ip[], double op[]);
      void backprop(double dop[], double eta); 
      void netinfo();
      void dybydw(double **dyw = NULL);
      void wt_jacobian(double **Jow = NULL);
      void input_jacobian(double **Joi = NULL);
      friend void ext_func(ffn *A, double *x);
      void display_var(string s);
      void wt_init(double max, double min);
      void get_parameters(double *wt_vec=NULL);
      void save_parameters(const char *filename);
      void load_parameters(const char *filename);
      void load_parameters(double *wt);
      int wmax(){ return WM; };
      void lf1_update(double yd[], double eta);
      void lf2_update(double yd[], double eta1, double eta2, double *wt1, double *wt2);
      void update_input_gd(double eta, double ydn[], double xn[]);
      void ExternalWeightUpdate(const double *dw);

      friend ostream& operator<<(ostream& stream, const ffn& a);
      ffn & operator=(const ffn& a);
  };                      


  // ===================================================================
  // MLP for a 3-layer network
  // ===================================================================

  class mlp3 : public func
  {
    int N[3], nw1, nw2, nw, bf, mnpl, nb1, nb2, nb;
    double *Xi, *Xh, *Xo, *Si, *Sh, *So;
    double **Jyw, **Jyx, **Jyb;
    double act[3], oact[3]; 
    double **W1, **W2, *W;
    double *BW1, *BW2, *BW, bias;
    int **WI1, **WI2;
    int called_wj, called_ij;

    void allocate_memory();
    void initialize_variables();

    public:
    mlp3(int N[]);
    mlp3(int N[], double act[], int bf = 1);
    mlp3(int N[], double act[], double oact[], int bf = 1);
    mlp3(int N[], double act[], double oact[], int bf, double bias);
    mlp3(const mlp3 &a); // copy constructor 
    ~mlp3();
    void network_output(double ip[], double op[]);
    void wt_jacobian(double **Jw=NULL);
    void input_jacobian(double **Jx=NULL);
    void wt_init(double max, double min);
    void gd_update(double yd[], double eta);
    void gd_update2(double yd[], double eta);
    void netinfo();
    int wmax();
    void display_var(string s);
    void get_parameters(double *wt);
    void load_parameters(double *wt);
    void lf1_update(double yd[]);

    //weight update algorithm
    void backprop(double dop[], double eta); //not defined for mlp3
    friend void ext_func(mlp3 *A, double *x);
  };

  // ======================================================================
  //Radial Basis functions
  //============================================
  class rbfn : public func
  {
    int NC,NIP,NOP, wm,Cmax,**Wt_index,**Cen_index;
    double **C,**W,*X,*Z,*S,*Y, *V, maxin,minin;
    double *AW, *AC;
    double **Jyu,**Jyw,**Jyc;
    double act[2], oact[2];          //activation flag
    int called_wj, called_ij, called_cj;
    double *Wt1, *Wt2; // needed for LF-II
    double **P, pinit;  //For RLS algorithm
    double *SIG;

    void allocate_memory();
    void deallocate_memory();
    void initialize_variables();

    public:
    rbfn(); //Default constructor
    rbfn(int N[3], double af[], double param[]);
    rbfn(int N[3], double af[2], double oa[], double param[]);
    rbfn(const rbfn &a);
    ~rbfn();
    void network_output(double ip[],double op[]);
    void backprop(double dop[],double eta_c,double eta_w, double eta_s=0);
    void wt_jacobian(double **Jow=NULL);
    void cen_jacobian(double **Joc=NULL);
    void input_jacobian(double **Joi=NULL);
    void wt_init(double max,double min);
    void cen_init(double max,double min);
    void update_input_gd(double eta, double ydn[], double xn[]);
    void update_input_gd2(double eta, double ydn[], double xn[], int input_vector_size);
    friend void ext_func(rbfn *A, double *x);
    void update_parm(double eta, double *dp, int cnt);

    void load_parameters(double *wt, double *cv, double *sigma=NULL);
    void get_parameters(double *wt, double *cv, double *sigma=NULL);
    void load_parameters(double **wt, double **cv=NULL);
    void get_parameters(double **wt, double **cv=NULL);
    void save_parameters(const char *filename);
    void load_parameters(const char *filename);
    int load_centers(double **CM); 
    void load_centers(const char *filename); 
    int read_centers(double **CM);
    void display_var(string s);

    void maxparm(int *wmax, int *cmax);
    void ksom_cluster(double ip[], double eta, double sigma);
    void bp_wt_update(double yd[], double eta);
    void rls_wt_update(double yd[], double lambda);
    void lf1_wt_update(double yd[], double mu);
    void LF_update(double yd[], double mu);
    void LF_update(double yd[], double mu, double lambda);
    void distributed_LF_update(double Yd[], double mu);
    friend ostream& operator<<(ostream& stream, const rbfn& a);
    rbfn& operator=(const rbfn & a); //member function
  };      

  //======================================================================
  // KOHONEN'S SELF ORGANIZING MAP : CLASS KSOM
  // ======================================================================

  class ksom : public func
  {
    private:
      int DIM, NI, NO, **idx, ***Ridx, NodeCnt;
      double *X, *Y, **C, **W, *S, *Z;
      double act[2];
      int N[3];

    public:
      ksom(); //Default Contsructor
      ksom(int dim, int nidx[3], int nip, double af[3]=NULL, int nop=0);
      ksom(const ksom &a);
      ~ksom();
      void allocate_memory();
      void deallocate_memory();
      void initialize_parameters(double max, double min);
      void network_output(const double x[], double y[]);
      void save_parameters(const char *filename);
      void load_parameters(const char *filename);
      void create_clusters(double eta, double sig, const double xd[]);
      friend ostream& operator<<(ostream& stream, const ksom& a);
      ksom& operator=(const ksom & a); //member function
  };

  //===============================================================================
  // FRIEND FUNCTIONS
  // ==============================================================================

  ostream& operator<<(ostream& os, const ffn& a);
  ostream& operator<<(ostream& os, const ksom& a);
  ostream& operator<<(ostream& os, const rbfn& a);

}
#endif
            
