/*--------------------------------------------------------------------

    Radial Basis Function Network (RBFN) Class Definitions
    Copyright (C) 2007-2010   Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------- */  
#include <iostream>
#include <fstream>
#include <iomanip>
#include<new>
#include<cmath>
#include <nnet.h>
#include<ctime>
#include<cstdlib>
#include <string>

using namespace nnet;
using namespace std;

/* ----------------------------------------
 *
 *       ALL INDICES START FROM 0
 *
 *------------------------------------- */

//Default Constructor
rbfn::rbfn()
{
  cout << "This is default constructor for RBFN Networks." << endl;
  cout << "This creates an empty RBFN network" << endl;
  cout << "You can use overloaded = operator to assign another RBFN to it" << endl;

  NIP = 0;
  NC = 0;
  NOP = 0;

  wm = NOP * NC;
  Cmax = NC * NIP;

  maxin = 0; // max input
  minin = 0; // min input
  pinit = 0; // Initial value of P matrix (RLS Algo);

  //flags to detect the Jacobian matrices
  called_ij = 0;
  called_wj = 0;
  called_cj = 0;

  //Activation vector for hidden nodes
  act[0] = 0.0;
  act[1] = 0.0;

  //Activation function for output layer
  oact[0] = 0.0;
  oact[1] = 0.0;

  //allocate memory
  C = NULL;
  W = NULL;
  X = NULL;
  Z = NULL;
  S = NULL;
  Y = NULL;
  V = NULL;
  AW = NULL;
  AC = NULL;
  Wt_index = NULL;
  Cen_index = NULL;
  Jyu = NULL;
  Jyc = NULL;
  Jyw = NULL;
  Wt1 = NULL;
  Wt2 = NULL;
  P = NULL;  // Needed for RLS algorithm
  SIG = NULL;
}
// ---------------------------------
// Constructor 2: Output layer is linear
// Note : No bias is being used.
// -------------------------------------

rbfn::rbfn(int N[], double af[], double param[])	
{
  NIP = N[0];
  NC = N[1];
  NOP = N[2];


  wm = NOP * NC;
  Cmax = NC * NIP;

  maxin = param[0]; // max input
  minin = param[1]; // min input
  pinit = param[2]; // Initial value of P matrix (RLS Algo);

  //flags to detect the Jacobian matrices
  called_ij = 0;
  called_wj = 0;
  called_cj = 0;

  // --------------------------------------
  // For gaussian function 
  // af = {5, sigma}
  //
  // f(x) = exp(-x^2/(2*sigma*sigma));
  //
  // For thin plate spline function
  //
  // af = {7, a}; a is constant
  //
  // f(x) =a^2*x^2*log(a*x)
  // --------------------------------------

  //Activation function for hidden nodes
  act[0] = af[0];
  act[1] = af[1];

  //Output nodes are linear
  oact[0] = 3; 
  oact[1] = 1;

  allocate_memory();
  initialize_variables();
}
//--------------------------------------------------
// Constructor 3
// Here an activation function for output layer can be set
// Date: 27 February 2009, Friday
// ------------------------------------------------
rbfn::rbfn(int N[], double af[], double oa[], double param[])	
{
  NIP = N[0];
  NC = N[1];
  NOP = N[2];


  wm = NOP * NC;
  Cmax = NC * NIP;

  maxin = param[0]; // max input
  minin = param[1]; // min input
  pinit = param[2]; // Initial value of P matrix (RLS Algo);

  //flags to detect the Jacobian matrices
  called_ij = 0;
  called_wj = 0;
  called_cj = 0;

  //Activation vector for hidden nodes
  act[0] = af[0];
  act[1] = af[1];

  // --------------------------------------
  // For gaussian function 
  // af = {5, sigma}
  //
  // f(x) = exp(-x^2/(2*sigma*sigma));
  //
  // For thin plate spline function
  //
  // af = {7, a}; a is constant
  //
  // f(x) =a^2*x^2*log(a*x)
  // --------------------------------------

  //Activation function for output layer
  oact[0] = oa[0];
  oact[1] = oa[1];

  allocate_memory();
  initialize_variables();
}
//----------------------------------------------------
// COPY CONSTRUCTOR FOR RBFN CLASS
// --------------------------------------------------

rbfn::rbfn(const rbfn &a)
{
  NIP = a.NIP;
  NOP = a.NOP;
  NC = a.NC;
  wm = a.wm;
  Cmax = a.Cmax;

  maxin = a.maxin;
  minin = a.minin;
  pinit = a.pinit;

  called_ij = a.called_ij;
  called_wj = a.called_wj;
  called_cj = a.called_cj;


  for(int i = 0; i < 2; i++)
  {
    act[i] = a.act[i];
    oact[i] = a.oact[i];
  }

  allocate_memory();

  //Needed for LF-II
  for(int i = 0; i < wm+Cmax; i++)
  {
    Wt1[i] = a.Wt1[i];
    Wt2[i] = a.Wt2[i];
  }

  //-------------------------------
  // Need for RLS
  for(int i = 0; i < NC; i++)
    for(int j = 0; j < NC; j++)
      P[i][j] = a.P[i][j];
  //---------------------------
  //Re-initializing centers
  for(int i = 0; i < NC; i++)
  {
    SIG[i] = a.SIG[i];  
    for(int j = 0; j < NIP; j++)
      C[i][j] = a.C[i][j];
  }

  for(int i = 0; i < wm; i++)
  {
    Wt_index[i][0] = a.Wt_index[i][0];
    Wt_index[i][1] = a.Wt_index[i][1];
  }

  for(int i = 0; i < Cmax; i++)
  {
    Cen_index[i][0] = a.Cen_index[i][0];
    Cen_index[i][1] = a.Cen_index[i][1];
  }

  //Re-initializing weights
  for(int i = 0; i < NOP; i++)
  {
    for(int j = 0; j < NC; j++)
      W[i][j] = a.W[i][j];
    for(int j = 0; j < NIP; j++)
      Jyu[i][j] = a.Jyu[i][j];
    for(int j = 0; j < wm; j++)
      Jyw[i][j] = a.Jyw[i][j];
    for(int j = 0; j < Cmax; j++)
      Jyc[i][j] = a.Jyc[i][j];
  }

  for(int i = 0; i < wm; i++)
    AW[i] = a.AW[i];
  for(int i = 0; i < Cmax; i++)
    AC[i] = a.AC[i];
}//EOF
//----------------------------------
// DESTRUCTOR FOR RBFN
// -------------------------------------
rbfn::~rbfn()
{
  deallocate_memory();
}
//------------------------------------------------
void rbfn::deallocate_memory()
{
  free_memory(X);
  free_memory(Y);
  free_memory(S);
  free_memory(V);
  free_memory(Z);
  free_memory(AW);
  free_memory(AC);
  free_memory(Wt1);
  free_memory(Wt2);
  free_memory(C, NC);
  free_memory(W, NOP);
  free_memory(Jyc, NOP);
  free_memory(Jyw, NOP);
  free_memory(Jyu, NOP);
  free_memory(Wt_index, wm);
  free_memory(Cen_index, Cmax);
  free_memory(P, NC);

  if((int)act[0] == 5)
    free_memory(SIG);

}
//---------------------------------------------
// allocate memory in RBFN
void rbfn::allocate_memory()
{
  if((int)act[0] == 5) // in case of Gaussian function
    SIG = array(NC); // assign different sigma for each hidden node

  //allocate memory
  C = array(NC, NIP);
  W = array(NOP, NC);
  X = array(NIP);
  Z = array(NC);
  S = array(NC);
  Y = array(NOP);
  V = array(NOP);
  AW = array(wm);
  AC = array(Cmax);
  Wt_index = intarray(wm, 2);
  Cen_index = intarray(Cmax, 2);
  Jyu = array(NOP, NIP);
  Jyc = array(NOP, Cmax);
  Jyw = array(NOP, wm);

  // Needed for LF-II Algorithm
  // Date: 10 January 2009, Saturday
  Wt1 = array(wm+Cmax);
  Wt2 = array(wm+Cmax);

  // Needed for RLS algorithm
  P = array(NC, NC);
}
//------------------------------------------------
// initialize_variables in RBFN
void rbfn::initialize_variables()
{
  //P is needed for RLS algorithm
  for(int i = 0; i < NC; i++)
    for(int j = 0; j < NC; j++)
    {
      if(i == j)
        P[i][j] = pinit;
      else
        P[i][j] = 0.0;
    }

  int k = 0;
  //initializing centers
  for(int i = 0; i < NC; i++)
    for(int j = 0; j < NIP; j++)
    {
      C[i][j] = minin + (maxin-minin) * gsl_rng_uniform(r);
      AC[k] = C[i][j];
      Cen_index[k][0] = i;
      Cen_index[k][1] = j;
      k = k + 1;
    }

  k = 0;
  //initializing weights
  for(int i = 0; i < NOP; i++)
  {
    for(int j = 0; j < NC; j++)
    {
      W[i][j] = 0.1 * gsl_rng_uniform(r);
      AW[k] = W[i][j];
      Wt_index[k][0] = i;
      Wt_index[k][1] = j;
      k = k + 1;
    }
  }

  //In case of gaussian activation function
  // there is a different sigma for each hidden node
  // Initially, they are assigned same value
  if((int)act[0] == 5)
  {
    for(int i = 0; i < NC; i++)
      SIG[i] = act[1];
  }
} //EOF
//--------------------------------------------
ostream& nnet::operator<<(ostream& os, const rbfn& a)
{
  os << "\n\n------------------------------------------\n";
  os << "Type:"<<" Radial Basis Function Network"<<endl;
  os << "Nodes: " << a.NIP << " - " << a.NC << " - " << a.NOP << endl;
  os << "Input Range: [ " << a.maxin << " - " << a.minin << " ]" << endl;

  os << "\nActivation function for hidden nodes: ";
  if((int)a.act[0] == 5)
  {
    os << "Gaussian" << endl;
    os << "Each hidden node is assigned a different sigma value" << endl;
    os << "sigma can be updated along with centres and weights" << endl;
  }
  else if((int)a.act[0] == 7)
    os<< "Thin-plate spline" << endl;
  else
  {
    os << "Not defined" << endl;
    os << "You should not use other activation functions for RBFN except 5 and 7" << endl;
  }
  os << "-------------------------\n" << endl;
  return os;
}//EOF 

//===========================================================
// This part was suggested by Awhan (awimagic@gmail.com)
// Overloading assignment operator
// Date; 13 December 2009, Sunday
// Remark: Checked and found OK. See the program "oprOverload.cpp" in
// the /routines/test folder.
//
// Note that this function is called when the objects on both
// side of = operator exist
// It is copying from one object to another. 
// =================================================
rbfn & rbfn::operator=(const rbfn & a)
{
  //cout << __FILE__ << " " << __PRETTY_FUNCTION__ << endl ;

  if( this == &a )
  {
    return *this;
  }

  //delete previously allocated memory
  deallocate_memory();

  //Assign parameters
  NIP = a.NIP;
  NOP = a.NOP;
  NC = a.NC;
  wm = a.wm;
  Cmax = a.Cmax;

  maxin = a.maxin;
  minin = a.minin;
  pinit = a.pinit;

  called_ij = a.called_ij;
  called_wj = a.called_wj;
  called_cj = a.called_cj;


  for(int i = 0; i < 2; i++)
  {
    act[i] = a.act[i];
    oact[i] = a.oact[i];
  }


  //Creat new memory allocation
  allocate_memory();

  //Needed for LF-II
  for(int i = 0; i < wm+Cmax; i++)
  {
    Wt1[i] = a.Wt1[i];
    Wt2[i] = a.Wt2[i];
  }

  //-------------------------------
  // Need for RLS
  for(int i = 0; i < NC; i++)
    for(int j = 0; j < NC; j++)
      P[i][j] = a.P[i][j];
  //---------------------------

  //Re-initializing centers
  for(int i = 0; i < NC; i++)
  {
    for(int j = 0; j < NIP; j++)
      C[i][j] = a.C[i][j];
  }

  if((int)act[0] == 5)
  { // in case of Gaussian function
    for(int i = 0; i < NC; i++)
    {
      SIG[i] = a.SIG[i];
    }
  }

  for(int i = 0; i < wm; i++)
  {
    Wt_index[i][0] = a.Wt_index[i][0];
    Wt_index[i][1] = a.Wt_index[i][1];
  }

  for(int i = 0; i < Cmax; i++)
  {
    Cen_index[i][0] = a.Cen_index[i][0];
    Cen_index[i][1] = a.Cen_index[i][1];
  }

  //Re-initializing weights
  for(int i = 0; i < NOP; i++)
  {
    for(int j = 0; j < NC; j++)
      W[i][j] = a.W[i][j];
    for(int j = 0; j < NIP; j++)
      Jyu[i][j] = a.Jyu[i][j];
    for(int j = 0; j < wm; j++)
      Jyw[i][j] = a.Jyw[i][j];
    for(int j = 0; j < Cmax; j++)
      Jyc[i][j] = a.Jyc[i][j];
  }

  for(int i = 0; i < wm; i++)
    AW[i] = a.AW[i];
  for(int i = 0; i < Cmax; i++)
    AC[i] = a.AC[i];

  return *this ;
}


//================================
void rbfn::display_var(string s)
{
  cout << setprecision(4) ;

  if(s == "wt")
  {
    cout << "\nWeights : dim(W) = " << NOP << "x" << NC << endl;
    cout << "------------------------\n";
    for(int i = 0; i < NOP; i++)
    {
      for(int j = 0; j < NC; j++)
        cout << "W[" << i << "][" << j << "] = " << showpos << W[i][j] << noshowpos << endl;
      cout << endl;
    }
    cout << "--------------------------------\n\n";
  }
  else if(s == "cen")
  {
    cout << "\nCenters :  dim(C) = " << NC << "x" << NIP << endl;
    cout << "-------------------------\n";
    for(int i = 0; i < NC; i++)
    {
      for(int j = 0; j < NIP; j++)
        cout << "C[" << i << "][" << j << "] = " << showpos << C[i][j] << noshowpos << "\t";
      cout << endl << endl;
    }
    cout << "--------------------------------\n\n";
  }
  else if(s == "io")
  {
    cout << "\nInput-output pair for each neuron\n";
    cout << "Column: Layers  Row: neurons\n";
    cout << "------------------------------\n";

    for(int i = 0; i < NC; i++)
    {
      cout << setw(4) << i << " : ";
      if(i < NIP)
        cout << setw(8) << X[i] << "\t";
      else
        cout << setw(8) << "***" << "\t";

      cout << setw(8) << Z[i] << " : " << setw(8) << S[i] << "\t";

      if(i < NOP)
        cout << setw(8) << Y[i] << endl;
      else
        cout << setw(8) << "***" << endl;
    }
  }
  else if(s == "jyw")
  {
    cout << "\nWeight-Jacobian : dim(Jyw) = " << NOP << "x" << wm << endl;
    cout << "-----------------------------\n";
    for(int i = 0; i < NOP; i++)
    {
      for(int j = 0; j < wm; j++)
        cout << "Jyw[" << i << "][" << j << "]= " << showpos << Jyw[i][j] << noshowpos << endl;
      cout << endl;
    }
    cout << "---------------------------------\n";
  }
  else if(s == "jyc")
  {
    cout << "\nCenter- Jacobian: dim(Jyc) = " << NOP << "x" << NC << endl;
    cout << "-------------------------------\n";
    for(int i = 0; i < NOP; i++)
    {
      for(int j = 0; j < Cmax; j++)
        cout << "Jyc[" << i << "][" << j << "] = " << showpos << Jyc[i][j] << noshowpos << endl;
      cout << endl; 
    }
    cout << "----------------------------------\n";
  }
  else if(s == "jyu")
  {
    cout << "\nInput-Jacobian : dim(Jyu) = " << NOP << "x" << NIP << endl;
    cout << "-----------------------------\n";
    for(int i = 0; i < NOP; i++)
    {
      for(int j = 0; j < NIP; j++)
        cout << "Jyu[" << i << "][" << j << "] = " << showpos << Jyu[i][j] << noshowpos << endl;
      cout << endl;  
    }
    cout << "-----------------------\n";
  }
  else if(s == "idx")
  {
    cout << "\nWeight indices\n-------------\n";
    for(int i = 0; i < wm; i++)
      cout << i << " ---->  (" << Wt_index[i][0] << ", " << Wt_index[i][1] << ") " << endl;
    cout << "\n------------------\n";

    cout << "Center indices\n----------------\n";
    for(int i = 0; i < Cmax; i++)
      cout << i << " ---->   ("<< Cen_index[i][0] << ", " << Cen_index[i][1] << ") " << endl;
    cout << "\n-------------------------\n";
  }
  else
  {
    cout << "Invalid Options .." << endl;
    cout << "Try 'wt', 'cen', 'jyw', 'jyc', 'jyu', 'io', 'idx'" << endl;
    exit(-1);
  }
}//EOF
//------------------------------------------------------
// Output activation function is incorporated
// Date: 27 February 2009, Friday
// Date: 23 April 2009, Thursday
//----------------------------------------------------------
void rbfn::network_output(double ip[], double op[])
{
  //apply input
  for(int i = 0; i < NIP; i++)
    X[i] = ip[i]; 

  for(int i = 0; i < NC; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NIP; j++)
      s1 += (C[i][j] - X[j]) * (C[i][j] - X[j]);
    Z[i] = sqrt(s1);

    if((int)act[0] == 5)
      act[1] = SIG[i]; //each node has a separate sigma

    S[i] = active_func(act, Z[i]);
  }                          

  //output of the RBFN
  for(int k = 0; k < NOP; k++)
  {
    double s2 = 0.0;
    for(int j = 0; j < NC; j++)
      s2 += W[k][j] * S[j];
    V[k] = s2;
    Y[k] = active_func(oact, V[k]);
    op[k] = Y[k];
  }
}  
//-----------------------------------------------
// backprop (double yd, double eta_c, double eta_w, double eta_s)
// Description: Updates the centers and weights using
// back-propagation algorithm
// Status: OK, already checked beforehand
// Last Check on 19 July 2006 Wednesday
// Last Check on 27 February 2009, Friday
// ------------------------------------------------------
void rbfn::backprop(double yd[], double eta_c, double eta_w, double eta_s)
{
  if(eta_w > 0)
  {
    //weight update law
    for(int k = 0; k < NOP; k++)
      for(int j = 0; j < NC; j++)
        W[k][j] += eta_w * (yd[k] - Y[k]) * dactive(oact, V[k]) * S[j];

    for(int i = 0; i < wm; i++)
      AW[i] = W[Wt_index[i][0]][Wt_index[i][1]];
  }

  if(eta_c > 0)
  {
    //center update law
    for(int j = 0; j < NC; j++)
    {
      if((int)act[0] == 5)
        act[1] = SIG[j];

      for(int i = 0; i < NIP; i++)
      {
        double s1 = 0.0;
        for(int k = 0; k < NOP; k++)
          s1 += (yd[k]-Y[k]) * dactive(oact, V[k]) * W[k][j];
        C[j][i] += eta_c * s1 * dactive(act, Z[j]) * (C[j][i]-X[i]) / Z[j];
      }
    }

    for(int i = 0; i < Cmax; i++)
      AC[i] = C[Cen_index[i][0]][Cen_index[i][1]];
  }

  if((int)act[0] == 5 && eta_s > 0) // only for Gaussian Function
  {
    for(int i = 0; i < NC; i++)
    {
      //update Sigma
      double s1 = 0.0;
      for(int j = 0; j < NOP; j++)
        s1 += (yd[j] - Y[j]) * dactive(oact, V[j]) * W[j][i] * S[i] * Z[i] * Z[i] /(pow(SIG[i], 3));

      SIG[i] += eta_s * s1;
    }
  }
}//EOF

//---------------------------------------
/* input_jacobian(double **Joi)
 * Description: Computes the jacobian matrix dY/dU
 * Date: 24/02/06 Friday
 * Status: OK
 * Checked: 27/02/06 Monday
 * Checked: 22 April 2006 Saturday
 * Checked: 19 July 2006 Wednesday
 * activation function is incorporated into output layer
 * Checked on 27 February 2009, Friday
 * ------------------------------------------------*/
void rbfn::input_jacobian(double **Joi)
{
  if(called_ij == 0)
    called_ij = 1;

  for(int i = 0; i < NOP; i++)
    for(int k = 0; k < NIP; k++)
    {
      double s1 = 0.0;
      for(int j = 0; j < NC; j++)
      {
        if((int)act[0] == 5)
          act[1] = SIG[j]; //each node has separate sigma

        s1 += W[i][j] * dactive(act, Z[j]) * (X[k]-C[j][k]) / Z[j];
      }
      Jyu[i][k] = dactive(oact, V[i]) * s1;
      if(Joi != NULL)
        Joi[i][k] = Jyu[i][k];
    }
}
//--------------------------------------------------------               
void rbfn::wt_init(double max, double min)
{
  int k = 0;
  for(int i = 0; i < NOP; i++)
    for(int j = 0; j < NC; j++)
    {
      W[i][j] = min + (max - min) * gsl_rng_uniform(r);
      AW[k] = W[i][j];
      k = k + 1;
    }
}
//------------------------------------------------
void rbfn::cen_init(double max, double min)
{
  int k = 0;
  for(int i = 0; i < NC; i++)
     for(int j = 0; j < NIP; j++)
     {
       C[i][j] = min + (max - min) * gsl_rng_uniform(r);
       AC[k] = C[i][j];
       k = k + 1;
     }
}
//--------------------------------------------------
// Wt_jacobian(): computes dy/dW of dimension NOP x wm
// Date: 20 April 2006 Thursday
// Status: Checked on 22 April 2006
// Status: Checked on 19 July 2006
//       : checked on 11 April 2008
//
// output activation functions are incorporated on 
// 27 February 2009, Friday.
// --------------------------------------
void rbfn::wt_jacobian(double **Jow)
{
  if(called_wj == 0)
    called_wj = 1;

  int i,j;
  for(i = 0; i < NOP; i++)
    for(j = 0; j < wm; j++)
    {
      Jyw[i][j] = dactive(oact, V[i]) * S[Wt_index[j][1]];

      if(Jow != NULL)
        Jow[i][j] = Jyw[i][j];
    }
}
//------------------------------------------------   
// Cen_jacobian(): COmputes dy/dC of dimension NOP x Cmax
// Date: 20 April 2006
// Status: Checked on 22 April 2006
//       : Checked on 19 July 2006 Wednesday
//       : Checked on 11 April 2008 Friday
// Output activation function included on 27 Feb 2009, Friday       
// ---------------------------------------------------------
void rbfn::cen_jacobian(double **Joc)
{

  if(called_cj == 0)
    called_cj = 1;

  int i,j;
  for(i = 0; i < NOP; i++)
    for(j = 0; j < Cmax; j++)
    {
      if((int)act[0] == 5)
        act[1] = SIG[Cen_index[j][0]];

      Jyc[i][j] = dactive(oact, V[i]) * W[i][Cen_index[j][0]] * dactive(act, Z[Cen_index[j][0]]) * 
        (C[Cen_index[j][0]][Cen_index[j][1]] - X[Cen_index[j][1]]) /  Z[Cen_index[j][0]];

      if(Joc != NULL)
        Joc[i][j] = Jyc[i][j];
    }
}
//---------------------------------------------
// Load parameters 
void rbfn::load_parameters(double *wt, double *cv, double *sig)
{
  for(int i = 0; i < wm; i++)
  {
    AW[i] = wt[i];
    W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
  }

  if(cv != NULL)
  {
    for(int i = 0; i < Cmax; i++)
    {
      AC[i] = cv[i];
      C[Cen_index[i][0]][Cen_index[i][1]] = AC[i];
    }
  }

  if(sig != NULL)
  {
    for(int i = 0; i < Cmax; i++)
      SIG[i] = sig[i];
  }

}
//----------------------------------------------------
void rbfn::load_parameters(double **wt, double **cv)
{
  int k = 0;
  for(int i = 0; i < NOP; i++)
    for(int j = 0; j < NC; j++)
    {
      W[i][j] = wt[i][j];
      AW[k] = W[i][j];
      k = k + 1;
    }

  if(cv != NULL)
  {
    k = 0;
    for(int i = 0; i < NC; i++)
      for(int j = 0; j < NIP; j++)
      {
        C[i][j] = cv[i][j];
        AC[k] = C[i][j];
        k = k + 1;
      }
  }
}
//--------------------------------------------
// get parameters
void rbfn::get_parameters(double *wt, double *cv, double *sig)
{
  for(int i = 0; i < wm; i++)
    wt[i] = AW[i];

  if(cv != NULL)
  {
    for(int i = 0; i < Cmax; i++)
      cv[i] = AC[i];
  }
  if(sig != NULL)
  {
    for(int i = 0; i < Cmax; i++)
      sig[i] = SIG[i];
  }
}
//-------------------------------------------------
void rbfn::get_parameters(double **wt, double **cv)
{
  for(int i = 0; i < NOP; i++)
    for(int j = 0; j < NC; j++)
      wt[i][j] = W[i][j];

  if(cv != NULL)
  {
    for(int i = 0; i < NC; i++)
      for(int j = 0; j < NIP; j++)
        cv[i][j] = C[i][j];
  }
}
//--------------------------------------------------------
void rbfn::maxparm(int *wmax, int *cmax)
{
  *wmax = wm;
  *cmax = Cmax;
}
//-----------------------------------------------------------
// Date: 07 August 2008
// This updates the network parameters given the update
// vector dp. 
// Status: Under review
// -------------------------------------------
void rbfn::update_parm(double eta, double *dp, int cnt)
{

  if(cnt == wm+Cmax)
  {
    for(int i = 0; i < wm+Cmax; i++)
    {
      if(i < wm)
      {
        AW[i] = AW[i] + eta * dp[i];
        W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
      }
      else
      {
        AC[i-wm] = AC[i-wm] + eta * dp[i]; 
        C[Cen_index[i-wm][0]][Cen_index[i-wm][1]] = AC[i-wm];
      }
    }
  }
  else if(cnt == wm)
  {
    for(int i = 0; i < wm; i++)
    {
      AW[i] = AW[i] + eta * dp[i];
      W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
    }
  }
  else if(cnt == Cmax)
  {
    for(int i = 0; i < Cmax; i++)
    {
      AC[i] = AC[i] + eta * dp[i]; 
      C[Cen_index[i][0]][Cen_index[i][1]] = AC[i];
    }
  }
  else
  {
    cout << "update_parm: The dimensions don't match .. exiting!" << endl;
    exit(-1);
  }
}//EOF
//-------------------------------------------------------------    
void rbfn::ksom_cluster(double ip[], double eta, double sigma)
{
  int windex;
  double min = 0.0;
  for(int i = 0; i < NC; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NIP; j++)
      s1 += pow((C[i][j] - ip[j]), 2.0);

    double z1 = sqrt(s1);

    if(z1 < min)
    {
      min = z1;
      windex = i;
    }
  }

  //update centers
  for(int i = 0; i < NC; i++)
  {
    // compute lattice distance
    double dw = pow((windex - i), 2.0);

    //compute neighborhood function
    double hw = exp( -dw / (2*sigma * sigma));

    for(int j = 0; j < NIP; j++)
      C[i][j] += eta * hw * (ip[j] - C[i][j]);
  }

  for(int i = 0; i < Cmax; i++)
    AC[i] = C[Cen_index[i][0]][Cen_index[i][1]]; 

}
//-------------------------------------------------
// output activation function included on 27 Feb 2009, Friday
void rbfn::bp_wt_update(double yd[], double eta_w)
{

  //weight update law
  for(int k = 0; k < NOP; k++)
    for(int j = 0; j < NC; j++)
      W[k][j] += eta_w * (yd[k] - Y[k]) * dactive(oact, V[k]) * S[j];

  for(int i = 0; i < wm; i++)
    AW[i] = W[Wt_index[i][0]][Wt_index[i][1]];  
          
}
//--------------------------------------------------
// RECURSIVE LEAST SQUARE ALGORITHM FOR RBFN
//
// Tested for xor and sine function using gaussian as well as tps
// activation functionl
// Date: 13 September 2007
//
// 27 Feb 2009, Friday: 
//  Output activation function NOT Included ... It is to be Modified 
//  ****** CAREFUL !! ********************
// ---------------------------------------------------------
void rbfn::rls_wt_update(double d[], double lam)
{

  double *K, *s2, **s3;
  double s1, s4, *e;

  K = new double [NC];
  s2 = new double [NC];
  s3 = new double *[NC];
  e = new double [NOP];

  for(int i = 0; i < NC; i++)
    s3[i] = new double [NC];


  // Error
  for(int i = 0; i < NOP; i++)
    e[i] = d[i] - Y[i];


  // S2 = P * S
  // s1 = S' * P * S

  s1 = 0.0;
  for(int i = 0; i < NC; i++)
  {
    s2[i] = 0.0;
    for(int j = 0; j < NC; j++)
      s2[i] += P[i][j] * S[j];

    s1 += S[i] * s2[i];
  }

  // Gain Matrix
  // K = P*S/(lam + S'*P*S)

  for(int i = 0; i < NC; i++)
    K[i] = s2[i] / (lam + s1);

  // S3 = K * S' : m x m

  for(int i = 0; i < NC; i++)
    for(int j = 0; j < NC; j++)
      s3[i][j] = K[i] * S[j];


  // S4 = K * S' * P = S3 * P

  for(int i = 0; i < NC; i++)
    for(int j = 0; j < NC; j++)
    {
      s4 = 0.0;
      for(int k = 0; k < NC; k++)
        s4 += s3[i][k] * P[k][j];

      // Update P
      P[i][j] = (P[i][j] - s4) / lam;
    }

  //Update W
  // W' = W' + K' * e

  for(int i = 0; i < NOP; i++)
  {
    for(int j = 0; j < NC; j++)
       W[i][j] = W[i][j] + K[j] * e[i];
  }


  delete [] K;
  delete [] s2;
  delete [] s3;
}

//---------------------------------------------------
void rbfn::save_parameters(const char *filename)
{
  ofstream f1(filename);
  for(int i = 0; i < wm; i++)
    f1 << AW[i] << endl;
  f1 << endl << endl;


  for(int i = 0; i < Cmax; i++)
    f1 << AC[i] << endl;

  f1.close();
}//EOF
//===============================
void rbfn::load_parameters(const char *filename)
{
  int max_cnt = wm + Cmax;

  int k = 0;
  ifstream f1(filename);
  
  for(int i = 0; i < wm; i++)
  {
    f1 >> AW[i];
    W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
    k = k + 1;
  }

  for(int i = 0; i < Cmax; i++)
  {
    f1 >> AC[i];
    C[Cen_index[i][0]][Cen_index[i][1]] = AC[i];
    k = k + 1;
  }
  if(k < max_cnt || k > max_cnt)
  {
    cout << "Error reading file ...\n";
    exit(-1);
  }
  f1.close();
}//EOF
//===========================================
// LF 1 Algorithm : IEEE TNN Vol. 17, No. 5, Pages 1116-1125
//
// You must compute jacobian before calling this function
//
// Date: 07 June 2008, Saturday
// Status: Success
// Last check on 04 May 2008 Sunday
//  Index of Yd starts from 0
//
//  Remark: Performance is sensitive to initial values of weights
//  and the value of learning parameter mu. 
//
//  27 Feb 2009, Friday:
//  Warning: Output Activation function NOT included ... It is to be
//  modified
// --------------------------------------------------
void rbfn::lf1_wt_update(double Yd[], double mu)
{
  double *jy, eta_a;

  try
  {
    jy = new double [wm];
  }
  catch(std::bad_alloc)
  {
    cout << "Allocation failure in ffn::lf1_update" << endl;
    exit(-1);
  }

  double st = 0.0;
  for(int i = 0; i < wm; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NOP; j++)
      s1 += Jyw[j][i] * (Yd[j] - Y[j]);

    jy[i] = s1;

    st += jy[i] * jy[i];
  }

  double sy = 0.0;
  for(int i = 0; i < NOP; i++)
    sy += pow((Yd[i] - Y[i]), 2.0);


  if(st < 1e-5)
    st = st + 0.0001;

  //adaptive learning rate
  eta_a =  mu * sy/st;

  //update weights
  for(int i = 0; i < wm; i++)
  {
    AW[i] = AW[i] + eta_a * jy[i];

    W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
  }

  delete [] jy;
}//EOF

//=============================================================
// Update Centers and weight using LF algorithm
// Date: 14 August 2008, Wednesday
// One must call cen_jacobian and wt_jacobian 
// before calling this function
//
// 27 Feb 2009, Friday: 
// Output activation function NOT Included .... It is to be modified
//  *********** CAREFUL !! *************
// --------------------------------------------
void rbfn::LF_update(double Yd[], double mu)
{  
  if(called_wj == 0 || called_cj == 0)
  {
    cout << "wt_jacobian and cen_jacobian function must be called before calling this function.\n";
    cout << " ... exiting!\n";
    exit(-1);
  }

  double *jy, eta_a;

  try
  {
    jy = new double [wm+Cmax];
  }
  catch(std::bad_alloc)
  {
    cout << "Allocation failure in ffn::LF1_update" << endl;
    exit(-1);
  }

  double st = 0.0;
  for(int i = 0; i < wm+Cmax; i++)
  {
    double s1 = 0.0;
    if(i < wm)
    {
      for(int j = 0; j < NOP; j++)
        s1 += Jyw[j][i] * (Yd[j] - Y[j]);
    }
    else
    {
      for(int j = 0; j < NOP; j++)
        s1 += Jyc[j][i-wm] * (Yd[j] - Y[j]);
    }

    jy[i] = s1;

    st += jy[i] * jy[i];
  }

  double sy = 0.0;
  for(int i = 0; i < NOP; i++)
    sy += pow((Yd[i] - Y[i]), 2.0);


  if(st < 1e-5)
    st = st + 0.0001;

  //adaptive learning rate
  eta_a =  mu * sy/st;

  //update weights
  for(int i = 0; i < wm+Cmax; i++)
  {
    if(i < wm)
    {
      AW[i] = AW[i] + eta_a * jy[i];
      W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
    }
    else
    {
      AC[i-wm] = AC[i-wm] + eta_a * jy[i];
      C[Cen_index[i-wm][0]][Cen_index[i-wm][1]] = AC[i-wm];
    }
  }

  delete [] jy;
}
//--------------------------------------------------------
// Distributed LF: Task is to reduce memory requirement while
// implementing LF Algorithm
// Date: 08 January 2009 Thursday
//
// The performance is tested on 6-700-3 RBF network and its
// performance is worse than the std LF-I algorithm.
//
//
// 27 Feb 2009, Friday: 
// Output activation function NOT included ... Be careful ...
// ******* WARNING **********
// ----------------------------------------------------------
void rbfn::distributed_LF_update(double Yd[], double mu)
{
  double *dyw = new double[NOP];
  double *dyc = new double[NOP];
  double *e = new double [NOP];

  double se = 0.0;
  for(int k = 0; k < NOP; k++)
  {
    e[k] = Yd[k] - Y[k];
    se += e[k] * e[k];
  }

  //update all parameters
  for(int i = 0; i < wm+Cmax; i++)
  {
    if(i < wm)
    {
      double sw = 0.0;
      for(int k = 0; k < NOP; k++)
      {
        if(Wt_index[i][0] != k)
          dyw[k] = 0.0;
        else
          dyw[k] = S[Wt_index[i][1]];

        sw += dyw[k] * e[k];
      }

      if(sw < 1e-3)
        sw = 0.01;

      //adaptive learning rate
      double eta = mu * se / (sw*sw);

      //update weights
      AW[i] = AW[i] + eta * sw;
      W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
    }
    else
    {
      double sc = 0.0;
      for(int k = 0; k < NOP; k++)
      {
        dyc[k] = W[k][Cen_index[i-wm][0]] * dactive(act, Z[Cen_index[i-wm][0]]) * 
          (C[Cen_index[i-wm][0]][Cen_index[i-wm][1]] - X[Cen_index[i-wm][1]]) /  Z[Cen_index[i-wm][0]];

        sc += dyc[k] * e[k];
      }

      if(sc < 1e-3)
        sc = 0.01;

      //adaptive learning rate
      double eta = mu * se / (sc*sc);

      //update centers
      AC[i-wm] = AC[i-wm] + eta * sc;
      C[Cen_index[i-wm][0]][Cen_index[i-wm][1]] = AC[i-wm];
    }
  }
  delete [] dyc;
  delete [] dyw;
}//EOF
//==================================================================
// LF-II Update algorithm 
// Date: 10 January 2009, Saturday
// Status: 
// Remark:
//
// 27 Feb 2009, Friday:
// Output activation function NOT included ... It is to be modified
//  ******** WARNING *************
// ------------------------------------------------
void rbfn::LF_update(double Yd[], double mu, double lambda)
{
  if(called_wj == 0 || called_cj == 0)
  {
    cout << "wt_jacobian and cen_jacobian function must be called before calling this function.\n";
    cout << " ... exiting!\n";
    exit(-1);
  }

  double *jy, eta_a, eta_b;

  try
  {
    jy = new double [wm+Cmax];
  }
  catch(std::bad_alloc)
  {
    cout << "Allocation failure in ffn::LF1_update" << endl;
    exit(-1);
  }

  double st = 0.0;
  for(int i = 0; i < wm+Cmax; i++)
  {
    double s1 = 0.0;
    if(i < wm)
    {
      for(int j = 0; j < NOP; j++)
        s1 += Jyw[j][i] * (Yd[j] - Y[j]);
    }
    else
    {
      for(int j = 0; j < NOP; j++)
        s1 += Jyc[j][i-wm] * (Yd[j] - Y[j]);
    }

    jy[i] = s1;

    st += jy[i] * jy[i];
  }

  double sy = 0.0;
  for(int i = 0; i < NOP; i++)
    sy += pow((Yd[i] - Y[i]), 2.0);


  if(st < 1e-5)
    st = st + 0.0001;

  //adaptive learning rate
  eta_a =  mu * sy/st;

  //adaptive accn rate
  eta_b =  lambda * sy / st;

  //update weights
  for(int i = 0; i < wm+Cmax; i++)
  {
    if(i < wm)
    {
      Wt2[i] = Wt1[i];
      double wddot = AW[i] - 2 * Wt1[i] + Wt2[i];
      Wt1[i] = AW[i];
      AW[i] = AW[i] + eta_a * jy[i] + eta_b * wddot;
      W[Wt_index[i][0]][Wt_index[i][1]] = AW[i];
    }
    else
    {
      Wt2[i] = Wt1[i];
      double cddot = AC[i-wm] - 2 * Wt1[i] + Wt2[i];
      Wt1[i] = AC[i-wm];
      AC[i-wm] = AC[i-wm] + eta_a * jy[i] + eta_b * cddot;
      C[Cen_index[i-wm][0]][Cen_index[i-wm][1]] = AC[i-wm];
    }
  }

  delete [] jy;

}//EOF
//---------------------------------------
// Network inversion algorithm based on Gradient Descent
// Date: 06 August 2008 Wednesday
// Status: OK
// Remark: Another check would be good to avoid bugs
//
//
// 27 Feb 2009, Friday: 
//
// Output activation function NOT included ... it is to be modified.
//
// Date: January 27, 2015, Tuesday:
// It is not required to include output activation here as it is
// accounted for in computing Jyu (input_jacobian) matrix.
// ********* WARNING *****************
// ----------------------------------------

void rbfn::update_input_gd(double eta, double ydn[], double xn[])
{
  if(called_ij == 0)
  {
    cout << "input_jacobian function must be called before calling this function.\n";
    cout << " ... exiting!\n";
    exit(-1);
  }

  for(int i = 0; i < NIP; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NOP; j++)
      s1 += (ydn[j] - Y[j]) * Jyu[j][i];

    X[i] += eta * s1 ;
  }

  for(int i = 0; i < NIP; i++)
    xn[i] = X[i];
}

//===================================================================
// Date: January 27, 2015
// Function: Update input using gradient descent
// However, only a part of the input vector is updated. 
// ------------------------------------------------------------

void rbfn::update_input_gd2(double eta, double ydn[], double xn[], int input_vector_size)
{
  if(called_ij == 0)
  {
    cout << "input_jacobian function must be called before calling this function.\n";
    cout << " ... exiting!\n";
    exit(-1);
  }

  for(int i = 0; i < input_vector_size; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NOP; j++)
      s1 += (ydn[j] - Y[j]) * Jyu[j][i];

    X[i] += eta * s1 ;
  }

  for(int i = 0; i < NIP; i++)
    xn[i] = X[i];
}
//=============================
// 11 Aug 2008 Monday
// loads the centers provided as matrix CM
// ----------------------------------------

int rbfn::load_centers(double **CM)
{
    int k = 0;
    for(int i = 0; i < NC; i++)
      for(int j = 0; j < NIP; j++)
      {
        C[i][j] = CM[i][j];
        AC[k] = C[i][j];
        k = k + 1;
      }
    return k;
}

//----------------------------------
int rbfn::read_centers(double **CM)
{
    int k = 0;
    for(int i = 0; i < NC; i++)
      for(int j = 0; j < NIP; j++)
      {
        CM[i][j] = C[i][j];
        k = k + 1;
      }
    return k;
}
//----------------------------------

  
// ==============================
//20 August 2008 wednesday
//loads centers from a file
//================================

void rbfn::load_centers(const char *filename)
{
  ifstream fc(filename);
    int k = 0;
    for(int i = 0; i < NC; i++)
      for(int j = 0; j < NIP; j++)
      {
        fc >> C[i][j];
        AC[k] = C[i][j];
        k = k + 1;
      }
    fc.close();
    cout << "No. of centers loaded = " << k << endl;
}
//=================================
    



  

