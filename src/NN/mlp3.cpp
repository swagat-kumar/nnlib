/*--------------------------------------------------------------------

    3 LAYER MULTI-LAYER-PERCEPTRON (MLP) Class Definitions
    Copyright (C) 2007   Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


-------------------------------------------------------------------------- */ 

  /* =======================================================
  * 
  *         MLP FOR A 3-LAYER NETWORK
  *                  
  *         External VECTOR INDICES START FROM 0
  *
  *
  * ===================================================== */


/* ================================================================
 * UPDATES:
 * 
 *  ----
 *  Date: 17 March 2007 Saturday
 *  - mlp3 class is rigorously tested and found to be ok.
 *  - wt_jacobian and input_jacobian were corrected 
 *  - another check might be necessary
 *  ----
 *
 *
 *  ----
 *  Date: 20 March 2007 Tuesday
 *  - netinfo() function was checked and some mistakes were corrected
 *  - wt_jacobian() and input_jacobian() were checked against hand calculations
 *    for a 2-1-1 network and found correct.
 *  ----
 *
 *  ----
 *  Date: 31 May 2007 Thursday
 *  - Providing an option to remove bias from neurons ...
 *  - act[3] and oact[3] : change in dimension
 *
 *  ---- 
 *
 *  ----
 *  Date: 08 June 2007 Friday
 *  - gd_update(): Uses explicit derivation
 *  - gd_update2(): uses wt_jacobian to find weight update.
 *    Wt_jacobian() must be called before calling this function
 *  - Tested for XOR function and found working. 
 *  ----
 *
 *
 * --------
 *  Date: 26 September 2008 Friday
 *  - Mlp class file is modified to incorporate zero bias changes
 *  - 4 new constructors are introduced
 *  - memory allocation routines are separated
 *  - separate code is written for zero bias case
 *  - Todo: Rigorous testing needed
 * -----------
 *
 * -------------------
 *  Date: 25 February 2009 Wednesday
 *  - New memory deallocation routines are used for deallocating
 *  memory: free_memory() in destructor
 *  - Thorough check 
 *
 * ----------------------------------
 *
 * ================================================================= */

#include<iostream>
#include<iomanip>
#include<new>
#include <nnet.h>
#include <cstdlib>

using namespace std;
using namespace nnet;

// Constructor 1
// Default activation for both hidden and output neurons: tansig 
// Default polarity: Bipolar
// Default bias : Unity
// Date: 26 September 2008, Friday
// ------------------------------------------------
mlp3::mlp3(int Ni[])
{
  srand(time(NULL));

  bias = 1.0; // 1 - unity bias is applied to all neurons 
  bf = 1;         //bipolar weights

  //Activation is tansig for hidden and output neurons
  act[0] = 1.0;
  act[1] = 1.0;
  act[2] = 0.0;

  oact[0] = 1.0;
  oact[1] = 1.0;
  oact[2] = 0.0;

  mnpl = 0;
  for(int i = 0; i < 3; i++)
  {
    N[i] = Ni[i];
    if(N[i] > mnpl)
      mnpl = N[i];            // max. no. of nodes for all layers
  }

  // size of weight vector
  nw1 = N[1] * N[0];
  nw2 = N[2] * N[1];
  nw = nw1 + nw2;

  //size of bias vector
  nb1 = N[1]; 
  nb2 = N[2];
  nb = nb1 + nb2;



  //flags to check computation of Jacobian matrices
  called_ij = 0;
  called_wj = 0;

  allocate_memory();

  initialize_variables();

}
//----------------------------------------
// Constructor 2
// Same activation for hidden and output neurons
// Unity Bias applied to all neurons
//---------------------------------------------- 
mlp3::mlp3(int Ni[], double af[], int bpf)
{
  srand(time(NULL));

  bias = 1.0;  //bias input

  //Same activation for hidden and output neurons
  for(int i = 0; i < 3; i++)
  {
    act[i] = af[i];          
    oact[i] = act[i];
  }


  if(bf == 1 || bf == 0)
    bf = bpf;                   // 1 - bipolar, 0 - unipolar
  else
  {
    cout << "MLP constructor 2: Invalid value for bipolar flag .. exiting!" << endl;
    exit(-1);
  }

  mnpl = 0;
  for(int i = 0; i < 3; i++)
  {
    N[i] = Ni[i];
    if(N[i] > mnpl)
      mnpl = N[i];            // max. no. of nodes for all layers
  }

  // size of weight vector
  nw1 = N[1] * N[0];
  nw2 = N[2] * N[1];
  nw = nw1 + nw2;

  //size of bias vector
  nb1 = N[1]; 
  nb2 = N[2];
  nb = nb1 + nb2;

  //flags to check computation of Jacobian matrices
  called_ij = 0;
  called_wj = 0;


  allocate_memory();
  initialize_variables();
}
//----------------------------------
//Constructor 3
// Separate activation for hidden and output neurons
// Unity bias applied to all neurons
mlp3::mlp3(int Ni[], double af[], double of[], int bpf)
{
  srand(time(NULL));

  bias = 1.0; // 1 - unity bias is applied to all neurons 

  //Different activation for hidden and output neurons
  for(int i = 0; i < 3; i++)
    act[i] = af[i];          

  for(int i = 0; i < 3; i++)
    oact[i] = of[i];          

  if(bpf == 1 || bpf == 0)
    bf = bpf;                   // 1 - bipolar, 0 - unipolar
  else
  {
    cout << "MLP constructor 3: Invalid value for bipolar flag .. exiting!" << endl;
    exit(-1);
  }


  mnpl = 0;
  for(int i = 0; i < 3; i++)
  {
    N[i] = Ni[i];
    if(N[i] > mnpl)
      mnpl = N[i];            // max. no. of nodes for all layers
  }

  // size of weight vector
  nw1 = N[1] * N[0];
  nw2 = N[2] * N[1];
  nw = nw1 + nw2;

  //size of bias vector
  nb1 = N[1]; 
  nb2 = N[2];
  nb = nb1 + nb2;

  //flags to check computation of Jacobian matrices
  called_ij = 0;
  called_wj = 0;

  allocate_memory();
  initialize_variables();
}
//-------------------------------------------
//Constructor 4
// Zero bias is applied to neurons 
// ---------------------------------------------
mlp3::mlp3(int Ni[], double af[], double of[], int bpf, double bv)
{
  srand(time(NULL));

  //value of bias input
  bias = bv; 

  //Different activation for hidden and output neurons
  for(int i = 0; i < 3; i++)
    act[i] = af[i];          

  for(int i = 0; i < 3; i++)
    oact[i] = of[i];          

  if(bpf == 1 || bpf == 0)
    bf = bpf;                   // 1 - bipolar, 0 - unipolar
  else
  {
    cout << "MLP constructor 4: Invalid value for bipolar flag .. exiting!" << endl;
    exit(-1);
  }

 // max. no. of nodes for all layers
  mnpl = 0;
  for(int i = 0; i < 3; i++)
  {
    N[i] = Ni[i];
    if(N[i] > mnpl)
      mnpl = N[i];            
  }

  // size of weight vector
  nw1 = N[1] * N[0];
  nw2 = N[2] * N[1];
  nw = nw1 + nw2;

  //size of bias vector
  nb1 = N[1]; 
  nb2 = N[2];
  nb = nb1 + nb2;

  // size of weight vector
  nw = nw1 + nw2;

  //flags to check computation of Jacobian matrices
  called_ij = 0;
  called_wj = 0;

  allocate_memory();
  initialize_variables();
}
//---------------------------------------------------------------
void mlp3::allocate_memory()
{
  try{
    Xi = new double [N[0]];        // input to neurons
    Xh = new double [N[1]];
    Xo = new double [N[2]];

    Si = new double [N[0]];        // output of neurons
    Sh = new double [N[1]];
    So = new double [N[2]];

    W1 = new double *[N[1]];       // weights between input layer and hidden layer
    W2 = new double *[N[2]];       // weights between hidden layer and output layer

    BW1 = new double [N[1]];       // bias weights for hidden layer
    BW2 = new double [N[2]];       // bias weights for output layer

    WI1 = new int *[nw1];
    WI2 = new int *[nw2];

    W = new double  [nw];         // Weight vector : nw X 1   
    BW = new double [nb];         // Bias vector: nb x 1

    Jyw = new double *[N[2]];      // Jyw = dy/dw : N[2] x nw
    Jyx = new double *[N[2]];      // Jyu = dy/dx : N[2] x N[1] 
    Jyb = new double *[N[2]];

    for(int i = 0; i < nw1; i++)
      WI1[i] = new int [2];        // stores indices of W1

    for(int i = 0; i < nw2; i++)
      WI2[i] = new int [2];        // stores indices of W2

    //Initialize weight matrix W1 : N1 x N0  
    for(int i = 0; i < N[1]; i++)
      W1[i] = new double [N[0]];

    //Initialize weight matrix W2 : N2 x N1
    for(int i = 0; i < N[2]; i++)
      W2[i] = new double [N[1]];

    //Initialize jacobian matrix Jyw=dy/dw
    for(int i = 0; i < N[2]; i++)
    {
      Jyw[i] = new double [nw];
      Jyx[i] = new double [N[0]]; 
      Jyb[i] = new double [nb];
    }

  }//try
  catch(std::bad_alloc xa)
  {
    cout<<"Allocation failure in mlp3 constructor\n";
    exit(-1);
  }   
}
//---------------------------------------------
void mlp3::initialize_variables()
{
  int k;

  //Initialize weight matrix W1 : N1 x N0  
  k = 0;
  for(int i = 0; i < N[1]; i++)
  {
    if(bf == 1)
      BW1[i] = -1.0 + 2.0 * rand()/(double)RAND_MAX;
    else
      BW1[i] = rand()/(double)RAND_MAX;

    for(int j = 0; j < N[0]; j++)
    {
      if(bf == 1)                    
        W1[i][j] = -1.0 + 2.0 * rand()/(double)RAND_MAX;
      else 
        W1[i][j] = rand()/(double)RAND_MAX;            // unipolar

      WI1[k][0] = i; WI1[k][1] = j;
      k = k + 1;
    }
  } 

  //Initialize weight matrix W2 : N2 x N1
  k = 0;
  for(int i = 0; i < N[2]; i++)
  {
    if(bf == 1)
      BW2[i] = -1.0 + 2.0 * rand()/(double)RAND_MAX;
    else
      BW2[i] = rand()/(double)RAND_MAX;
      
    for(int j = 0; j < N[1]; j++)
    {
      if(bf  == 1)                    
        W2[i][j] = -1.0 + 2.0 * rand()/(double)RAND_MAX; //bipolar
      else 
        W2[i][j] = rand()/(double)RAND_MAX;             // unipolar

      WI2[k][0] = i; WI2[k][1] = j;
      k = k + 1;
    }
  }


  for(int i = 0; i < N[2]; i++)
  {
    for(int j = 0; j < nw; j++)
      Jyw[i][j] = 0.0;
    for(int j = 0; j < N[0]; j++)
      Jyx[i][j] = 0.0;
    for(int j = 0; j < nb; j++)
      Jyb[i][j] = 0.0;
  }

  // Weight vector : W = [ W1 | W2 ]
  for(int i = 0; i < nw; i++)
  {
    if(i < nw1)
      W[i] = W1[WI1[i][0]][WI1[i][1]];
    else
      W[i] = W2[WI2[i-nw1][0]][WI2[i-nw1][1]];
  }

  for(int i = 0; i < nb; i++)
  {
    if(i < nb1)
      BW[i] = BW1[i];
    else
      BW[i] = BW2[i-nb1];
  }
}//EOF
//----------------------------------------------
// COPY CONSTRUCTOR for mlp3
// Date: 27 December 2006
// Modified on 26 September 2008
// Status: 
// ---------------------------------------
mlp3::mlp3(const mlp3 &a)
{
  int i,j;

  for(i = 0; i < 3; i++)
    act[i] = a.act[i];

  for(i = 0; i < 3; i++)  
    oact[i] = a.oact[i];

  for(i = 0; i < 3; i++)  // 3 layers ..
    N[i] = a.N[i];

  mnpl = a.mnpl;
  bias = a.bias;

  nw1 = a.nw1;
  nw2 = a.nw2;
  nw = a.nw;
  nb1 = a.nb1;
  nb2 = a.nb2;
  nb = a.nb;

  allocate_memory();

  //Reinitialize the variables

  for(int i = 0; i < N[0]; i++)
  {
    Xi[i] = a.Xi[i];
    Si[i] = a.Si[i];
  }
  for(int i = 0; i < N[1]; i++)
  {
    Xi[i] = a.Xi[i];
    Si[i] = a.Si[i];
  }
  for(int i = 0; i < N[2]; i++)
  {
    Xi[i] = a.Xi[i];
    Si[i] = a.Si[i];
  } 

  for(int i = 0; i < N[2]; i++)
    for(int j = 0; j < nw; j++)
      Jyw[i][j] = a.Jyw[i][j];

  for(int i = 0; i < N[2]; i++)
    for(int j = 0; j < N[0]; j++)
      Jyx[i][j] = a.Jyx[i][j];

  for(int i = 0; i < N[2]; i++)
    for(int j = 0; j < nb; j++)
      Jyb[i][j] = 0.0;

  for(int i = 0; i < N[2]; i++)
    for(j = 0; j < N[1]; j++)
      W2[i][j] = a.W2[i][j];

  for(int i = 0; i < N[1]; i++)
    for(j = 0; j < N[0]; j++)
      W1[i][j] = a.W1[i][j];

  for(int i = 0; i < N[1]; i++)
    BW1[i] = a.BW1[i];

  for(int i = 0; i < N[2]; i++)
    BW2[i] = a. BW2[i]; 

  for(int i = 0; i < nw; i++)
    W[i] = a.W[i];

  for(int i = 0; i < nb; i++)
    BW[i] = a.BW[i];

  for(int i = 0; i < nw1; i++)
    for(int j = 0; j < 2; j++)
      WI1[i][j] = a. WI1[i][j];

  for(int i = 0; i < nw2; i++)
    for(int j = 0; j < 2; j++)
      WI2[i][j] = a. WI2[i][j];

}//EOF          
//----------------------------------------------------
// DESTRUCTOR FOR mlp3 CLASS
//-----------------------------------------------------------
mlp3::~mlp3()
{
  delete [] Xi;
  delete [] Xo;
  delete [] Xh;
  delete [] Si;
  delete [] So;
  delete [] Sh;
  free_memory(Jyw, N[2]);
  free_memory(Jyx, N[2]);
  free_memory(Jyb, N[2]);
//  delete [] Jyw;
//  delete [] Jyx; 
//  delete [] Jyb;
  delete [] W;
  delete [] BW;
  free_memory(W1, N[1]);
  free_memory(W2, N[2]);
//  delete [] W1;
//  delete [] W2;
  delete [] BW1;
  delete [] BW2;
  free_memory(WI1, nw1);
  free_memory(WI2, nw2);
//  delete [] WI1;
//  delete [] WI2;
}
//-----------------------------------------------------
// Indices of input and output vector start from 0
// Date: 27 December 2006
// Last Update: 26 September 2008 Friday
// Separate computation for zero bias 
// ------------------------------------------------
void mlp3::network_output(double ip[], double op[])
{
  //apply input
  for(int i = 0; i < N[0]; i++)
  {
    Xi[i] = ip[i];
    Si[i] = Xi[i];
  }

  // hidden layer input and output
  for(int i = 0; i < N[1]; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < N[0]; j++)
      s1 += W1[i][j] * Si[j];

    Xh[i] = s1 + BW1[i] * bias;
    Sh[i] = active_func(act, Xh[i]);
  }

  //Output layer input and output
  for(int i = 0; i < N[2]; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < N[1]; j++)
      s1 += W2[i][j] * Sh[j];

    Xo[i] = s1 + BW2[i] * bias;
    So[i] = active_func(oact, Xo[i]);
    op[i] = So[i];
  }
}//EOF
//--------------------------------------------------
// Computing weight Jacobian = Jyw = dy/dw
// Separate computation for zero bias
// Date: 30 December 2006, Saturday
// Next Update: 17 March 2007 Saturday
// Last Modified on: 26 September 2008 Friday
// ---------------------------------------------
void mlp3::wt_jacobian(double **Jow)
{
  if(called_wj == 0)
    called_wj = 1;

  for(int i = 0; i < N[2]; i++)
    for(int k = 0; k < nw; k++)
    {
      if(k < nw1)
      {
        Jyw[i][k] = dactive(oact, Xo[i]) * W2[i][WI1[k][0]] * 
          dactive(act, Xh[WI1[k][0]]) * Si[WI1[k][1]];
      }
      else
        Jyw[i][k] = dactive(oact, Xo[i]) * Sh[WI2[k-nw1][1]];
    }

  for(int i = 0; i < N[2]; i++)
    for(int k = 0; k < nb; k++)
    {
      if(k < nb1)
        Jyb[i][k] = dactive(oact, Xo[i]) * W2[i][k] *
          dactive(act, Xh[k]) * bias;
      else
        Jyb[i][k] = dactive(oact, Xo[i]) * bias;
    }


  if(Jow != NULL)
  {
    if(bias > 0)
    {
      for(int i = 0; i < N[2]; i++)
        for(int j = 0; j < nw+nb; j++)
        {
          if(j < nw)
            Jow[i][j] = Jyw[i][j];
          else
            Jow[i][j] = Jyb[i][j-nw];
        }
    }
    else
    {
      for(int i = 0; i < N[2]; i++)
        for(int j = 0; j < nw; j++)
          Jow[i][j] = Jyw[i][j];
    }
  }
}//EOF
//-------------------------------------------------
// Computing Input Jacobian = Jyx = dy/dx
// Separate Computation for zero bias
// Date: 17 march 2007 Saturday
// Last update : 26 September 2008, Friday
// Checked on 25 February 2009, Wednesday
// -------------------------------------------
void mlp3::input_jacobian(double **Jyu)
{
  if(called_ij == 0)
    called_ij = 1;

  for(int i = 0; i < N[2]; i++)
    for(int j = 0; j < N[0]; j++)
    {
      double s1 = 0.0;  
      for(int k = 0; k < N[1]; k++)
        s1 += W2[i][k] * dactive(act, Xh[k]) * W1[k][j];
      Jyx[i][j] = dactive(oact, Xo[i]) * s1;

      if(Jyu != NULL)
        Jyu[i][j] = Jyx[i][j];
    }
}
//-----------------------------------------------------------------------
void mlp3::netinfo()
{
  cout << "\n\n--------------------------\n";
  cout << "Network Type: 3-Layer MLP" << endl;
  cout << "Nodes: " << N[0] << " - " << N[1] << " - " << N[2] << endl;
  cout << "Bias: " << bias << endl;
  cout << "Size of Weight Vector: " << nw1 << " + " << nw2 << " = " << nw << endl;
  if(bias > 0)
  {
    cout << "Size of Bias Weight Vector: " << nb1 << " + " << nb2 << " = " << nb << endl;
    cout << "Total size of weight Vector including bias weights = " << nw+nb << endl;
  }
  

  cout << "Weight initialization: ";
  if(bf == 1)
    cout << "Bipolar - random numbers between -1 to 1\n";
  else 
    cout << "Unipolar - random numbers between 0 to 1\n";
  //cout << "\nIf you want different initialization values use wt_init()\n";
  cout << "\nActivation function:" << endl;
  cout << "Output Neurons: ";

  if((int)oact[0] == 1)
    cout << "tansig" << "\talpha= " << oact[1] << endl;
  else if((int)oact[0] == 2)
    cout << "sigmoid" << "\talpha= " << oact[1] << endl;
  else if((int)oact[0] == 3) 
    cout << "Linear" << "\talpha= " << oact[1] << endl;
  else if((int)oact[0] == 4)
    cout << "Arctan" << "\talpha = " << oact[1] << endl;
  else if((int)oact[0] == 5)
    cout << "Gaussian" << "\talpha = " << oact[1] << endl;
  else if((int)oact[0] == 6)
    cout << "Tanh" << "\ta = " << oact[1] << "\tb = " << oact[2] << endl;
  else if((int)oact[0] == 7)
    cout << "Thin-plate-spline" << "\ta = " << oact[1] << endl;
  else if((int)oact[0] == 8)
    cout << "cosh" << "\ta = " << oact[1] << endl;
  else if((int)oact[0] == 9)
    cout << "tansigsq" << "\ta = " << oact[1] << endl;

  cout << "Hidden Neurons: ";
  if((int)act[0] == 1)
    cout << "tansig" << "\talpha= " << act[1] << endl;
  else if((int)act[0] == 2)
    cout << "sigmoid" << "\talpha= " << act[1] << endl;
  else if((int)act[0] == 3) 
    cout << "Linear" << "\talpha= " << act[1] << endl;
  else if((int)act[0] == 4)
    cout << "Arctan" << "\talpha = " << act[1] << endl;
  else if((int)act[0] == 5)
    cout << "Gaussian" << "\talpha = " << act[1] << endl;
  else if((int)act[0] == 6)
    cout << "Tanh" << "\ta = " << act[1] << "\tb = " << act[2] << endl;
  else if((int)act[0] == 7)
    cout << "Thin-plate-spline" << "\ta = " << act[1] << endl;
  else if((int)act[0] == 8)
    cout << "cosh" << "\ta = " << act[1] << endl;
  else if((int)act[0] == 9)
    cout << "tansigsq" << "\ta = " << act[1] << endl;
  cout << "----------------------------\n\n\n";
}//EOF
//---------------------------------------------------------
void mlp3::display_var(string s)
{
  cout << setprecision(4) ;

  if(s == "wt")
  {
    cout << setw(10) << setprecision(4); 
    cout << "\n------------------------------------\n";
    cout << "\nWeight matrix between input and hidden layer = W1: N[1]xN[0]\n\n";

    for(int i = 0; i < N[1]; i++)
    {
      for(int j = 0; j < N[0]; j++)
      {    
        cout << "W1[" << i << "][" << j << "]= ";
        cout << setiosflags(ios::showpos) << W1[i][j] << "  ";
        cout << resetiosflags(ios::showpos);
      }
      cout << endl;
    }

    cout<<"--------------------------------\n";
    cout<<"\nWeight matrix between hidden and output layer = W2': N[1] x N[2]\n\n";

    for(int i = 0; i < N[1]; i++)
    {
      for(int j = 0; j < N[2]; j++)
      {    
        cout << "W2[" << j << "][" << i << "]= ";
        cout << setiosflags(ios::showpos) << W2[j][i] << "  ";
        cout << resetiosflags(ios::showpos);
      }
      cout << endl;
    }
    cout << "-----------------------\n";
    if(bias > 0)
    {
      cout << "\nBias weights" << endl;
      for(int i = 0; i < nb1; i++)
      {
        if(i < nb2)
          cout << BW1[i] << "\t" << BW2[i] << endl;
        else
          cout << BW1[i] << endl;
      }
    }
    cout<<"--------------------------------\n\n\n";
  }
  else if(s == "io")
  {
    cout << "\n-----------------------------------\n";
    cout<<setiosflags(ios::showpoint);
    cout<<"Input-output pair of Each neuron\n";
    cout<<"Column: Layers  Row: neurons\n";
    cout<<"------------------------------\n";

    cout << setiosflags(ios::showpos);
    for(int i = 0; i < mnpl; i++)
    {
      for(int j = 0; j < 3; j++)   // layers
      {
        if(i < N[j])
        {
          if(j == 0)
            cout << right << setw(8) << Xi[i] << " " << right << setw(8) << Si[i] << "\t";
          else if( j == 1)
            cout << right << setw(8) << Xh[i] << " " << right << setw(8) << Sh[i] << "\t";
          else if(j == 2)
            cout << right << setw(8) << Xo[i] << " " << right << setw(8) << So[i] << "\t";
        }
        else
        {  
          if(j == 0)
            cout << right << setw(8) << "***" << " " << right << setw(8) << "***" << "\t";
          else if( j == 1)
            cout << right << setw(8) << "***" << " " << right << setw(8) << "***" << "\t";
          else if(j == 2)
            cout << right << setw(8) << "***" << " " << right << setw(8) << "***" << "\t"; 
        }

      }
      cout << endl;
    }
    cout<<resetiosflags(ios::showpos);
  }
  else if(s == "jyw")
  {
    cout << "\nWeight Jacobian matrix: Jyw' dim: " << nw  << "x" << N[2] << endl;
    cout << "-----------------------------------------------------\n";

    for(int i = 0; i < nw; i++)
    {
      for(int j = 0; j < N[2]; j++)
      {
        cout << "Jyw[" << j << "][" << i << "]= ";
        cout << setiosflags(ios::showpos) << right << setw(10) << Jyw[j][i] << "\t";
        cout << resetiosflags(ios::showpos);
      }
      cout << endl;
    }
    cout << "-----------------------------\n";
  }
  else if(s == "jyu")
  {
    cout << "\nInput jacobian matrix: Jyx dim: " << N[2] << "x" << N[0] << endl;

    for(int i = 0; i < N[2]; i++)
    {
      for(int j = 0; j < N[0]; j++)
      {
        cout << "Jyx[" << i << "][" << j << "]= ";
        cout << setiosflags(ios::showpos) << right << setw(10) << Jyx[i][j] << "\t";
        cout << resetiosflags(ios::showpos);
      }
      cout << endl;
    }
    cout << "--------------------------------\n"; 
  }
  else if(s == "idx")
  {
    cout << "\nWeight Indices:\t Weight Vector (W) \n";
    cout << "--------------------------------------------------------\n";
    for(int i = 0; i < nw; i++)
    {
      if(i < nw1)
        cout << i << " -----> " << "W1(" << WI1[i][0] << ", " 
          << WI1[i][1] << "):\t" << "W[" << i << "]= "<< W[i] << endl;
      else
        cout << i << " -----> " << "W2(" << WI2[i-nw1][0] << ", " 
          << WI2[i-nw1][1] << "):\t" << "W[" << i << "]= " << W[i] << endl;
    }
    cout << "-------------------------------------------------------" << endl;
  }
  else
  {
    cout << "Invalid Options .." << endl;
    cout << "Try 'wt', 'jyw', 'jyu', 'io', 'idx'" << endl;
    exit(-1);
  }
}//EOF
//----------------------------------------------------
void mlp3::get_parameters(double *wt)
{
  for(int i = 0; i < nw; i++)
    wt[i] = W[i];
  if(bias > 0)
  {
    for(int j = 0; j < nb; j++)
      wt[nw+j] = BW[j];
  }
}
//------------------------------
void mlp3::load_parameters(double *wt)
{
  for(int i = 0; i < nw; i++)
  {
    W[i] = wt[i];
    if(i < nw1)
      W1[WI1[i][0]][WI1[i][1]] = W[i];
    else
      W2[WI2[i-nw1][0]][WI2[i-nw1][1]] = W[i];
  }

  if(bias > 0)
  {
    for(int j = 0; j < nb; j++)
    {
      BW[j] = wt[nw+j];
      if(j < nb)
        BW1[j] = BW[j];
      else
        BW2[j-nb1] = BW[j];
    }
  }
}
//----------------------------
// Returns size of weight vector
int mlp3::wmax()
{
  if(bias > 0)
    return nw+nb;
  else
    return nw;
}
//------------------------------------------
//Reinitialize weights
// Separate initialization for zero bias case
// Last update: 26 September 2008, Friday
// --------------------------------------------
void mlp3::wt_init(double max, double min)
{

  //Initialize weight matrix W1   
  for(int i = 0; i < N[1]; i++)
    for(int j = 0; j < N[0]; j++)
      W1[i][j] = min + (max - min) * rand()/(double)RAND_MAX;


  //Initialize weight matrix W2
  for(int i = 0; i < N[2]; i++)
    for(int j = 0; j < N[1]; j++)
      W2[i][j] = min + (max - min) * rand()/(double)RAND_MAX;

  for(int i = 0; i < nb; i++)
  {
    if(i < nb1)
      BW1[i] = min + (max - min) * rand()/(double)RAND_MAX;
    else
      BW2[i-nb1] = min + (max - min) * rand()/(double)RAND_MAX;
  }

  // Weight vector : W = [ W1 | W2 ]
  for(int i = 0; i < nw; i++)
  {
    if(i < nw1)
      W[i] = W1[WI1[i][0]][WI1[i][1]];
    else
      W[i] = W2[WI2[i-nw1][0]][WI2[i-nw1][1]];
  }

  for(int i = 0; i < nb; i++)
  {
    if(i < nb1)
      BW[i] = BW1[i];
    else
      BW[i] = BW2[i-nb1];
  }
}
//-----------------------------------------------------
// Gradient Descent Weight Update
// Uses explicit Derivative
// 
// Date: 08 June 2007 Friday
// Status:  checked on 24 Feb 2009, Tuesday
// Remark: Another check necessary
//
// ---------------------------------------------------
void mlp3::gd_update(double  yd[], double eta)
{
  double s1, delw;
  double *Yd;


  Yd = new double [N[2]];
  for(int i = 0; i < N[2]; i++)
    Yd[i] = yd[i];


  for(int j = 0; j < nw; j++)
  {
    if(j < nw1) //weights on first layer
    {
      s1 = 0.0;
      for(int i = 0; i < N[2]; i++)
        s1 += (Yd[i] - So[i]) * dactive(oact, Xo[i]) * W2[i][WI1[j][0]];
      delw = eta * dactive(act, Xh[WI1[j][0]]) * Si[WI1[j][1]] * s1;

      W[j] = W[j] + delw;
      W1[WI1[j][0]][WI1[j][1]] = W[j];
    }
    else  // weights on second layer
    {
      delw = eta * (Yd[WI2[j-nw1][0]] - So[WI2[j-nw1][0]]) * 
        dactive(oact, Xo[WI2[j-nw1][0]]) * Sh[WI2[j-nw1][1]];

      W[j] = W[j] + delw;
      W2[WI2[j-nw1][0]][WI2[j-nw1][1]] = W[j];
    } 
  }

  //update bias weights
  for(int j = 0; j < nb; j++)
  {
    if(j < nb1)
    {
      s1 = 0.0;
      for(int i = 0; i < N[2]; i++)
        s1 += (Yd[i] - So[i]) * dactive(oact, Xo[i]) * W2[i][j];
      delw = eta * dactive(act, Xh[j]) * bias * s1;
    }
    else
      delw = eta * (Yd[j-nb1] - So[j-nb1]) * dactive(oact, Xo[j-nb1]) * bias;
  }
}
//------------------------------------------
// Gradient-Descent Weight Update: 
// Makes use of jacobian Matrix
// Must call wt_jacobian() function before this
// Date: 08 June 2007 Friday
// Warning: This code has not been checked ...
// ------------------------------------------------
void mlp3::gd_update2(double yd[], double eta)
{
  if(called_wj == 0)
  {
    cout << "wt_jacobian function must be called before calling this function.\n";
    cout << " ... exiting!\n";
    exit(-1);
  }

  double *Yd,s1;

  Yd = new double[N[2]];

  for(int i = 0; i < N[2]; i++)
    Yd[i] = yd[i];

  for(int i = 0; i < nw; i++)
  {

    s1 = 0.0;
    for(int j = 0; j < N[2]; j++)
      s1 += (Yd[j] - So[j]) * Jyw[j][i];

    W[i] = W[i] + eta * s1;

    if(i < nw1)
      W1[WI1[i][0]][WI1[i][1]] = W[i];
    else
      W2[WI2[i-nw1][0]][WI2[i-nw1][1]] = W[i];
  }

  for(int i = 0; i < nb; i++)
  {
    s1 = 0.0;
    for(int j = 0; j < N[2]; j++)
      s1 += (Yd[j] - So[j]) * Jyb[j][i];

    BW[i] = BW[i] + eta * s1;

    if(i < nb1)
      BW1[i] = W[i];
    else
      BW2[i-nb1] = W[i];
  }

}//EOF
//----------------------------------------------
      
       
         

   
