/*--------------------------------------------------------------------

    NEURAL NETWORK LIBRARY FOR C/C++
    Copyright (C) 2007-2010   Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

-------------------------------------------------------------------------- */ 

#include<iostream>
#include<fstream>
#include<cstdlib>
#include<nnet.h>
#include<cmath>

using namespace nnet;


//Default Constructor
ksom::ksom()
{
  cout << "This is default constructor for KSOM Networks." << endl;
  cout << "This creates an empty RBFN network" << endl;
  cout << "You can use overloaded = operator to assign another KSOM to it" << endl;

  DIM = 0;
  NI = 0;
  NO = 0;
  NodeCnt = 0;

  // Gaussian activation function for neurons
  act[0] = 5.0;
  act[1] = 1.0;
  act[2] = 0.0;

  idx = NULL;
  Ridx = NULL;
  X = NULL;
  Y = NULL;
  C = NULL;
  W = NULL;
  S = NULL;
  Z = NULL; 

}
//------------------------------
ksom::ksom(int lattice_dim, int Nidx[3], int nip, double af[3], int nop)
{
  //Dimension of KSOM lattice - 1D, 2D or 3D
  DIM = lattice_dim;

  //Dimension of Input space
  NI = nip;

  //Dimension of Output space (= 0, by default)
  NO = nop;

  //spread of gaussian function
  if(af != NULL)
  {
    for(int i = 0; i < 3; i++)
      act[i] = af[i];
  }
  else
  {
    act[0] = 5.0;
    act[1] = 1.0;
    act[2] = 0.0;
  }

  for(int i = 0; i < 3; i++)
    N[i] = Nidx[i];

  //Maximum number of nodes in each direction
  int cnt = 1;
  for(int i = 0; i < DIM; i++)
  {
    N[i] = Nidx[i];
    cnt = cnt * N[i];
  }

  //Total number of nodes in KSOM lattice
  NodeCnt = cnt;

  allocate_memory();
  initialize_parameters(1.0, -1.0);

}

//-----------------------------
void ksom::allocate_memory()
{
  try
  {
    X = new double [NI];
    idx = new int *[NodeCnt];
    C = new double *[NodeCnt];
    S = new double [NodeCnt];
    Z = new double [NodeCnt];

    for(int i = 0; i < NodeCnt; i++)
      C[i] = new double [NI];

    //If there is an output layer
    if(NO > 0)
    {
      Y = new double [NO];
      W = new double *[NO];
      for(int i = 0; i < NO; i++)
        W[i] = new double [NodeCnt];
    }


    if(DIM == 1)
    {
      Ridx = new int **[N[0]];
      for(int i = 0; i < N[0]; i++)
      {
        Ridx[i] = new int *[1];
        Ridx[i][0] = new int [1];
      }

      for(int i = 0; i < NodeCnt; i++)
        idx[i] = new int[1];

      int k = 0;
      for(int i = 0; i < N[0]; i++)
      {
        idx[k][0] = i;    
        Ridx[i][0][0] = k;

        k = k + 1;
      }
    }
    else if(DIM == 2)
    {
      Ridx = new int **[N[0]];
      for(int i = 0; i < N[0]; i++)
      {
        Ridx[i] = new int *[N[1]];
        for(int j = 0; j < N[1]; j++)
          Ridx[i][j] = new int [1];
      }

      for(int i = 0; i < NodeCnt; i++)
        idx[i] = new int[2];

      int k = 0;
      for(int i = 0; i < N[0]; i++)
        for(int j = 0; j < N[1]; j++)
        {
          idx[k][0] = i;
          idx[k][1] = j;

          Ridx[i][j][0] = k;
          k = k + 1;
        }
    }
    else if(DIM == 3)
    {
      Ridx = new int **[N[0]];
      for(int i = 0; i < N[0]; i++)
      {
        Ridx[i] = new int *[N[1]];
        for(int j = 0; j < N[1]; j++)
          Ridx[i][j] = new int [N[2]];
      }

      for(int i = 0; i < NodeCnt; i++)
        idx[i] = new int[3];

      int k = 0;
      for(int i = 0; i < N[0]; i++)
        for(int j = 0; j < N[1]; j++)
          for(int l = 0; l < N[2]; l++)
          {
            idx[k][0] = i;  //store forward index
            idx[k][1] = j;
            idx[k][2] = l;
            Ridx[i][j][l] = k; //store backward index

            k = k + 1;
          }
    }
    else
    {
      cout << "Wrong value for Lattice dimension." << endl;
      cout << "DIM = 1, 2 or 3" << endl;
      exit(-1);
    }
  }
  catch(bad_alloc)
  {
    cout << "Memory allocation failure in ksom class" << endl;
    exit(-1);
  }

}//EOF
//-------------------------------------
void ksom::initialize_parameters(double max, double min)
{
  for(int k = 0; k < NodeCnt; k++)
  {
    for(int i = 0; i < NI; i++)
      C[k][i] = min + (max - min) * gsl_rng_uniform(r);

    if(NO > 0)
    {
      for(int i = 0; i < NO; i++)
        W[i][k] = min + (max-min) * gsl_rng_uniform(r);
    }

    S[k] = 0.0;
    Z[k] = 0.0;
  }
}//EOF
//------------------------------------------------------
// Destructor for KSOM
ksom::~ksom()
{
  deallocate_memory();
}//EOF
//--------------------------------
void ksom::deallocate_memory()
{
  free_memory(X);
  free_memory(Z);
  free_memory(S);
  free_memory(C, NodeCnt);
  if(NO > 0)
  {
    free_memory(W, NO);
    delete [] Y;
  }
  free_memory(idx, NodeCnt);

  if(DIM > 0)
  {
    if(DIM == 1)
      free_memory(Ridx, N[0], 1);
    else if(DIM == 2)
      free_memory(Ridx, N[0], N[1]);
    else if(DIM == 3)
      free_memory(Ridx, N[0], N[1]);
    else
    {
      cout << "Invalid value for lattice dimension" << endl;
      exit(-1);
    }
  }
}
//----------------------------------------------
// Copy Constructor for KSOM

ksom::ksom(const ksom &a)
{

  //Dimension of KSOM lattice - 1D, 2D or 3D
  DIM = a.DIM;

  //Dimension of Input space
  NI = a.NI;

  //Dimension of Output space (= 0, by default)
  NO = a.NO;

  //spread of gaussian function
  for(int i = 0; i < 3; i++)
    act[i] = a.act[i];

  //Maximum number of nodes in each direction
  for(int i = 0; i < DIM; i++)
    N[i] = a.N[i];

  //Total number of nodes in KSOM lattice
  NodeCnt = a.NodeCnt;

  allocate_memory();

  for(int i = 0; i < NodeCnt; i++)
  {
    for(int j = 0; j < NI; j++)
      C[i][j] = a.C[i][j];

    if(NO > 0)
    {
      for(int j = 0; j < NO; j++)
        W[j][i] = a.W[j][i];
    }
  }
}//EOF
//---------------------------------------------
// Create clusters in input space
void ksom::create_clusters(double eta, double sig, const double *xd)
{
  int *w_idx = new int[DIM];
  int winner;

  // Select a winner in cartesian space
  double Min_w = 50000;
  for(int k = 0; k < NodeCnt; k++)
  {
    double sw = 0.0;
    for(int l = 0; l < NI; l++)  
      sw += pow((C[k][l] - xd[l]), 2.0);
    double zw = sqrt(sw);

    if(zw < Min_w)
    {
      Min_w = zw;
      winner = k;
    }
  }//for each index

  //winner index
  for(int i = 0; i < DIM; i++)
    w_idx[i] = idx[winner][i];

  for(int k = 0; k < NodeCnt; k++)
  {
    //compute lattice distance of each neuron
    //from winner neuron
    double dw = 0;
    for(int i = 0; i < DIM; i++)
      dw += pow((w_idx[i] - idx[k][i]), 2.0);

    //compute neighborhood function
    double hw = exp( -dw / (2 * sig * sig) );

    //update neurons 
    for (int l = 0; l < NI; l++) 
      C[k][l] += eta * hw * (xd[l] - C[k][l]); 
  }
}//EOF
//-------------------------------------------------
void ksom::network_output(const double x[], double y[])
{
  if(NO <= 0)
  {
    cout << "This KSOM network does not have an output layer" << endl;
    exit(-1);
  }

  for(int i = 0; i < NI; i++)
    X[i] = x[i];

  for(int k = 0; k < NodeCnt; k++)
  {
    double s1 = 0.0;
    for(int i = 0; i < NI; i++)
      s1 += pow((x[i] - C[k][i]), 2.0);

    Z[k] = sqrt(s1);
    S[k] = active_func(act,Z[k]);
  }

  for(int i = 0; i < NO; i++)
  {
    double s1 = 0.0;
    for(int k = 0; k < NodeCnt; k++)
      s1 += W[i][k] * S[k];
    Y[i] = s1;
    y[i] = Y[i];
  }
}//EOF
//------------------------------------------------------
void ksom::save_parameters(const char *filename)
{
  ofstream f1(filename);
  for(int i = 0; i < NodeCnt; i++)
  {
    for(int j = 0; j < NI; j++)
      f1 << C[i][j] << "\t";
    f1 << endl;
  }
  f1 << endl << endl;

  if(NO > 0)
  {
    for(int k = 0; k < NodeCnt; k++)
    {
      for(int i = 0; i < NO; i++)
        f1 << W[i][k] << "\t";
      f1 << endl;
    }
  }
  f1.close();
  cout << "KSOM parameters are saved\n" << endl;
}//EOF
//-----------------------------------------------------
void ksom::load_parameters(const char *filename)
{
  ifstream f1(filename);
  for(int i = 0; i < NodeCnt; i++)
  {
    for(int j = 0; j < NI; j++)
      f1 >> C[i][j] ;
  }

  if(NO > 0)
  {
    for(int k = 0; k < NodeCnt; k++)
    {
      for(int i = 0; i < NO; i++)
        f1 >> W[i][k] ;
    }
  }
  f1.close();
  cout << "KSOM parameters are saved\n" << endl;
}//EOF
//---------------------------------------------
// Displaying network information
ostream& nnet::operator<<(ostream& os, const ksom& a)
{
  os << "\n\n------------------------------------------\n";
  os << "Type:" << " Kohonen Self-Organizing MAP "<<endl;
  os << "Lattice Dimension = " << a.DIM << endl;
  os << "Number of Nodes = ";
  if(a.DIM > 0)
  {
    if(a.DIM == 1)
      os << a.N[0] << endl;
    else if(a.DIM == 2)
      os << a.N[0] << " x " << a.N[1] << endl;
    else if(a.DIM == 3)
      os << a.N[0] << " x " << a.N[1] << " x " << a.N[2] << endl;
    else
    {
      //cout << "Function : " << __PRETTY_FUNCTION__ << endl;
      cout << "Error at Line " << __LINE__ << " in file " << __FILE__ << endl;
      cout << "Wrong value for lattice dimension" << endl;
      exit(-1);
    }
  }
  else
  {
    cout << " 0  (Empty network)" << endl;
  }
  os << "Dimension of Input Space = " << a.NI << endl;
  os << "Dimension of Output Space = " << a.NO << endl;

  if(a.NO > 0)
    os << "Activation function for nodes = " << (int) a.act[0] << endl;

  return os;
}//EOF 
//---------------------------------------------------
// Overloading assignment operator for KSOM 
// This is suggested by Awhan Patnaik (awimagic@gmail.com)
// Date: 24 March 2010, Wednesday
// Remark: Not tested yet ...
//
// Note that assignment destroys the content of destination object.
// --------------------------------------------------
ksom & ksom::operator=(const ksom & a)
{

  if( this == &a )
  {
    return *this;
  }

  //delete previously allocated memory
  deallocate_memory();

  DIM = a.DIM;
  NI = a.NI;
  NO = a.NO;

  //spread of gaussian function
  for(int i = 0; i < 3; i++)
    act[i] = a.act[i];

  //Maximum number of nodes in each direction
  for(int i = 0; i < DIM; i++)
    N[i] = a.N[i];

  //Total number of nodes in KSOM lattice
  NodeCnt = a.NodeCnt;

  allocate_memory();

  for(int i = 0; i < NodeCnt; i++)
  {
    for(int j = 0; j < NI; j++)
      C[i][j] = a.C[i][j];

    if(NO > 0)
    {
      for(int j = 0; j < NO; j++)
        W[j][i] = a.W[j][i];
    }
  }
  return *this ;
}


  
