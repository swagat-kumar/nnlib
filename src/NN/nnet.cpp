/* -------------------------------------------------------------------- 
    Function Definitions for NEURAL NETWORK CLASS
    Copyright (C) 2007-2009 Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 ---------------------------------------------------------------------------*/

/* =====================================
 * 1 - tansig
 * 2 - sigmoid
 * 3 - purelin
 * 4 - arctan
 * 5 - gauss
 * 6 - hyptan
 * 7 - tps
 * 8 - hypcos
 * 9 - tansigsq
 * 10 - square
 *
 * ================================ */
                                        
#include<iostream>
#include<cmath>
#include<new>         
#include<string>
#include<iomanip>
#include <nnet.h>
#include<cassert>
#include <cstdlib>


using namespace std;
using namespace nnet;

//Constructor for base class
func::func()
{
  gsl_rng_env_setup();

  long int seed = gsl_rng_default_seed;

  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  
  if(seed == 0)
  {
    seed = (time(NULL));
    gsl_rng_set (r, seed);
  }

  printf("\n\n--------------------------------------\n");
  printf ("GSL Random Number Generator type: %s\n", gsl_rng_name (r));
  printf ("seed = %lu\n", seed);
  printf("Use Environment variables to change seed and type of random number generator\n");
  printf("--------------------------------------\n\n");
}
//-----------------------------------
//Destructor for base class
func::~func()
{
  if(r != NULL)
    gsl_rng_free (r); 
}
//------------------------------------

/* NEURON ACTIVATION FUNCTIONS */
//--------------------------------------
//unipolar sigmoid
double func::sigmoid(double x,double alpha)
{
    return(1.0/(1+exp(-alpha*x)));
}
//--------------------------------------
//bipolar sigmoid
double func::tansig(double x,double alpha)
{
   return(-1.0+2.0/(1+exp(-alpha*x)));
}
//-----------------------
// y = 2/pi * arctan(a * x)
double func::arctan(double x, double a)
{
  if(a <= 0)
  {
    cout << "the second argument to arctan must be positive\n";
    exit(-1);
  }
  double s = 2/M_PI * atan(a*x);
  return s;
}
//----------------------
// Gaussian function
// Date: 16/03/07
double func::gauss(double x, double sigma)
{
   return(exp(-x*x/(2*sigma*sigma)));
}   
//--------------------------
//linear func function;y=a*x
double func::purelin(double x,double alpha=1)
{
   return(alpha*x);
}          
//-------------
// tanh function
// Date: 31/05/07
double func::hyptan(double x, double a, double b)
{
  return (a * tanh(b*x));
}
//--------------------
// Thin-plate-spline function
// Date: 31/05/07
double func::tps(double x, double a)
{
  double s;
  s = a*a*x*x*log(a*x);
  return s;
}
//-------------------------------------
// Hyperbolic cosine function
// Date: 02/06/07
double func::hypcos(double x)
{
  return(cosh(x) - 1);
}
//-------------------------
// square of tansig function
//Date: 30 Sep. 2008 Tuesday
// Note that : y = (1-exp(-x))/(1+exp(-x))
// But this is not defined for x = -inf
// -------------------------------------------
double func::tansigsq(double x, double a=1)
{
  double z = a * x;

  double y = -1.0 + 2.0/(1+exp(-z));

  return (0.5*y*y);
}
//-----------------------------
double func::square(double x, double a = 1)
{
  double z = a * x;
  double y = z * z;

  return(y);
}

// ===============================

// Derivative of tansig function 
double func::dtansig(double x,double alpha)
{ 
  // Note that x is the input Neuron.
  double y = tansig(x, alpha);

  return(0.5*alpha*(1+y)*(1-y)); 
}
//--------------------------
//Derivative of sigmoid function
double func::dsigmoid(double x,double alpha)
{
  // Note that x is the input to the neuron
  double y = sigmoid(x, alpha);
  return(alpha*y*(1-y));
}
//--------------------------
// Derivative of arctan 
//
// dy/dx = 2a / (pi * (1+a^2x^2))
//
// Note that x is the input to the neuron
// ---------------------------------------
double func::darctan(double x, double a)
{
  double s = 2 * a / (M_PI*(1 + a*a*x*x));
  return s;
}                                                                                          
//----------------------------
//Derivative of Gaussian function
double func::dgauss(double x, double sigma)
{
  // Note that x is the input to the neuron
  // and y is the output of the neuron
  double y;
  y = gauss(x, sigma);
  return(-x*y/(sigma*sigma)); 
}
//--------------
//Derivative of linear function dy/dx=a 
// x is the input to the neuron
double func::dlin(double x, double alpha=1)
{
  x = x;
  return(alpha);
}
//--------------
//Derivative of tanh function
//Date: 31/05/07
// x is the input to the neuron
double func::dhyptan(double x, double a, double b)
{
  double s;
  s = a * b * (1 - tanh(b*x) * tanh(b*x));

  return s;
}
//-------------------
//Derivative of tps function
// Date: 31/05/07
double func::dtps(double x, double a)
{
  double s, t;
  s = a * x;
  t = a * s * (1 + 2 * log(s));
  return t;
}
//-----------------------------------
// Derivative of cosh(x)
// Date: 02/06/07      
double func::dhypcos(double x)
{
  return (sinh(x));
}
//--------------------------
double func::dtansigsq(double x, double a=1)
{
  //x is the input to dtansigsq function

  double z = a * x;
  double y = -1.0 + 2.0/(1+exp(-z));

  double s = 0.5 * a * y * (1+y) * (1-y);

  return(s);
}
//-----------------------------------
double func::dsquare(double x, double a=1)
{
  // x is the input to dsquare function

  double y = 2 * a * a * x;

  return(y);
}







// ---------------------------
//  * GENERAL FUNCTIONS * 
//------------------------------
//Kronecker delta function
int func::kdel(int i,int j){
   return((i==j)?1:0);
} 
//------------------------------------------
double func::active_func(double act[], double x)
{
  int af = (int)act[0];
  double s;
  
  if(af == 1)
   s = tansig(x, act[1]);
  else if(af == 2)
    s = sigmoid(x, act[1]);
  else if(af == 3)
    s = purelin(x, act[1]);
  else if(af == 4)
    s = arctan(x, act[1]);
  else if(af == 5)
    s = gauss(x, act[1]);
  else if(af == 6)
    s = hyptan(x, act[1], act[2]);
  else if(af == 7)
    s = tps(x, act[1]);
  else if(af == 8)
    s = hypcos(x);
  else if(af == 9)
    s = tansigsq(x, act[1]);
  else if(af == 10)
    s = square(x, act[1]);
  else
  {
    cout << "nnet::func: invalid argument for 'active_func': choose from 1 -10\n";
    exit(-1);
  }
  return s;
}
//---------------------------------------------------
double func::dactive(double act[], double x)
{
  int af = (int)act[0];
  double s;
  
  if(af == 1)
    s = dtansig(x, act[1]);
  else if(af == 2)
    s = dsigmoid(x, act[1]);
  else if(af == 3)
    s = dlin(x, act[1]);
  else if(af == 4)
    s = darctan(x, act[1]);
  else if(af == 5)
    s = dgauss(x, act[1]);
  else if(af == 6)
    s = dhyptan(x, act[1], act[2]);
  else if(af == 7)
    s = dtps(x, act[1]);
  else if(af == 8)
    s = dhypcos(x);
  else if(af == 9)
    s = dtansigsq(x, act[1]);
  else if(af == 10)
    s = dsquare(x, act[1]);
  else
  {
    cout << "nnet::func: invalid argument for 'dactiv_func': choose from 1 - 10 \n";
    exit(-1);
  }
  return s;
}
// -----------------------------------
// This is for generating random numbers with uniform
// distribution - Taken from NRUTIL package
// Date: 20/10/06 : Friday 
// ---------------- Numerical recipes in C ------------------
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)
 
double func::ran1(long *idum)
{
	int j;
	long k;
	static long iy=0;
	static long iv[NTAB];
	double temp;

	if (*idum <= 0 || !iy) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0) *idum += IM;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0) *idum += IM;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = *idum;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}
#undef IA
#undef IM
#undef AM
#undef IQ
#undef IR
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX  
//---------------------------------
// Memory allocation routines
double* func::array(int m)
{
  double *x;
  try
  {
    x = new double [m];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }
  for(int i = 0; i < m; i++)
    x[i] = 0.0;
  return(x);
}
//----------------------------------------
double** func::array(int m, int n)
{
  double **x;
  try
  {
    x = new double *[m];
    for(int i = 0; i < m; i++)
      x[i] = new double [n];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      x[i][j] = 0.0;

  return(x);
}
//--------------------------------------
int** func::intarray(int m, int n)
{
  int **x;
  try
  {
    x = new int *[m];
    for(int i = 0; i < m; i++)
      x[i] = new int [n];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      x[i][j] = 0.0;

  return(x);
}
//---------------------------------------
double*** func::array(int m, int n, int p)
{
  double ***x;
  try
  {
    x = new double **[m];
    for(int i = 0; i < m; i++)
    {
      x[i] = new double *[n];
      for(int j = 0; j < n; j++)
        x[i][j] = new double [p];
    }
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      for(int k = 0; k < p; k++)
        x[i][j][k] = 0.0;

  return(x);
}
//-------------------------------------------
// Memory deallocation routines
void func::free_memory(double *x)
{
  if(x != NULL)
    delete [] x;
}
//deallocating integer array
void func::free_memory(int *x)
{
  if(x != NULL)
    delete [] x;
}
//deallocating an 2D array
// where m is the first dimension, i.e., x[m][n]
void func::free_memory(double **x, int m)  
{
  if(x != NULL)
  {
    for(int i = 0; i < m; i++)
    {
      delete [] x[i];
    }
    delete [] x;
  }
}
//deallocating integer arrays
void func::free_memory(int **x, int m)
{
  if(x != NULL)
  {
    for(int i = 0; i < m; i++) 
    {
      delete [] x[i];
    }
    delete [] x;
  }
}
//deallocating 3D array x[m][n][p]
void func::free_memory(double ***x, int m, int n)
{
  if(x != NULL)
  {
    for(int i = 0; i < m; i++)
    {
      for(int j = 0; j < n; j++)
        delete [] x[i][j];
      delete [] x[i];
    }
    delete [] x;
  }
}
//deallocating 3D integer array x[m][n][p]
void func::free_memory(int ***x, int m, int n)
{
  if( x != NULL)
  {
    for(int i = 0; i < m; i++)
    {
      for(int j = 0; j < n; j++)
        delete [] x[i][j];
      delete [] x[i];
    }
    delete [] x;
  }
}
//---------------------------------------

         
       
       
  

  
         
