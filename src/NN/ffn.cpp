 /*--------------------------------------------------------------------

    NEURAL NETWORK CLASS LIBRARY
    Copyright (C) 2007   Swagat Kumar (swagat.kumar@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

-------------------------------------------------------------------------- */   

#include<iostream>
#include<new>
#include<iomanip>
#include<string>
#include <nnet.h>
#include<cmath>
#include<fstream>
#include<cstdlib>

using namespace std;
using namespace nnet;

//Default Constructor
ffn::ffn()
{
  printf("\nDefault Contstructor for FFN");
  printf("It creates an empty FFN\n");
  printf("Assignment operator can be used to assign a new object to it\n\n");

  //No. of Layers in FFN
  L = 3;          

  // bipolar flag : 1 -bipolar, 0 - unipolar
  bf = 0;   
  if(bf == 1)
  {
    rmax = 1.0;
    rmin = -1.0;
  }
  else if(bf == 0)
  {
    rmax = 1.0;
    rmin = 0.0;
  }
  else
  {
    cout << "Invalid value for bipolar flag. choose '0/1'\n";
    exit(-1);
  }

  //Bias value
  bias = 0;

  double af[3] = {1.0, 1.0, 0.0};
  //Activation function for hidden and output layers
  for(int i = 0; i < 3; i++)
  {
    act[i] = af[i];
    oact[i] = af[i];
  }

  //Flags for indicating the availability of Jacobian matrices
  called_ij = 0;
  called_wj = 0;

  // N contains the number of neurons in each layer
  // Layer Index : 0, 1, 2, ... L-1
  // mnpl is the maximum number of nodes in any layer

  N = new int [L];
  N[0] = 2;
  N[1] = 2;
  N[2] = 1;

  mnpl = 0;
  for(int i = 0; i < L; i++)
  {
    if(N[i] > mnpl)
      mnpl = N[i];
  }

  //maximum size of weight vector
  int k = 0;
  for(int l = 0; l < L-1; l++)
    k += N[l+1]*(N[l]+1);
  WM = k; 

  allocate_memory();

}//EOF
//--------------------------------------------------------------------
// Constructor 2 for FFN
// Default settings
// - Hidden and output layers have same activation function
// - bipolar flag is set
// - unity bias is applied to all neurons 
// -------------------------------------------------------------------
ffn::ffn(int NI[], int L1, double af[])
{

  //No. of Layers in FFN
  L = L1;          

  // bipolar flag is set
  bf = 1;   
  rmax = 1.0;
  rmin = -1.0;

  //Unity bias is applied to all neurons
  bias = 1.0;

  //Same Activation function for hidden and output layers
  for(int i = 0; i < 3; i++)
  {
    act[i] = af[i];
    oact[i] = af[i];
  }

  //Flags for indicating the availability of Jacobian matrices
  called_ij = 0;
  called_wj = 0;

  // N contains the number of neurons in each layer
  // Layer Index : 0, 1, 2, ... L-1
  // mnpl is the maximum number of nodes in any layer

  N = new int[L];
  mnpl = 0;
  for(int i = 0; i < L; i++)
  {
    N[i] = NI[i];           
    if(N[i] > mnpl)
      mnpl = N[i];
  }

  //maximum size of weight vector
  int k = 0;
  for(int l = 0; l < L-1; l++)
    k += N[l+1]*(N[l]+1);
  WM = k; 

  allocate_memory();
}

//----------------------------------------------------------
/*constructor for nnet:ffn class*/
ffn::ffn(int NI[], int L1, double af[], double oa[], int bpf, double b)
{

  //No. of Layers in FFN
  L = L1;          

  // bipolar flag : 1 -bipolar, 0 - unipolar
    bf = bpf;   
  if(bf == 1)
  {
    rmax = 1.0;
    rmin = -1.0;
  }
  else if(bf == 0)
  {
    rmax = 1.0;
    rmin = 0.0;
  }
  else
  {
    cout << "Invalid value for bipolar flag. choose '0/1'\n";
    exit(-1);
  }

  //Bias value
  bias = b;

  //Activation function for hidden and output layers
  for(int i = 0; i < 3; i++)
  {
    act[i] = af[i];
    oact[i] = oa[i];
  }

  //Flags for indicating the availability of Jacobian matrices
  called_ij = 0;
  called_wj = 0;

  // N contains the number of neurons in each layer
  // Layer Index : 0, 1, 2, ... L-1
  // mnpl is the maximum number of nodes in any layer

  N = new int[L];
  mnpl = 0;
  for(int i = 0; i < L; i++)
  {
    N[i] = NI[i];           
    if(N[i] > mnpl)
      mnpl = N[i];
  }

  //maximum size of weight vector
  int k = 0;
  for(int l = 0; l < L-1; l++)
    k += N[l+1]*(N[l]+1);
  WM = k; 

  allocate_memory();
}
//----------------------------------------------
// COPY CONSTRUCTOR for ffn
// ---------------------------------------
ffn::ffn(const ffn &a)
{
   L = a.L;
   
   bf = a.bf;
   bias = a.bias;

   WM = a.WM;

   for(int i = 0; i < 3; i++)
   {
      act[i] = a.act[i];
      oact[i] = a.oact[i];
   }

   N = new int [L];
   for(int l = 0; l < L; l++)
     N[l] = a.N[l];
   mnpl = a.mnpl;

   allocate_memory();

   //Re-initialization of class elements
      
   for(int l = 0; l < L; l++)
     for(int i = 0; i <= N[l]; i++)
     {
       S[l][i] = a.S[l][i];
       X[l][i] = a.X[l][i];
     }
     
   for(int i = 0; i < N[L]; i++)
      for(int j = 0; j < WM; j++)
         Jyw[i][j] = a.Jyw[i][j];

   for(int i = 0; i < N[L]; i++)
      for(int j = 0; j < N[0]; j++)
         Jyu[i][j] = a.Jyu[i][j];
      
      
   for(int l = 0; l < L-1; l++)
     for(int i = 1; i <= N[l+1]; i++)
       for(int j = 0; j <= N[l]; j++)
         W[l][i][j] = a.W[l][i][j];

   for(int i = 0; i < a.WM; i++)
      for(int j = 0; j < 3; j++)
         Wt_index[i][j] = a.Wt_index[i][j];

   for(int i = 0; i < N[L]; i++)
      Y[i] = a.Y[i];
}          
//----------------------------------------------------
// Destructor FOR ffn CLASS
//-----------------------------------------------------------
ffn::~ffn()
{
  deallocate_memory();
}//EOF
//-----------------------------------------
void ffn::deallocate_memory()
{
  free_memory(X, L);
  free_memory(S, L);
  free_memory(Jyw, N[L-1]);
  free_memory(Jyu, N[L-1]);
  free_memory(Wt_index, WM);

  free_memory(AW);
  free_memory(Y);


  //free memory allocate for W and WI
  if(W != NULL && WI != NULL)
  {
    for(int l = 0; l < L-1; l++)
    {
      for(int i = 0; i <= N[l+1]; i++)
      {
        delete [] W[l][i];
        delete [] WI[l][i];
      }
      delete [] W[l];
      delete [] WI[l];
    }
    delete [] W;
    delete [] WI;
  }
  free_memory(N);

}//EOF
//---------------------------------------------
// Memory allocation routine
void ffn::allocate_memory()
{
  try
  {
    //X : Input to each layer: L x (N[l] + 1), l = 0, 1, ..., L-1
    //S : Output of each layer: L x (N[l] + 1), l = 0, 1, ..., L-1

    X = new double *[L];       
    S = new double *[L];

    //initializing states
    for(int l = 0; l < L; l++)
    {
      X[l] = new double [N[l]+1]; 
      S[l] = new double [N[l]+1];

      for(int i = 0; i <= N[l]; i++)
      {
        if(i == 0) 
        {
          S[l][i] = bias;
          X[l][i] = bias;
        }
        else  
        {
          S[l][i] = 0.0;
          X[l][i] = 0.0;
        }
      }
    }

    // Y : Output of the total Network: N[L-1] x 1 
    // L-1 is the index of output layer and it has N[L-1] nodes
    Y = new double[N[L-1]];      //output of network

    //W: Weights of the network : L-1 x N[l+1] x (N[l]+1)
    // l = 0, 1, 2, ..., L-2
    W = new double **[L-1];        

    //WI: Inverse index : (l, i, j) ---> k where k = 0, 1, 2, ..., WM
    WI = new int **[L-1];

    // Jyw: Weight Jacobian  : N[L-1] x WM
    // Jyu: Input Jacobian : N[L-1] x N[0]

    Jyw = new double *[N[L-1]];  
    Jyu = new double *[N[L-1]];  

    for(int i = 0; i < N[L-1]; i++)
    {
      Jyw[i] = new double [WM];
      Jyu[i] = new double [N[0]]; 

      for(int j = 0; j < WM; j++)
        Jyw[i][j] = 0.0;
      for(int j = 0; j < N[0]; j++)
        Jyu[i][j] = 0.0;        
    }

    // This stores the 3-dimensional weight index
    // k ------> (l, i, j)
    Wt_index=new int *[WM];     
    for(int k = 0; k < WM; k++)
      Wt_index[k]= new int[3];

    //One-dimensional weight vector : WM x 1
    AW = new double [WM];         


    //Allocate memory for weights and initialize them
    // W has 3 indices:
    // first index : l = 0, 1, 2, ... L-2 indicates layer
    // second index : i = 1, 2, ... N[l+1] indicates destination neuron
    // third index : j = 0, 1, 2, ..., N[l]+1 indicates source neuron.

    int k = 0;
    for(int l = 0; l < L-1; l++) // layer index 
    {
      W[l] = new double *[N[l+1]+1];
      WI[l] = new int *[N[l+1]+1];

      for(int i = 0; i <= N[l+1]; i++) //destination index
      {                                // i=0 is not used ...
        W[l][i] = new double [N[l]+1];
        WI[l][i] = new int [N[l]+1];

        for(int j = 0; j <= N[l]; j++) //source index
        {
          W[l][i][j] = rmin + (rmax - rmin) * gsl_rng_uniform(r);

          if(i != 0)
          {
            AW[k] = W[l][i][j];

            // Forward weight index: K --> (l,i,j)
            Wt_index[k][0] = l; 
            Wt_index[k][1] = i;
            Wt_index[k][2] = j;

            //Reverse Weight index : (l,i,j) ---> k
            WI[l][i][j] = k;
            k++;
          }
        }//j-loop
      }//i-loop
    }//l-loop

  }
  catch(std::bad_alloc xa)
  {
    cout<<"Allocation failure in FFN constructor\n";
    exit(-1);
  }   
}//EOF 
//-----------------------------------------
// Overloading insertion operator
// Date: 24 March 2009 Tuesday
// ------------------------------------------
ostream& nnet::operator<<(ostream& os, const ffn& a)
{
  os << "\n\n------------------------------------------\n";
  os << "Type:"<<" Feedforward Network"<<endl;
  os << "No. of Layers = "<< a.L << endl;
  os << "bias = " << a.bias << endl;
  os << "No. of neurons in each layer: ";
  os << "[ " ;
  for(int l = 0; l < a.L; l++)
    os << a.N[l] << "  ";
  os << "]" << endl;

  os << "\nWeight initialization range = (" << a.rmax << ", " << a.rmin << ")" << endl;
  os << "If you want different initialization values use wt_init()\n";
  os << "\nTotal number of weights = " << a.WM << endl;

  if( a.N != NULL)
  {
    os << "Size of weight Jacobian matrix (Jyw) = " << a.N[a.L-1] << "x" << a.WM << endl;
    os << "Size of Input Jacobian matrix (Jyu) = " << a.N[a.L-1] << "x" << a.N[0] << endl;
  }


  os << "\n\nActivation function:" << endl;
  os << "Output Neurons: " ;

  switch((int)a.oact[0])
  {
    case 1:
      os << "tansig," << " alpha = " << a.oact[1] << endl;
      break;
    case 2:
      os << "sigmoid," << " alpha = " << a.oact[1] << endl; 
      break;
    case 3: 
      os << "Linear," << " alpha = " << a.oact[1] << endl;
      break;
    case 4:
      os << "arctan," << " alpha =" << a.oact[1] << endl;
      break;
    case 5:
      os << "gauss," << " sigma =" << a.oact[1] << endl;
      break;
    case 6:
      os << "hyptan," << " alpha =" << a.oact[1] << endl;
      break;
    case 7:
      os << "thin-plate-spline," << " alpha =" << a.oact[1] << endl;
      break;
    case 8:
      os << "hypcos," << " alpha =" << a.oact[1] << endl;
      break;
    case 9:
      os << "tansigsquare," << " alpha =" << a.oact[1] << endl;
      break;
    case 10:
      os << "square," << " alpha=" << a.oact[1] << endl;
      break;
    default:
      os << "Invalid value for Activation function" << endl;
  }


  os << "Hidden Neurons: ";

  switch((int)a.act[0])
  {
    case 1:
      os << "tansig," << " alpha = " << a.act[1] << endl;
      break;
    case 2:
      os << "sigmoid," << " alpha = " << a.act[1] << endl; 
      break;
    case 3: 
      os << "Linear," << " alpha = " << a.act[1] << endl;
      break;
    case 4:
      os << "arctan," << " alpha =" << a.act[1] << endl;
      break;
    case 5:
      os << "gauss," << " sigma =" << a.act[1] << endl;
      break;
    case 6:
      os << "hyptan," << " alpha =" << a.act[1] << endl;
      break;
    case 7:
      os << "thin-plate-spline," << " alpha =" << a.oact[1] << endl;
      break;
    case 8:
      os << "hypcos," << " alpha =" << a.act[1] << endl;
      break;
    case 9:
      os << "tansigsquare," << " alpha =" << a.act[1] << endl;
      break;
    case 10:
      os << "square," << " alpha=" << a.act[1] << endl;
      break;
    default:
      os << "Invalid value for Activation function" << endl;
  }
  os << "-------------------------------------------\n\n";

  return os;
}//EOF 
//==========================================================
// overloading assignment operator
// useful when assigning one network to another network
// Thanks to Awhan for giving this suggestion
//
// Date: 13 December 2009, Sunday
// Status:
//
// ========================================================

ffn & ffn::operator=(const ffn & a)
{
  //cout << __FILE__ << " " << __PRETTY_FUNCTION__ << endl ;

  if( this == &a )
  {
    return *this;
  }

  //delete previously allocated memory
  deallocate_memory();

  L = a.L;

  bf = a.bf;
  bias = a.bias;

  WM = a.WM;

  for(int i = 0; i < 3; i++)
  {
    act[i] = a.act[i];
    oact[i] = a.oact[i];
  }

  N = new int [L];
  for(int l = 0; l < L; l++)
    N[l] = a.N[l];
  mnpl = a.mnpl;

  allocate_memory();

  //Re-initialization of class elements

  for(int l = 0; l < L; l++)
    for(int i = 0; i <= N[l]; i++)
    {
      S[l][i] = a.S[l][i];
      X[l][i] = a.X[l][i];
    }

  for(int i = 0; i < N[L-1]; i++)
    for(int j = 0; j < WM; j++)
      Jyw[i][j] = a.Jyw[i][j];

  for(int i = 0; i < N[L-1]; i++)
    for(int j = 0; j < N[0]; j++)
      Jyu[i][j] = a.Jyu[i][j];


  for(int l = 0; l < L-1; l++)
    for(int i = 1; i <= N[l+1]; i++)
      for(int j = 0; j <= N[l]; j++)
        W[l][i][j] = a.W[l][i][j];

  for(int i = 0; i < a.WM; i++)
    for(int j = 0; j < 3; j++)
      Wt_index[i][j] = a.Wt_index[i][j];

  for(int i = 0; i < N[L-1]; i++)
    Y[i] = a.Y[i];

  return *this ;
}//EOF
//--------------------------------------------------
// Display routine
// Objective: Display the contents of input parameter.
// Status: to be checked
// ---------------------------
void ffn::display_var(string s)
{
  cout << setw(10) << setprecision(4);
  if(s == "jyw")
  {
    cout << "\n-------------------------------\n";
    cout << "Weight Jacobian matrix (transpose) : Jyw' dim: "<< WM << "x" << N[L-1] << endl;
    cout << "-------------------------------\n";
    cout << setiosflags(ios::showpos);
    for(int i = 0; i < WM; i++)
    {
      for(int j = 0; j < N[L-1]; j++)
        cout << right << Jyw[j][i] << "\t";
      cout << endl;
    }
    cout << resetiosflags(ios::showpos);
    cout << "-----------------------------\n";    
  }
  else if(s == "jyu")
  {
    cout << "\n-----------------------------\n";
    cout << "Input jacobian matrix (Transpose): Jyu' dim: " << N[0] << "x" << N[L-1] << endl;
    cout << "-----------------------------\n";
    cout << setiosflags(ios::showpos);
    for(int i = 0; i < N[0]; i++)
    {
      for(int j = 0; j < N[L-1]; j++)
        cout << right << Jyu[j][i] << "\t";
      cout << endl;
    }
    cout << resetiosflags(ios::showpos);
    cout << "--------------------------------\n";   
  }
  else if(s == "wt")
  {
    cout << "\n----------------------\n";
    cout << "Weight matrix values:\n";
    cout << "-------------------------------" << endl;
    for(int l = 0; l < L-1; l++)
    {
      cout << "Layer: "<< l << ": W[" << l << "]" << endl;
      cout << "Dimension: " << N[l+1] << "x" << (N[l]+1) << endl << endl;
      cout << setiosflags(ios::showpos);
      for(int i = 1; i <= N[l+1]; i++)
      {
        for(int j = 0; j <= N[l]; j++)
          cout << left << setw(10) << W[l][i][j] << "\t";
        cout << endl;
      }
      cout << resetiosflags(ios::showpos);
      cout << endl << endl;
    }
    cout << "-------------------------------" << endl;
  }
  else if(s == "io")
  {
    cout << setiosflags(ios::showpoint);
    cout << "\n----------------------\n";
    cout << "Input-output pair of Each neuron\n";
    cout << "Column: Layers  Row: neurons\n";
    cout << "------------------------------\n";

    for(int i = 0; i <= mnpl; i++)
    {
      cout << i << ": ";
      cout << setiosflags(ios::showpos);
      for(int l = 0; l < L; l++)
      {
        if(i <= N[l])
          cout << right << setw(8) << X[l][i] << " " << right << setw(8) << S[l][i] << "\t";
        else
          cout << right << setw(8) << "***" << " " << right << setw(8) << "***" << "\t";
      }
      cout << endl;
      cout << resetiosflags(ios::showpos);
    }
    cout << "-------------------------------" << endl;
  }
  else if(s == "idx")
  {
    cout << left << endl;
    cout << "------------------------------------\n";
    cout << "Weight indices ..." << endl;
    int k = 0;
    for(int l = 0; l < L-1; l++)
      for(int i = 1; i <= N[l+1]; i++)
        for(int j = 0; j <= N[l]; j++)
        {
          cout << "(" << l << ", " << i << ", " << j << ") --> " << WI[l][i][j] << "   | ";
          cout << k << " --> (" << Wt_index[k][0] << ", " << Wt_index[k][1] << ", " << Wt_index[k][2] << ")" << endl;
          k = k + 1;
        }
    cout << "------------------------------------\n" << endl;
  }                               
  else
  {
    cout << "Invalid argument" << endl;
    cout << "Valid arguments are : 'jyw', 'jyu', 'wt', 'io', 'idx'\n";
    exit(-1);
  }
}//EOF

//----------------------------------------------------------
// Finds the output of Network for given input vector ip[]  
//  Status: OK
//  Last Checked on 24 March 2009, Tuesday
//  Last Checked on 19 April 2009, Sunday
//  ---------------------------------------------------------

void ffn::network_output(double ip[], double op[]) 
{
  // index of ip and op starts from 0
  //apply input

  // L = 0 is the input layer
  for(int i = 1; i <= N[0]; i++)
    X[0][i] = ip[i-1];  

  //find network output
  for(int l = 0; l < L; l++)
    for(int i = 0; i <= N[l]; i++)
    {
      if(i == 0) //bias neuron
      {
        S[l][i] = bias;
        X[l][i] = bias;
      }
      else
      { 
        if(l == 0) //no learning for input layer
          S[l][i] = X[l][i];
        else
        {
          double s1 = 0.0;
          for(int j = 0; j <= N[l-1]; j++)
            s1 += W[l-1][i][j] * S[l-1][j];
          X[l][i] = s1;
          if(l == L-1) //output layer
            S[l][i] = active_func(oact, X[l][i]);
          else
            S[l][i] = active_func(act, X[l][i]);
        }
      }
    }
  for(int i = 0; i < N[L-1]; i++)
  {
    Y[i] = S[L-1][i+1];
    op[i] = Y[i];
  }
}//EOF

//---------------------------------
// Computes the Jacobian matrix dy/dw using back propagation
// algorithm.
 //Last check on 24 March 2009, Tuesday
 //-------------------------------------------------------------------
void ffn::wt_jacobian(double **Jow)        
{                           
  //set the flag when this function is called for the first time
  if(called_wj == 0)
    called_wj = 1;

  // Output layer Index: L-1
  // (n,p,q) : Weight index
  // n = 0, 1, ..., L-2
  // p = 1, 2, ..., (N[n+1]+1);
  // q = 0, 1, ..., (N[n]+1)
  //
  // dY     dS(L-1,i)
  // --- =  --------
  // dW     dW(n,p,q)
  //
  // -----------------------

  for(int i = 1; i <= N[L-1]; i++)  //index of output layer nodes (starts from 1)
    for(int n = L-2; n >= 0; n--)   // i=0 is bias node
      for(int p = 1; p <= N[n+1]; p++)
        for(int q = 0; q <= N[n]; q++)
        {
          if(n == L-2) //weights for last layer
          {   
            Jyw[i-1][WI[n][p][q]] = dactive(oact, X[n+1][i]) * kdel(i,p) * S[n][q];
          }
          else if(n == L-3) //weights for second-last layer
          {
            Jyw[i-1][WI[n][p][q]] = Jyw[i-1][WI[n+1][i][p]]/S[n+1][p] * W[n+1][i][p] *
              dactive(act, X[n+1][p]) * S[n][q]; 
          }
          else
          {   
            double s1 = 0.0;
            for(int j = 1; j <= N[n+2]; j++)
              s1 += Jyw[i-1][WI[n+1][j][p]]/S[n+1][p] * W[n+1][j][p];
            Jyw[i-1][WI[n][p][q]] = s1 * dactive(act, X[n+1][p]) * S[n][q];
          }
        }

  // The indices of output matrix Jow starts from 0
  if(Jow != NULL)
  {
    for(int i = 0; i < N[L-1]; i++)
      for(int k = 0; k < WM; k++)
        Jow[i][k] = Jyw[i][k];
  }
}
//----------------------------------------------
void ffn::input_jacobian(double **dyu)
	/* This routine computes dy/du jacobian matrix where
	 * u is the input and y is the output of the
	 * network. The dimension of this matrix is
	 * N[L]xN[1].  it updates the matrix variable **Jyu.
    *
    * Last Check: 24 March 2009, Tuesday
    * Remark: Check again.
    *
    * Last Check on: 19 May 2009, Tuesday
    * --------------------------------------------------*/  
{
  //set the flag when this function is called
  if(called_ij == 0)
    called_ij = 1;

  //temporary array
  double **R;

  try
  {
    R = new double *[L];
    for(int l = 1; l < L; l++)
    {
      R[l] = new double [N[l]+1];
      for(int i = 0; i <= N[l]; i++)
        R[l][i]=0.0;
    }
  }
  catch(std::bad_alloc xa)
  {
    cout<<"Allocation failure in ffn::input_jacobian\n";
    exit(-1);
  }

  // --------------------------------------------
  //
  //
  // dY    dS(L-1, i) ; i = 1, 2, ..., N[L-1]
  // --- = ----------------------------------
  // dX    dX(0, m) ; m = 1, 2, ..., N[0]
  //
  //
  //       dS(L-1,i)     
  //     = --------- * R (L-1, i)  ; i = 1, 2, ..., N[L-1]
  //       dX(L-1,i)     
  //
  //              N[L-2]
  //             ------             dS(L-2, j)
  //             \   W(L-2, i, j) * ---------- * R(L-2, j) 
  // R(L-1, i) = /                  dX(L-2, j)
  //             -------
  //             j = 0
  //
  //
  //             k = N[L-3]
  //             -------           dS(L-3, k)
  // R(L-2, j) = \  W(L-3, j, k) * ---------- * R(L-3, k)
  //             /                 dX(L-3, k)
  //             ---------
  //              k = 0
  //  j = 0, 1, ..., N[L-2] 
  //
  //
  // Assume L = 4, then L-3 = 1
  //
  //  R(1,k) = W(L-4, k, m) = W(0, k, m);
  //
  //  k = 1, ..., N[L-3] = N[1];
  //
  //  ------------------------------------------------

  for(int i = 1; i <= N[L-1]; i++)  //output layer neuron index
  { 
    for(int m = 1; m <= N[0]; m++) //input layer neuron index
    { 
      for(int l = 1; l < L; l++)  //Take layers from 1 to L-1
      { 
        if(l == 1)    
        { 
          for(int k = 1; k<= N[l]; k++) // derivative for bias node is zero
            R[l][k] = W[l-1][k][m];// m - input node
        }
        else if(l < L)  
        { 
          for(int k = 1; k<= N[l]; k++) 
          {
            double s1 = 0.0;
            for(int j = 1; j <= N[l-1]; j++)
              s1 += W[l-1][k][j] * dactive(act, X[l-1][j]) * R[l-1][j];
            R[l][k] = s1;
          }
        } 
      }// Loop 3 closes here	

      Jyu[i-1][m-1] = dactive(oact, X[L-1][i]) * R[L-1][i];

      if(dyu != NULL)
        dyu[i-1][m-1] = Jyu[i-1][m-1]; 

    }//i-loop
  }//m-loop

  for(int l = 1; l < L; l++)
    delete [] R[l];
  delete [] R;
}//EOF 

//------------------------------------
// Separate Weight initialization routine for FFN
// Last Check: 24 March 2009, Tuesday
// --------------------------------------
void ffn::wt_init(double max, double min)
{
  rmax = max;
  rmin = min;

  int k = 0;
  for(int l = 0; l < L-1; l++)
    for(int i = 1; i <= N[l+1]; i++)
      for(int j = 0; j <= N[l]; j++)
      {
        W[l][i][j] = rmin + (rmax-rmin) * gsl_rng_uniform(r);

        AW[k] = W[l][i][j];
        WI[l][i][j] = k; //l,i,j --> k  
        Wt_index[k][0] = l, Wt_index[k][1] = i, Wt_index[k][2] = j;
        k++;
      }
}//EOF
//----------------------------------------------------------------------
// It fetches network parameters when called
// Date: 21/10/06
// Status: Under Review
// Last Check: 24 March 2009, Tuesday
// -------------------------------------
void ffn::get_parameters(double *wt_vec)
{
  if(wt_vec != NULL)
  {
    for(int i = 0; i < WM; i++)
      wt_vec[i] = AW[i];
  }
  else
  {
    cout << "Pass a vector of appropriate dimension" << endl;
    exit(-1);
  }
}
//--------------------------------------------
// Date: 31 March 2008 Monday
// It uploads the values of weights into the network
// Status: Under Review
//
// The index of supplied vector wt starts from 0.
// Last check: 24 March 2009, Tuesday
// ----------------------------------------------------
void ffn::load_parameters(double *wt)
{
  int k = 0;
  for(int l = 0; l < L-1; l++)
    for(int i = 1; i <= N[l+1]; i++)
      for(int j = 0; j <= N[l]; j++)
      {
        W[l][i][j] = wt[k];
        AW[k] = W[l][i][j];
        WI[l][i][j] = k; //l,i,j --> k  
        Wt_index[k][0] = l, Wt_index[k][1] = i, Wt_index[k][2] = j;
        k = k + 1;
      }
}
//----------------------------------------------------
void ffn::save_parameters(const char *filename)
{
  ofstream f1(filename);
  for(int i = 0; i < WM; i++)
    f1 << AW[i] << endl;
  f1.close();
  cout << "FFN parameters are saved\n" << endl;
}
//----------------------------------------------
void ffn::load_parameters(const char *filename)
{
  ifstream f1(filename);
  for(int i = 0; i < WM; i++)
  {
    f1 >> AW[i];
    W[Wt_index[i][0]][Wt_index[i][1]][Wt_index[i][2]] = AW[i];
  }
  f1.close();
  cout << "Parameteres loaded onto the network\n";
}
//---------------------------------------
// LEARNING ALGORITHMS
//--------------------------------

// Back-propagation algorithm
// Date: 25 March 2009, Wednesday
// Remark: Rigorous check needed
// --------------------------------------

void ffn::backprop(double yd[], double eta)
{
  // Wt layer index : 0, 1, 2, ..., L-2
  // Delta layer index: 1, 2, ..., L-1

  double **delta;
  try
  {
    // delta[L-1], delta[L-2] , ...., delta[1];
    delta = new double *[L];
    for(int l = 0; l < L; l++) //l=0 is not used
    {
      delta[l] = new double [N[l]+1]; 
      for(int i = 0; i <= N[l]; i++) // i = 0 is not used 
        delta[l][i] = 0.0;
    }
  }
  catch(std::bad_alloc xa)
  {
    cout<<"Allocation failure in FFN::backprop function\n";
    exit(-1);
  }

  for(int l = L-1; l > 0; l--)
    for(int j = 1; j <= N[l]; j++)
    {
      if(l == L-1) //Output layer 
      { 
        delta[l][j] = (S[l][j] - yd[j-1]) * dactive(oact, X[l][j]);
      }	
      else //for inner layers
      { 
        double s1 = 0.0;
        for(int p = 1; p <= N[l+1]; p++)
          s1 += delta[l+1][p] * W[l][p][j];  

        delta[l][j] = dactive(act, X[l][j]) * s1;
      }
    }

  for(int l = 0; l < L-1; l++)
    for(int i = 1; i <= N[l+1]; i++)
      for(int j = 0; j <= N[l]; j++)
      {
        W[l][i][j] = W[l][i][j] - eta * delta[l+1][i] * S[l][j];
        AW[WI[l][i][j]] = W[l][i][j];
      }

  for(int l = 0; l < L; l++)
    delete [] delta[l];
  delete [] delta;
} 
//-------------------------------------------------------- 
// LF 1 Algorithm : IEEE TNN Vol. 17, No. 5, Pages 1116-1125
//
// You must compute jacobian before calling this function
//
// Date: 02 May 2008, Friday
// Status: Success
// Last check on 04 May 2008 Sunday
//  Index of Yd starts from 0
//
//  Remark: Performance is sensitive to initial values of weights
//  and the value of learning parameter mu. 
//
//  Checked on 29 April 2009, Wednesday
// --------------------------------------------------
void ffn::lf1_update(double Yd[], double mu)
{
  if(called_wj == 0)
  {
    cout << "Call Wt_Jacobian() before calling this function" << endl;
    exit(-1);
  }

  double *jy, eta_a;

  try
  {
    jy = new double [WM+1];
  }
  catch(std::bad_alloc)
  {
    cout << "Allocation failure in ffn::lf1_update" << endl;
    exit(-1);
  }
  //Indices of Jyw, Yd and Y start from 0

  double st = 0.0;
  for(int i = 0; i < WM; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < N[L-1]; j++)
      s1 += Jyw[j][i] * (Yd[j] - Y[j]);
    jy[i] = s1;

    st += jy[i] * jy[i];
  }

  double sy = 0.0;
  for(int i = 0; i < N[L-1]; i++)
    sy += pow((Yd[i] - Y[i]), 2.0);


  if(st < 1e-5)
    st = st + 0.0001;

  //adaptive learning rate
  eta_a =  mu * sy/st;

  //update weights
  for(int i = 0; i < WM; i++)
  {
    AW[i] = AW[i] + eta_a * jy[i];

    W[Wt_index[i][0]][Wt_index[i][1]][Wt_index[i][2]] = AW[i];
  }

  delete [] jy;
}//EOF
//==============================
// ******** IT IS TO BE MODIFIED **************
// Date: 25 March 2009, Wednesday
// ------------------------------------------------------
// LF 2 Algorithm 
// Refer to IEEE TNN Vol. 17, No. 5, Page = 1116 - 1125
//
// You must compute jacobian by calling wt_jacobian() 
// function before calling this function
//
// Status: Success
// Date: 02 May 2008 Friday
// Last check: 04 May 2008 Sunday
// Indices of Yd, wt1 and wt2 start from 0
//
// Remark: The performance is sensitive to initial conditions and the
// values of learning parameters mu and lambda.
//
// As a thumb rule,  lambda < mu/10
// ---------------------------------------
void ffn::lf2_update(double Yd[], double mu, double lambda, double *wt1, double *wt2)
{
  double *w_ddot, *jy;
  double eta_a, eta_w;
  try{
    w_ddot = new double [WM+1];
    jy = new double [WM+1];
  }catch(std::bad_alloc){
    cout << "Allocation failure in ffn::lf2_update" << endl;
    exit(-1);
  }

  double st = 0.0;
  for(int i = 1; i <= WM; i++)
  {
    double s1 = 0.0;
    for(int j = 1; j <= N[L]; j++)
      s1 += Jyw[j][i] * (Yd[j-1] - Y[j]);
    jy[i] = s1;
    st += jy[i] * jy[i];

    w_ddot[i] = AW[i] - 2 * wt1[i-1] + wt2[i-1];
  }

  double sy = 0.0;
  for(int i = 1; i <= N[L]; i++)
    sy += pow((Yd[i-1] - Y[i]), 2.0);

  if(st < 1e-5)
    st = st + 0.0001;

  eta_a = mu * sy/st;

  eta_w = lambda * sy/st;

  //update weights
  for(int i = 1; i <= WM; i++)
  {
    AW[i] = AW[i] + eta_a * jy[i] - eta_w * w_ddot[i];

    W[Wt_index[i][1]][Wt_index[i][2]][Wt_index[i][3]] = AW[i];
  }

  delete [] w_ddot;
  delete [] jy;

}//EOF    */
//=======================================
/* Network inversion using GD
 * Date: 04 August 2008 Monday
 * Status: Under Revision
 * --------------------------------- */

// Date: 25 March 2009, Wednesday
// Last Check: 19 May 2009, Tuesday
// ------------------------------------------------------
void ffn::update_input_gd(double eta, double ydn[], double xn[])
{
  if(called_ij == 0)
  {
    cout << "input_jacobian function must be called before calling this function.\n";
    cout << " ... exiting!\n";
    exit(-1);
  }

  for(int i = 1; i <= N[0]; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < N[L-1]; j++)
      s1 += (ydn[j] - Y[j]) * Jyu[j][i-1];

    X[0][i] += eta * s1 ;
  }

  for(int i = 0; i < N[0]; i++)
    xn[i] = X[0][i+1];
}   

//=============================
void ffn::ExternalWeightUpdate(const double *dw)
{
  for(int i = 0; i < WM; i++)
  {
    AW[i] = AW[i] + dw[i];
    W[Wt_index[i][0]][Wt_index[i][1]][Wt_index[i][2]] = AW[i];
  }
}
//===========================



  
  






