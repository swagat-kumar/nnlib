/* ----------------------------------------------------
 *       SINGULAR VALUE DECOMPOSITION USING LAPACK
 * ---------------------------------------------------
 *
 *  A singular value decomposition of an m-by-n matrix A is given by
 *
 *             A = U S V'
 *
 *  where U and V are orthogonal (unitary)  and S is an m-by-n
 *  diagonal matrix with real diagonal elements, s_i, such that 
 *
 *             s_1 >= s_2 >= s_3 >= .... s_min{m,n} >= 0
 *
 * The s_i are the singular values of A and first min(m, n) columns of
 * U and V are left and right singular vectors of A respectively.
 *
 *
 * Singular values and singular vectors satisfy :
 *
 *          A v_i = s_i u_i and A' u_i = s_i v_i
 *
 * where u_i and v_i are the ith columns of U and V respectively.
 *
 * --------------------------------------------------------
 *  void dgesvd( double **A, int m, int n, double *S, double **U, double **VT);
 *
 *  A : m x n
 *
 *  U : m x m
 *
 *  VT : n x n
 *
 *  S : min(m, n) x 1 : singular values
 *
 * ----------------------------------------------------------------
 *
 * Author : Swagat Kumar 
 * Email: swagatk@iitk.ac.in/ swagat.kumar@gmail.com
 * Affiliation: Indian Institute of Technology Kanpur
 * License: GPL (GNU Public License)
 * Date: 14 December 2007 Friday
 * ----------------------------------------------------------- */

#include<utility.h>
#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstdlib>

using namespace std;


//-----------------------------------

extern "C" void dgesvd_(char *jobu, char *jobvt, int *m, 
    int *n, double *a, int *lda, double *s, double *u, 
    int *ldu, double *vt, int *ldvt, double *work, 
    int *lwork, int *info);

//------------------------------------------
int maximum(int m, int n)
{
  int s = m < n ? n : m;
  return (s);
}
int minimum(int m, int n)
{
  int s = m < n ? m : n;
  return(s);
}
//------------------------------------------------
// Lapack routines uses Matrices in Column-Major mode 
double* dgesvd_c2f(double **in, int rows, int cols)
{
  double *out;
  int i, j;

  out = new double[rows*cols];
  for (i = 0; i < rows; i++)
    for (j = 0; j < cols; j++) 
      out[i+j*rows] = in[i][j]; // column-major mode
  return(out);
} 
//-----------------------------------

int dgesvd(double **A, int m, int n, double *s1, double **U, double **VT)
{
  int lda, ldu, ldvt, lwork, info;

  double *a, *work, *u, *vt, *s;

  lda = m ;          // leading dimension of A
  ldu = m ;          // leading dimension of U
  ldvt = n;          // leading dimension of V'

  int sdim = minimum(m,n);

  lwork = maximum(3*minimum(m,n)+maximum(m,n),5*minimum(m, n)) + 10;

  work = new double [lwork];     // workspace memory to be used by LAPACK routine
  u = new double [ldu * m];
  vt = new double [ldvt * n];
  s = new double [sdim];

  //Note that the A matrix is destroyed by dgesvd routine and hence
  // it is assigned to a local variable Aip

  a = dgesvd_c2f(A, m, n);

  dgesvd_((char*)"A", (char*)"A", &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work, &lwork, &info);

  if(U != NULL)
  {
    for(int i = 0; i < ldu; i++)
      for(int j = 0; j < m; j++)
        U[i][j] = u[i + j * ldu];
  }

  if(VT != NULL)
  {
    for(int i = 0; i < ldvt; i++)
      for(int j = 0; j < n; j++)
        VT[i][j] = vt[i + j * ldvt];
  }
  if(s1 != NULL)
  {
    for(int i = 0; i < sdim; i++)
      s1[i] = s[i];
  }

  double eps = 2.2204e-16;
  double tol = maximum(m, n) * s[0] * eps;

  int cnt = 0;
  for(int i = 0; i < minimum(m, n); i++)
  {
    if(s[i] > tol)
      cnt = cnt + 1;
  }

  if(info != 0)
  {
    printf("Error in DGESVD =  %d\n", info);
    exit(-1);
  }

  //free memory
  delete [] work;
  delete [] u;
  delete [] vt;
  delete [] a;
  delete [] s;

  //return the rank of the matrix
  return cnt;
}//EOF
//------------------------------------
int dgesvd(const double *A, int m, int n, double *s1, double *U, double *VT)
{
  int lda, ldu, ldvt, lwork, info;

  double *a, *work, *u, *vt, *s;

  lda = m ;          // leading dimension of A
  ldu = m ;          // leading dimension of U
  ldvt = n;          // leading dimension of V'

  lwork = maximum(3*minimum(m,n)+maximum(m,n),5*minimum(m, n)) + 10;

  int sdim = minimum(m,n);

  work = new double [lwork];     // workspace memory to be used by LAPACK routine
  u = new double [ldu * m];
  vt = new double [ldvt * n];
  a = new double [m*n];
  s = new double [sdim];


  //convert from row-major to column-major mode
  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      a[i + j * m] = A[i * n + j];


  dgesvd_((char*)"A", (char*)"A", &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work, &lwork, &info);

  if(U != NULL)
  {
    for(int i = 0; i < ldu; i++)
      for(int j = 0; j < m; j++)
        U[i * m + j] = u[i + j * ldu];
  }

  if(VT != NULL)
  {
    for(int i = 0; i < ldvt; i++)
      for(int j = 0; j < n; j++)
        VT[i * n + j] = vt[i + j * ldvt];
  }

  if(s1 != NULL)
  {
    for(int i = 0; i < sdim; i++)
      s1[i] = s[i];
  }

  double eps = 2.2204e-16;
  double tol = maximum(m, n) * s[0] * eps;

  int cnt = 0;
  for(int i = 0; i < minimum(m, n); i++)
  {
    if(s[i] > tol)
      cnt = cnt + 1;
  }

  if(info != 0)
  {
    printf("Error in DGESVD =  %d", info);
    exit(-1);
  }

  //free memory
  delete [] work;
  delete [] u;
  delete [] vt;
  delete [] a;
  delete [] s;

  //return the rank of the matrix
  return cnt;
}//EOF

