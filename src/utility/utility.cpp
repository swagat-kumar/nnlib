/* -------------------------------------
 * Utility functions
 * Dependency: utility.h
 * Date: 26 February 2009 Thursday
 * ------------------------------------ */

#include<iostream>
#include<cassert>
#include<new>
#include<utility.h>
#include<cstdlib>
#include<iomanip>

using namespace std;


//------------------
// Date: 30 January 2009, friday
void normalize(const double *x, double *xn, int m, const double *x_max, const double *x_min, int bpf)
{
  if(bpf == 1)
  {
    for(int i = 0; i < m; i++)
      xn[i] = 2.0 *((x[i] - x_min[i])/(x_max[i] - x_min[i]) - 0.5);
  }
  else if(bpf == 0)
  {
    for(int i = 0; i < m; i++)
      xn[i] = (x[i] - x_min[i])/(x_max[i] - x_min[i]);
  }
  else
  {
    cout << "invalid value for last argument ... use 0 or 1" << endl;
    exit(-1);
  }
}//EOF
//---------------------------------------------------
void denormalize(const double *xn, double *x, int m, const double *x_max, const double *x_min, int bpf)
{
  if(bpf == 1)
  {
    for(int i = 0; i < m; i++)
      x[i] = x_min[i] + (xn[i]/2.0 + 0.5) * (x_max[i] - x_min[i]);
  }
  else if(bpf == 0)
  {
    for(int i = 0; i < m; i++)
      x[i] = x_min[i] + xn[i] * (x_max[i] - x_min[i]);
  }
  else
  {
    cout << "invalid value for last argument .. use 0 or 1" << endl;
    exit(-1);
  }
}
//========================================
//Array DisplayArray routines
//===========================================
void DisplayArray( double *q, int row, int col)
{
  int i,j;
  cout << setprecision(4) << showpos << showpoint;
  cout << endl;
  for(i = 0; i < row; i++)
  {
    for(j = 0; j < col; j++)
      cout << setw(10) << scientific << q[i * col + j] << "\t";
    cout << endl;
  }
  cout << endl;
  cout << noshowpoint << noshowpos << fixed << endl;
}
//--------------------------------------------
void DisplayArray(double *q, int row)
{
  cout << setprecision(4) << showpos << showpoint;
  int i;
  cout << endl;
  for(i = 0; i < row; i++)
    cout << setw(10) << scientific << q[i] << "\t";
  cout << noshowpos << noshowpoint << fixed << endl << endl;
}                      

//------------------------------------------

void DisplayArray(double **q, int row, int col)
{
  int i,j;
  cout << setprecision(4) << showpos << showpoint;
  cout << endl;
  for(i = 0; i < row; i++)
  {
    for(j = 0; j < col; j++)
      cout << scientific << setw(10) << q[i][j] << "\t";
    cout << endl;
  }
  cout << noshowpos << noshowpoint << fixed << endl << endl;
}  

//-----------------------------------------

void DisplayIntArray(int **q, int row, int col, int sw)
{
  int i,j;
  cout << showpos;
  cout << endl;
  for(i = 0; i < row; i++)
  {
    for(j = 0; j < col; j++)
      cout << setw(sw) << q[i][j] << " ";
    cout << endl;
  }
  cout << noshowpos << endl << endl;
}  

//-------------------------------------------
void DisplayArray(int *q, int row, int sw)
{
  cout << showpos << endl;
  for(int i = 0; i < row; ++i)
    cout << setw(sw) << q[i] << "\t";
  cout << noshowpos << endl << endl;
}                      
//----------------------------------------------
void DisplayArray(int *q, int row, int col, int sw)
{
  cout << showpos << endl;
  for(int i = 0; i < row; i++)
  {
    for(int j = 0; j < col; j++)
      cout << setw(sw) << q[i * col + j] << "\t";
    cout << endl;
  }
  cout << noshowpos << endl << endl;
}

//===============================================
// Array memory allocation routines
// ================================================

double* array(int m)
{
  double *x;
  try
  {
    x = new double [m];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }
  for(int i = 0; i < m; i++)
    x[i] = 0.0;
  return(x);
}
double* array(const double *a, int m)
{
  double *x;
  try
  {
    x = new double [m];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }
  for(int i = 0; i < m; i++)
    x[i] = a[i];

  return(x);
}
//----------------------------------------
double** array(int m, int n)
{
  double **x;
  try
  {
    x = new double *[m];
    for(int i = 0; i < m; i++)
      x[i] = new double [n];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      x[i][j] = 0.0;

  return(x);
}
//------------------------------------------------
// Create an Integer array
int** Intarray(int m, int n)
{
  int **x;
  try
  {
    x = new int *[m];
    for(int i = 0; i < m; i++)
      x[i] = new int [n];
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      x[i][j] = 0;

  return(x);
}
//----------------------------------
double** array(const double *a, int m, int n)
{
  double **x;

  try
  {
    int k = 0;
    x = new double *[m];
    for(int i = 0; i < m; i++)
    {
      x[i] = new double [n]; 
      for(int j = 0; j< n; j++)
      {
        x[i][j] = a[k];
        k = k + 1;
      }
    }
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }
  return(x);
}
//---------------------------------------
double*** array(int m, int n, int p)
{
  double ***x;
  try
  {
    x = new double **[m];
    for(int i = 0; i < m; i++)
    {
      x[i] = new double *[n];
      for(int j = 0; j < n; j++)
        x[i][j] = new double [p];
    }
  }
  catch(bad_alloc xa)
  {
    cout << "Allocation Failure in array function \n";
    assert(0);
  }

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      for(int k = 0; k < p; k++)
        x[i][j][k] = 0.0;

  return(x);
}
//=====================================
// Memory dellocation routines
// ===========================================
void free_memory(double *x)
{
  delete [] x;
}
//-------------------
//deallocating an 2D array
// where m is the first dimension, i.e., x[m][n]
void free_memory(double **x, int m)
{
  for(int i = 0; i < m; i++)
  {
    delete [] x[i];
  }
  delete [] x;
}
//--------------------------------
void free_memory(int **x, int m)
{
  for(int i = 0; i < m; i++)
  {
    delete [] x[i];
  }
  delete [] x;
}
//-------------------------
//deallocating 3D array x[m][n][p]
void free_memory(double ***x, int m, int n)
{
  for(int i = 0; i < m; i++)
  {
    for(int j = 0; j < n; j++)
      delete [] x[i][j];
    delete [] x[i];
  }
  delete [] x;
}
//======================================

/***************************
 *
 * sorting an array using qsort()
 *
 * **************************/
int compare_double_ascend(const void *num1, const void* num2)
{
  const double *n1 = (const double *) num1;
  const double *n2 = (const double *) num2;

  if (*n1 <  *n2) 
    return -1;
  else if (*n1 == *n2) 
    return  0;
  else return  1;   
}
//---------------------
int compare_double_descend(const void *num1, const void* num2)
{
  const double *n1 = (const double *) num1;
  const double *n2 = (const double *) num2;

  if (*n1 >  *n2) 
    return -1;
  else if (*n1 == *n2) 
    return  0;
  else
    return  1;   
}
//------------------------------------
int compare_integer_ascend(const void *num1, const void *num2)
{
  const int *n1 = (const int *) num1;
  const int *n2 = (const int *) num2;

  if (*n1 <  *n2) 
    return -1;
  else if (*n1 == *n2) 
    return  0;
  else 
    return  1;
}
//----------------------------------------
int compare_integer_descend(const void *num1, const void *num2)
{
  const int *n1 = (const int *) num1;
  const int *n2 = (const int *) num2;

  if (*n1 >  *n2) 
    return -1;
  else if (*n1 == *n2) 
    return  0;
  else 
    return  1;
}
//--------------------------------------------
void qsortD(double *data, int N, string option) 
{

  if(option == "ascend")
    qsort(data, N, sizeof(double),compare_double_ascend);
  else if(option == "descend")
    qsort(data, N, sizeof(double),compare_double_descend);
}
//-----------------------------------------------
void qsortInt(int *data, int N, string option) 
{
  if(option == "ascend")
    qsort(data, N, sizeof(int),compare_integer_ascend);
  else if(option == "descend")
    qsort(data, N, sizeof(int),compare_integer_descend);
}


