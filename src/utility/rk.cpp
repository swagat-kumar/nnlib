#include<iostream>
#include<cmath>
#include<cstdlib>
#include<utility.h>

using namespace std;
//using namespace cvm;

/* Fourth order fixed step runge-kutta routine 
 * .....................................
 * Description:
 * For a given input x(t) and u(t), it computes the next
 * value of state vector x(t+dt) by solving the set of first
 * order ordinary differential equations
 * xdot(t)=f(x(t),u(t),t)
 * 
 * Input Arguments:  xdot, x, u, t, h (time-step) and n
 * (dimension of state vector);
 * Arguments for func_dxdt are : xdot[], x[], u[], t 
 * NOTE: the vector indices start from 0.
 * ----------------------------------------------------*/

void runge_kutta(double *xdot,double *x, void *param, double t,double h,int n,
	void (*func_dxdt)(double*,double*,double,void*))
{
  int i;
  double *k1,*k2,*k3,*k4,*tempx;
  double *u = (double*)param;
  try{
    k1=	new double[n];
    k2=new double[n];
    k3=new double[n];
    k4=new double[n];
    tempx=new double[n];
  }catch(std::bad_alloc xa)
  {
    cout << "Allocation failure in runge_kutta routine\n";
    exit(-1);
  }

  (*func_dxdt)(xdot,x,t,u);
  for(i = 0; i < n; i++)
  {
    k1[i]=h*xdot[i];
    tempx[i]=x[i]+k1[i]/2.0;
  }

  (*func_dxdt)(xdot,tempx,t+h/2,u);
  for(i=0;i<n;i++)
  {
    k2[i]=h*xdot[i];
    tempx[i]=x[i]+k2[i]/2.0;
  }

  (*func_dxdt)(xdot,tempx,t+h/2,u);
  for(i=0;i<n;i++)
  {
    k3[i]=h*xdot[i];
    tempx[i]=x[i]+k3[i];
  }	

  (*func_dxdt)(xdot,tempx,t+h,u);
  for(i=0;i<n;i++)
    k4[i]=h*xdot[i];

  for(i=0;i<n;i++)
    x[i]=x[i]+(k1[i]+2*k2[i]+2*k3[i]+k4[i])/6.0;

  delete [] k1;
  delete [] k2;
  delete [] k3;
  delete [] k4;
  delete [] tempx;
}                                        
//-----------------------------------------

#ifdef CVM
Vector cvm::rk4_cvm(Vector x, Vector u, double t, double dt, 
    Vector(*func_dxdt)(Vector, Vector, double))
{
  int n = x.getrow();

  Vector k1(n);
  Vector k2(n);
  Vector k3(n);
  Vector k4(n);
  Vector tempx(n);
  Vector xdot(n);

  xdot = (*func_dxdt)(x,u,t);
  k1 = dt * xdot;
  tempx = x + 0.5 * k1;


  xdot = (*func_dxdt)(tempx, u, t+dt/2);
  k2 = dt * xdot;
  tempx = x + 0.5 * k2;

  xdot = (*func_dxdt)(tempx, u, t+dt/2);
  k3 = dt * xdot;
  tempx = x + k3;

  xdot = (*func_dxdt)(tempx, u, t+dt);
  k4 = dt * xdot;

  x = x + (k1 + 2 * k2 + 2 * k3 + k4)/6.0;


  return(x);
}
#endif
//--------------------------------------------
